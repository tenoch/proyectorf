package bo.gob.uif.webclient.fb.me;

import bo.gob.uif.webclient.fb.Constants;
import bo.gob.uif.webclient.fb.MultiqueryResults;
import com.restfb.Facebook;

import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.Post;
import facebook4j.Reading;
import facebook4j.ResponseList;
import facebook4j.auth.AccessToken;
import facebook4j.conf.ConfigurationBuilder;
import facebook4j.internal.org.json.JSONArray;
import facebook4j.internal.org.json.JSONException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.System.out;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.core.config.Configuration;
import org.json.JSONObject;

public class SimpleMeExample {

    //public Facebook facebook:
    public static void main(String[] args) throws FacebookException, JSONException, IOException {

     // Make the configuration builder 
//        ConfigurationBuilder confBuilder = new ConfigurationBuilder(); 
//        confBuilder.setDebugEnabled(true); 
//        // Set application id, secret key and access token 
//        confBuilder.setOAuthAppId(""); 
//        confBuilder.setOAuthAppSecret(""); 
//        confBuilder.setOAuthAccessToken("");
//
//
//        // Set permission 
//        confBuilder.setOAuthPermissions("email,publish_stream, id, name, first_name, last_name, generic"); 
//        confBuilder.setUseSSL(true); 
//        confBuilder.setJSONStoreEnabled(true); 
//
//
//        // Create configuration object 
//        Configuration configuration = (Configuration) confBuilder.build(); 
//
//        // Create facebook instance 
////        FacebookFactory ff = new FacebookFactory((facebook4j.conf.Configuration) configuration); 
////        Facebook facebook = (Facebook) ff.getInstance();
//     facebook4j.Facebook facebook = new FacebookFactory().getInstance();
//        try { 
        // Get facebook posts 
        // String results = getFacebookPosts((Facebook) facebook);
        //String responce = stringToJson(results); 
            // Create file and write to the file 
//            File file = new File("C:\\Facebook\\test.txt"); 
//            if (!file.exists()) 
//            { 
//                file.createNewFile(); 
//            }
//                FileWriter fw = new FileWriter(file.getAbsoluteFile()); 
//                BufferedWriter bw = new BufferedWriter(fw);
//                bw.write(results); 
//                bw.close(); 
//                System.out.println("Writing complete"); 
//
//            } catch (IOException e) { 
//                // TODO Auto-generated catch block 
//                e.printStackTrace();
//                } 
//   }
//        facebook.setOAuthAppId(Constants.MY_APP_ID, Constants.MY_APP_SECRET);
//
//        facebook.setOAuthAccessToken(new AccessToken(Constants.MY_ACCESS_TOKEN));
//   ResponseList<Post> feed = facebook.getFeed();
//System.out.println(facebook.getId());
//    System.out.println(facebook.getName());
//    System.out.println(facebook.getFeed());
//
//
//   
//   
//    }
//}
//public static String getFacebookPosts(Facebook facebook) throws FacebookException { 
//    // Get posts for a particular search 
////    ResponseList<Post> results = facebook.getFeed("Rebook");
//      ResponseList<Post> feed = facebook.getFeed();
//    //System.out.println(facebook.getId());
//    //System.out.println(facebook.getName());
//    //System.out.println(facebook.getFeed());
//
//    return feed.toString(); 
//    } 
//
//public static String stringToJson(String data) throws org.json.JSONException 
//{ // Create JSON object 
//    JSONObject jsonObject = JSONObject.fromObject(data); 
//    JSONArray message = (JSONArray) jsonObject.get("message"); 
//    System.out.println("Message : "+message); 
//    return "Done"; 
//    }
//
//}   
//        FacebookClient facebookClient = new DefaultFacebookClient(Constants.MY_ACCESS_TOKEN);
//        //FacebookClient publicOnlyFacebookClient = new DefaultFacebookClient();
//        User user = facebookClient.fetchObject("me", User.class);
//        out.println("User name: " + user.getName());
//        Connection<User> myFriends = facebookClient.fetchConnection("me/friends", User.class);
//        Connection<Post> myFeed = facebookClient.fetchConnection("me/feed", Post.class);
//
//        out.println("Count of my friends: " + myFriends.getData().size());
//        out.println("First item in my feed: " + myFeed.getData().get(0));
//
//
//        for (List<Post> myFeedConnectionPage : myFeed) {
//            for (Post post : myFeedConnectionPage) {
//                out.println("Post: " + post);
//            }
//        }
//        Connection<Post> publicSearch
//                = facebookClient.fetchConnection("search", Post.class,
//                        Parameter.with("q", "llevo"), Parameter.with("type", "post"));
//
//        Connection<User> targetedSearch
//                = facebookClient.fetchConnection("me/home", User.class,
//                        Parameter.with("q", "Takas"), Parameter.with("type", "user"));
//
//        out.println("Public search: " + publicSearch.getData().get(0).getMessage());
//        out.println("Posts on my wall by friends named Mark: " + targetedSearch.getData().size());
//        Connection<Insight> insights = facebookClient.fetchConnection("742750866105806/insights", Insight.class);
//
//        for (Insight insight : insights.getData()) {
//            out.println(insight.getName());
//        }
//        String query = "SELECT uid, name FROM user WHERE uid=220439 or uid=7901103";
//      List<FqlUser> users = facebookClient.executeFqlQuery(query, FqlUser.class);
//       out.println("Users: " + users);
        facebook4j.Facebook facebook = new FacebookFactory().getInstance();
        facebook.setOAuthAppId(Constants.MY_APP_ID, Constants.MY_APP_SECRET);

        facebook.setOAuthAccessToken(new AccessToken(Constants.MY_ACCESS_TOKEN));
    //  Reading reading = new Reading().fields("id", "created_time", "message");
     // facebook.getFeed("Rebook", reading);
//        ResponseList friends = facebook.getFriends();
//        for (Object friend : friends) {
//            System.out.println(friend);
//        }
//
//        System.out.println("\tGetting my posts\n");
//        ResponseList<Post> feed = facebook.getFeed();
//        for (Post post : feed) {
//            System.out.println(post);
//        }
//
        System.out.println("\tGetting cool posts\n");
        ResponseList<Post> geeksFeed = facebook.getFeed("");
        for (Post post : geeksFeed) {
            System.out.println(post);
        }
//
//        ResponseList<Post> results = facebook.getFeed();
//        System.out.println(facebook.getId());
//        System.out.println(facebook.getName());
//        System.out.println(facebook.getFeed());

// Single FQL
//        String query = "SELECT uid2 FROM friend WHERE uid1=me()";
//        JSONArray jsonArray = facebook.executeFQL(query);
//        for (int i = 0; i < jsonArray.length(); i++) {
//            facebook4j.internal.org.json.JSONObject jsonObject = jsonArray.getJSONObject(i);
//            System.out.println(jsonObject.get("uid2"));
//        }
//Map<String, String> queriesqueries = new HashMap<String, String>() {
//    {
//        put("users", "SELECT uid, name FROM user WHERE uid=220439 OR uid=7901103");
//        put("likers", "SELECT user_id FROM like WHERE object_id=122788341354");
//    }
//    
//};
    }
}
