package bo.gob.uif.webclient.fb.me;

import com.restfb.Facebook;

public class FqlUser {

   @Facebook
  String uid;

  @Facebook
  String name;

  @Override
  public String toString() {
    return String.format("%s (%s)", name, uid);
  }

}
