package bo.gob.uif.webclient.fb;

import bo.gob.uif.webclient.fb.me.FqlLiker;
import bo.gob.uif.webclient.fb.me.FqlUser;
import com.restfb.Facebook;
import java.util.List;

public class MultiqueryResults {
  @Facebook
  List<FqlUser> users;

  @Facebook
  List<FqlLiker> likers;

    public List<FqlUser> getUsers() {
        return users;
    }

    public void setUsers(List<FqlUser> users) {
        this.users = users;
    }

    public List<FqlLiker> getLikers() {
        return likers;
    }

    public void setLikers(List<FqlLiker> likers) {
        this.likers = likers;
    }
  
  
  
  
  
}