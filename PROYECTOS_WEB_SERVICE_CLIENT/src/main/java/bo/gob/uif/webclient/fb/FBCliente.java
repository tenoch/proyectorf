/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.gob.uif.webclient.fb;

import bo.gob.uif.webclients.inra.*;
import bo.gob.uif.webclients.cfg.MainWebServicesConfig;
import bo.gob.uif.webclients.cfg.WebServiceConfig;
import bo.gob.uif.webclients.inra.beans.RespuestaInraBean;
import bo.gob.uif.webclients.inra.beans.RespuestaInraDatosBeneficiarioBean;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author Susana.Escobar
 *
 * Creado por: Susana Escobar Paz
 *
 * Descripcion: clase que contiene implementacion de metodos, para consultar al
 * servicio web rest proporcionado por la entidad Inra
 *
 * Fecha: 30/05/2018
 */
public class FBCliente {

    public static WebServiceConfig wsConfig = MainWebServicesConfig.INSTANCE.getWebServiceConfig("INRAConsultaSerWebRest");
    public static final ObjectMapper JSON_MAPPER = new ObjectMapper();
    private static Logger log;

    /**
     * Creado por: Susana Escobar Paz
     *
     * Descripcion: metodo que se conecta al servicio Web Rest del Inra,
     * mediante POST enviando una cadena de texto con datos de autenticacion en
     * formato Json, para recibir el token necesario para poder realizar
     * consultas
     *
     * Fecha: 01/06/2018
     *
     * @return
     */
    public static String obtieneToken() {
//        Logger log;
//        log = LogManager.getLogger("InraClient.obtieneToken");
//
//        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
//        String token = "";
//        String POST_DATA = "{\"usuario\":\""
//                + "" + wsConfig.getUsuario() + "\""
//                + ",\"contrasena\":\""
//                + "" + wsConfig.getContrasena() + "\"}";
//
//        try {
//            URL url = new URL(wsConfig.getLocation());
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setRequestMethod("POST");
//            conn.setRequestProperty("Content-Type", "application/json");
//            conn.setRequestProperty("Accept", "application/json");
//            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.79 Safari/537.36");
//            conn.setDoOutput(true);
//            conn.setRequestProperty("Content-Length", Integer.toString(POST_DATA.length()));
//            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
//            dos.writeBytes(POST_DATA);
//            dos.flush();
//            dos.close();
//            token = conn.getHeaderField("Authorization");
//            conn.disconnect();
//            if (token != null) {
//                log.info("Devolviendo Token");
//            } else {
//                log.error("No se pudo obtener Token, Codigo de respuesta:  " + conn.getResponseCode());
//            }
//
//        } catch (MalformedURLException ex) {
//            System.out.println("Error al conectarse con el servicio, no se pudo obtener Token " + ex);
//            ex.printStackTrace();
//            log.catching(ex);
//        } catch (IOException ex) {
//            System.out.println("Error al conectarse con el servicio, no se pudo obtener Token " + ex);
//            ex.printStackTrace();
//            log.catching(ex);
//        }
        String token=Constants.MY_ACCESS_TOKEN;
        return token;
    }

    
    public static RespuestaInraBean busquedaNumeroTitulo(String numeroTitulo) {
        Logger log1;
        log1 = LogManager.getLogger("InraClient.busquedaNumeroTitulo");
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        RespuestaInraBean objeto = new RespuestaInraBean();
        try {
            String myToken = obtieneToken();
            URL urlget = new URL(wsConfig.getLocation2() + numeroTitulo);
            HttpURLConnection connget = (HttpURLConnection) urlget.openConnection();
            connget.setRequestMethod("GET");
            connget.setDoInput(true);
            connget.setRequestProperty("Accept", "application/json");
            connget.setRequestProperty("Authorization", myToken);
            connget.setRequestProperty("Content-Type", "application/json;charset=UTF-8");

            String response = IOUtils.toString(connget.getInputStream(), "UTF-8");

            if (!response.equals("")) {
                objeto = JSON_MAPPER.readValue(response, new TypeReference<RespuestaInraBean>() {
                });
                objeto.setDatoEncontrado(true);
                objeto.setMensaje("Registro encontrado");
            } else {
                objeto.setDatoEncontrado(false);
                objeto.setMensaje("No se encontró el registro");
            }

        } catch (MalformedURLException ex) {
            objeto.setDatoEncontrado(false);
            objeto.setMensaje("Error al conectarse con el servicio");
            System.out.println("Error al conectarse con el servicio " + ex);
            ex.printStackTrace();
            log1.catching(ex);
        } catch (IOException ex) {
            objeto.setDatoEncontrado(false);
            objeto.setMensaje("Error al conectarse con el servicio");
            System.out.println("Error al conectarse con el servicio " + ex);
            ex.printStackTrace();
            log1.catching(ex);
        }
        return objeto;
    }

    /**
     * Creado por: Susana Escobar Paz
     *
     * Descripcion: metodo que se conecta al servicio Web Rest del Inra,
     * mediante POST enviando dos cadenas de texto con datos de numero de
     * docuemnto de identificacion de una persona y un dato (parte de su nombre)
     * como parametros y un token.
     *
     * Fecha: 01/06/2018
     *
     * @param numeroIdentificacion
     * @param datoBeneficiario
     * @return
     */
    public static RespuestaInraDatosBeneficiarioBean busquedaDatosBenefciario(String numeroIdentificacion, String datoBeneficiario) {
        Logger log2;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log2 = LogManager.getLogger("InraClient.busquedaDatosBenefciario");
        RespuestaInraDatosBeneficiarioBean objeto = new RespuestaInraDatosBeneficiarioBean();
        List<RespuestaInraBean> listaRespuestaInraBean = new ArrayList<RespuestaInraBean>();

        try {

            String myToken = obtieneToken();

            URL urlget = new URL(wsConfig.getLocation3() + numeroIdentificacion + "/" + datoBeneficiario);
            HttpURLConnection connget = (HttpURLConnection) urlget.openConnection();
            connget.setRequestMethod("GET");
            connget.setDoInput(true);
            connget.setRequestProperty("Accept", "application/json;charset=UTF-8");
            connget.setRequestProperty("Content-Type", "application/json;charset=UTF-8");

            connget.setRequestProperty("Authorization", myToken);

            String response = IOUtils.toString(connget.getInputStream(), "UTF-8");

            if (!response.equals("") && !response.equals("[]")) {
                listaRespuestaInraBean = JSON_MAPPER.readValue(response, new TypeReference<List<RespuestaInraBean>>() {
                });

                objeto.setListaRespuestaDatosBeneficiario(listaRespuestaInraBean);
                objeto.setDatoEncontrado(true);
                objeto.setMensaje("Registro encontrado");
            } else {
                objeto.setDatoEncontrado(false);
                objeto.setMensaje("No se encontró el registro");
            }

        } catch (MalformedURLException ex) {
            objeto.setDatoEncontrado(false);
            objeto.setMensaje("Error al conectarse con el servicio");
            System.out.println("Error al conectarse con el servicio " + ex);
            ex.printStackTrace();
            log2.catching(ex);

        } catch (IOException ex) {
            objeto.setDatoEncontrado(false);
            objeto.setMensaje("Error al conectarse con el servicio");
            System.out.println("Error al conectarse con el servicio " + ex);
            ex.printStackTrace();
            log2.catching(ex);
        }
        return objeto;
    }
}
