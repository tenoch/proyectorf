package bo.gob.uif.webclients.caronte.beans;

import java.io.Serializable;

public class SujetoObligadoDatosBean implements Serializable {

    private String codigoEntidad;
    private String descripcionEntidad;
    private String tipoEntidad;
    private String otroEntidad;
    private String nitEntidad;
    private String lugarEntidad;
    private String zonaEntidad;
    private String direccionEntidad;
    private String telefonoEntidad;
    private String correoEntidad;
    private String tipoSujetoObligado;
    private String errCodigo;
    private String errMensaje;

    public String getCodigoEntidad() {
        return codigoEntidad;
    }

    public void setCodigoEntidad(String codigoEntidad) {
        this.codigoEntidad = codigoEntidad;
    }

    public String getDescripcionEntidad() {
        return descripcionEntidad;
    }

    public void setDescripcionEntidad(String descripcionEntidad) {
        this.descripcionEntidad = descripcionEntidad;
    }

    public String getTipoEntidad() {
        return tipoEntidad;
    }

    public void setTipoEntidad(String tipoEntidad) {
        this.tipoEntidad = tipoEntidad;
    }

    public String getOtroEntidad() {
        return otroEntidad;
    }

    public void setOtroEntidad(String otroEntidad) {
        this.otroEntidad = otroEntidad;
    }

    public String getNitEntidad() {
        return nitEntidad;
    }

    public void setNitEntidad(String nitEntidad) {
        this.nitEntidad = nitEntidad;
    }

    public String getLugarEntidad() {
        return lugarEntidad;
    }

    public void setLugarEntidad(String lugarEntidad) {
        this.lugarEntidad = lugarEntidad;
    }

    public String getZonaEntidad() {
        return zonaEntidad;
    }

    public void setZonaEntidad(String zonaEntidad) {
        this.zonaEntidad = zonaEntidad;
    }

    public String getDireccionEntidad() {
        return direccionEntidad;
    }

    public void setDireccionEntidad(String direccionEntidad) {
        this.direccionEntidad = direccionEntidad;
    }

    public String getTelefonoEntidad() {
        return telefonoEntidad;
    }

    public void setTelefonoEntidad(String telefonoEntidad) {
        this.telefonoEntidad = telefonoEntidad;
    }

    public String getCorreoEntidad() {
        return correoEntidad;
    }

    public void setCorreoEntidad(String correoEntidad) {
        this.correoEntidad = correoEntidad;
    }

    public String getTipoSujetoObligado() {
        return tipoSujetoObligado;
    }

    public void setTipoSujetoObligado(String tipoSujetoObligado) {
        this.tipoSujetoObligado = tipoSujetoObligado;
    }

    public String getErrCodigo() {
        return errCodigo;
    }

    public void setErrCodigo(String errCodigo) {
        this.errCodigo = errCodigo;
    }

    public String getErrMensaje() {
        return errMensaje;
    }

    public void setErrMensaje(String errMensaje) {
        this.errMensaje = errMensaje;
    }
}
