package bo.gob.uif.webclients.siso.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Vladimir.Callisaya on 15/09/2016.
 */
public class ReporteRosBean implements Serializable {

    private String existe;
    private Date fechaEnvio;
    private long idRosFormulario;
    private long idUsuarioSicod;
    private String nroTramiteSicod;
    private String numero;
    private String sujetoObligado;

    public String getExiste() {
        return existe;
    }

    public void setExiste(String existe) {
        this.existe = existe;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public long getIdRosFormulario() {
        return idRosFormulario;
    }

    public void setIdRosFormulario(long idRosFormulario) {
        this.idRosFormulario = idRosFormulario;
    }

    public String getNroTramiteSicod() {
        return nroTramiteSicod;
    }

    public void setNroTramiteSicod(String nroTramiteSicod) {
        this.nroTramiteSicod = nroTramiteSicod;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getSujetoObligado() {
        return sujetoObligado;
    }

    public void setSujetoObligado(String sujetoObligado) {
        this.sujetoObligado = sujetoObligado;
    }

    public long getIdUsuarioSicod() {
        return idUsuarioSicod;
    }

    public void setIdUsuarioSicod(long idUsuarioSicod) {
        this.idUsuarioSicod = idUsuarioSicod;
    }

    @Override
    public String toString() {
        return "ReporteRosBean{"
                + "fechaEnvio=" + fechaEnvio
                + ", idRosFormulario=" + idRosFormulario
                + ", nroTramiteSicod='" + nroTramiteSicod + '\''
                + ", numero='" + numero + '\''
                + ", sujetoObligado='" + sujetoObligado + '\''
                + '}';
    }
}
