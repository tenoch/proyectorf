/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.gob.uif.webclients.sicod.beans;

/**
 *
 * @author Alan.Portugal
 */
public class AsignacionTramiteRespBean {

    private long idAsignacion;
    private long idUsuarioDestino;

    public long getIdAsignacion() {
        return idAsignacion;
    }

    public void setIdAsignacion(long idAsignacion) {
        this.idAsignacion = idAsignacion;
    }

    public long getIdUsuarioDestino() {
        return idUsuarioDestino;
    }

    public void setIdUsuarioDestino(long idUsuarioDestino) {
        this.idUsuarioDestino = idUsuarioDestino;
    }

}
