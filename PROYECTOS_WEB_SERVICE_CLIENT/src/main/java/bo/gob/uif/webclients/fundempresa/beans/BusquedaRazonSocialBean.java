package bo.gob.uif.webclients.fundempresa.beans;

import java.io.Serializable;

/**
 * Created by Vladimir.Callisaya on 06/09/2016.
 */
public class BusquedaRazonSocialBean implements Serializable {

    private String ctrResult;
    private String idMatricula;
    private String razonSocial;
    private String nit;

    public String getCtrResult() {
        return ctrResult;
    }

    public void setCtrResult(String ctrResult) {
        this.ctrResult = ctrResult;
    }

    public String getIdMatricula() {
        return idMatricula;
    }

    public void setIdMatricula(String idMatricula) {
        this.idMatricula = idMatricula;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    @Override
    public String toString() {
        return "BusquedaRazonSocialBean{"
                + "ctrResult='" + ctrResult + '\''
                + ", idMatricula='" + idMatricula + '\''
                + ", razonSocial='" + razonSocial + '\''
                + ", nit='" + nit + '\''
                + '}';
    }
}
