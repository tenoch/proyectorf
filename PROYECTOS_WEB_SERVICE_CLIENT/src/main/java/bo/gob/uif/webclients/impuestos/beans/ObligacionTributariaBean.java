package bo.gob.uif.webclients.impuestos.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Vladimir.Callisaya on 11/08/2016.
 */
public class ObligacionTributariaBean implements Serializable {

    private String descripcionObligacion;
    private Date fechaDesde;
    private Date fechaHasta;

    public String getDescripcionObligacion() {
        return descripcionObligacion;
    }

    public void setDescripcionObligacion(String descripcionObligacion) {
        this.descripcionObligacion = descripcionObligacion;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }
}
