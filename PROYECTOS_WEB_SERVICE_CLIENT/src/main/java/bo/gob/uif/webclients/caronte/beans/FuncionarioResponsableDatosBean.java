package bo.gob.uif.webclients.caronte.beans;

import java.io.Serializable;

public class FuncionarioResponsableDatosBean implements Serializable {

    private String codigoFuncionario;
    private String nombreFuncionario;
    private String apPaternoFuncionario;
    private String apMaternoFuncionario;
    private String apCasadaFuncionario;
    private String documentoFuncionario;
    private String fechaNacimientoFuncionario;
    private String telefonoFuncionario;
    private String internoFuncionario;
    private String errCodigo;
    private String errMensaje;

    public String getCodigoFuncionario() {
        return codigoFuncionario;
    }

    public void setCodigoFuncionario(String codigoFuncionario) {
        this.codigoFuncionario = codigoFuncionario;
    }

    public String getNombreFuncionario() {
        return nombreFuncionario;
    }

    public void setNombreFuncionario(String nombreFuncionario) {
        this.nombreFuncionario = nombreFuncionario;
    }

    public String getApPaternoFuncionario() {
        return apPaternoFuncionario;
    }

    public void setApPaternoFuncionario(String apPaternoFuncionario) {
        this.apPaternoFuncionario = apPaternoFuncionario;
    }

    public String getApMaternoFuncionario() {
        return apMaternoFuncionario;
    }

    public void setApMaternoFuncionario(String apMaternoFuncionario) {
        this.apMaternoFuncionario = apMaternoFuncionario;
    }

    public String getApCasadaFuncionario() {
        return apCasadaFuncionario;
    }

    public void setApCasadaFuncionario(String apCasadaFuncionario) {
        this.apCasadaFuncionario = apCasadaFuncionario;
    }

    public String getDocumentoFuncionario() {
        return documentoFuncionario;
    }

    public void setDocumentoFuncionario(String documentoFuncionario) {
        this.documentoFuncionario = documentoFuncionario;
    }

    public String getFechaNacimientoFuncionario() {
        return fechaNacimientoFuncionario;
    }

    public void setFechaNacimientoFuncionario(String fechaNacimientoFuncionario) {
        this.fechaNacimientoFuncionario = fechaNacimientoFuncionario;
    }

    public String getTelefonoFuncionario() {
        return telefonoFuncionario;
    }

    public void setTelefonoFuncionario(String telefonoFuncionario) {
        this.telefonoFuncionario = telefonoFuncionario;
    }

    public String getInternoFuncionario() {
        return internoFuncionario;
    }

    public void setInternoFuncionario(String internoFuncionario) {
        this.internoFuncionario = internoFuncionario;
    }

    public String getErrCodigo() {
        return errCodigo;
    }

    public void setErrCodigo(String errCodigo) {
        this.errCodigo = errCodigo;
    }

    public String getErrMensaje() {
        return errMensaje;
    }

    public void setErrMensaje(String errMensaje) {
        this.errMensaje = errMensaje;
    }
}
