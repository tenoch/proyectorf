package bo.gob.uif.webclients.exceptions;

/**
 * Created by Vladimir.Callisaya on 11/08/2016.
 */
public class WebServiceInternalException extends Exception {

    public WebServiceInternalException(String message) {
        super(message);
    }

    public WebServiceInternalException(String message, Throwable cause) {
        super(message, cause);
    }
}
