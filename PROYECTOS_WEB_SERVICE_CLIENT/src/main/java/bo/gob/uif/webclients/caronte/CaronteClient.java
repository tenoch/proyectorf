//package bo.gob.uif.webclients.caronte;
//
//import bo.gob.uif.webclients.cfg.MainWebServicesConfig;
//import bo.gob.uif.webclients.cfg.WebServiceConfig;
//import bo.gob.uif.webclients.common.SoapHttpRequest;
//import bo.gob.uif.webclients.common.StringTokenizer;
//import bo.gob.uif.webclients.caronte.beans.SujetoObligadoBusquedaBean;
//import bo.gob.uif.webclients.caronte.beans.SujetoObligadoDatosBean;
//import bo.gob.uif.webclients.caronte.beans.FuncionarioResponsableDatosBean;
//import bo.gob.uif.webclients.caronte.beans.AnalistaCumplimientoDatosBean;
//
//
//import bo.gob.uif.webclients.caronte.beans.UsuarioServiciosBean;
//import bo.gob.uif.webclients.fundempresa.beans.MatriculaDatosSucBean;
//
//import org.jdom2.Document;
//import org.jdom2.Element;
//import org.jdom2.Namespace;
//
//import java.awt.*;
//import java.io.ByteArrayInputStream;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.ParserConfigurationException;
//import javax.xml.xpath.XPath;
//import javax.xml.xpath.XPathFactory;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.jdom2.filter.ElementFilter;
//import org.jdom2.output.XMLOutputter;
//
//
//public class CaronteClient {
//	public static WebServiceConfig wsConfig = MainWebServicesConfig.INSTANCE.getWebServiceConfig("CARONTEConsultaSerWeb");
//    private static Logger log;
//    private static StringTokenizer st = new StringTokenizer();
//    
//    public static List<SujetoObligadoBusquedaBean> getSujetosObligados_busqueda(String strSujetoObligado, int tipoBusqueda, String usuario, String contrasenia) {
//    	List<SujetoObligadoBusquedaBean> listaSujetoObligado_busqueda = new ArrayList<SujetoObligadoBusquedaBean>();
//    	SujetoObligadoBusquedaBean sujetoObligadoBusquedaBean = new SujetoObligadoBusquedaBean();
//        
//    	Element element;
//        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
//        log = LogManager.getLogger("CaronteClient.getSujetosObligados_busqueda");
//        log.info("Iniciando Metodo");
//
//        try {
//            StringBuilder soapXml = new StringBuilder();
//            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">");
//            soapXml.append("<soapenv:Header/>");
//            soapXml.append("<soapenv:Body>");
//            soapXml.append("<tem:getSujetosObligados_busqueda>");
//            soapXml.append("<tem:strSujetoObligado>").append(strSujetoObligado).append("</tem:strSujetoObligado>");
//            soapXml.append("<tem:intTipoBusqueda>").append(tipoBusqueda).append("</tem:intTipoBusqueda>");
//            soapXml.append("<tem:usuario>").append(usuario).append("</tem:usuario>");
//            soapXml.append("<tem:contrasenia>").append(contrasenia).append("</tem:contrasenia>");
//            soapXml.append("</tem:getSujetosObligados_busqueda>");
//            soapXml.append("</soapenv:Body>");
//            soapXml.append("</soapenv:Envelope>");
//
//            log.info("Consumiendo WS");
//            String requestLog = soapXml.toString();
//            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
//            log.debug("Request : " + reqs);
//
//            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),//"http://200.105.134.19:10080/wsrcbv2/servrcb.asmx",
//                    "text/xml;charset=UTF-8",
//                    "http://tempuri.org/getSujetosObligados_busqueda",
//                    soapXml.toString());
//            
//            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim())); // original
//            
//            log.debug("response : " + reps);
//
//            Namespace xs = Namespace.getNamespace("http://tempuri.org/");
//            element = response.getRootElement();
//            element = element.getChild("Body", element.getNamespace());
//            element = element.getChild("getSujetosObligados_busquedaResponse", xs);
//            element = element.getChild("getSujetosObligados_busquedaResult", xs);
//
//            List<Element> listElementSujetoObligado = element.getChildren("PersonaSO", xs);            
//            
//            for (Element elem : listElementSujetoObligado) {
//            	sujetoObligadoBusquedaBean = new SujetoObligadoBusquedaBean();
//            	                
//            	if (elem.getChildText("ErrCodigo", xs).equals("0")) {
//            		// System.out.println("estoy en el if ... ");
//                	sujetoObligadoBusquedaBean.setIdEntidad(elem.getChildText("IdEntidad", xs));
//                	sujetoObligadoBusquedaBean.setSujetoObligado(!"".equals(elem.getChildText("SujetoObligado", xs)) ? elem.getChildText("SujetoObligado", xs) : null);
//                	sujetoObligadoBusquedaBean.setTipoEmpresa(!"".equals(elem.getChildText("TipoEmpresa", xs)) ? elem.getChildText("TipoEmpresa", xs) : null);
//                	sujetoObligadoBusquedaBean.setTipoSujetoObligado(!"".equals(elem.getChildText("TipoSujetoObligado", xs)) ? elem.getChildText("TipoSujetoObligado", xs) : null);
//                	sujetoObligadoBusquedaBean.setOfcen_ciudad_localidad(!"".equals(elem.getChildText("OfcenCiudadLocalidad", xs)) ? elem.getChildText("OfcenCiudadLocalidad", xs) : null);
//                	sujetoObligadoBusquedaBean.setEstado(!"".equals(elem.getChildText("DescripcionEstado", xs)) ? elem.getChildText("DescripcionEstado", xs) : null);
//                	sujetoObligadoBusquedaBean.setErr_codigo(!"".equals(elem.getChildText("ErrCodigo", xs)) ? elem.getChildText("ErrCodigo", xs) : null);
//                	sujetoObligadoBusquedaBean.setErr_mensaje(!"".equals(elem.getChildText("ErrMensaje", xs)) ? elem.getChildText("ErrMensaje", xs) : null);
//            
//            	} else {
//                	// System.out.println("estoy en el else ... ");
//                	sujetoObligadoBusquedaBean.setErr_codigo(!"".equals(elem.getChildText("ErrCodigo", xs)) ? elem.getChildText("ErrCodigo", xs) : null);
//                	sujetoObligadoBusquedaBean.setErr_mensaje(!"".equals(elem.getChildText("ErrMensaje", xs)) ? elem.getChildText("ErrMensaje", xs) : null);
//                }
//                
//                listaSujetoObligado_busqueda.add(sujetoObligadoBusquedaBean);
//            }
//                 
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            log.catching(ex);
//            sujetoObligadoBusquedaBean = new SujetoObligadoBusquedaBean();
//            sujetoObligadoBusquedaBean.setErr_codigo("1");
//            sujetoObligadoBusquedaBean.setErr_mensaje("Error inesperado.");
//            listaSujetoObligado_busqueda.add(sujetoObligadoBusquedaBean);
//        }
//        log.info("finalizando Metodo");
//        return listaSujetoObligado_busqueda;
//    }
//    
//    public static List<SujetoObligadoDatosBean> getSujetosObligados_datos_dinamicos(int idEntidad, int nivCodigo, int dominfCodigo, String usuario, String contrasenia) {
//    	List<SujetoObligadoDatosBean> listaSujetoObligado_busqueda = new ArrayList<SujetoObligadoDatosBean>();
//    	SujetoObligadoDatosBean sujetoObligadoBusquedaBean = new SujetoObligadoDatosBean();
//        
//    	Element element;
//        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
//        log = LogManager.getLogger("CaronteClient.getSujetosObligados_busqueda");
//        log.info("Iniciando Metodo");
//
//        try {
//            StringBuilder soapXml = new StringBuilder();
//            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">");
//            soapXml.append("<soapenv:Header/>");
//            soapXml.append("<soapenv:Body>");
//            soapXml.append("<tem:getSujetosObligados_datos_dinamicos>");
//            soapXml.append("<tem:idEntidad>").append(idEntidad).append("</tem:idEntidad>");
//            soapXml.append("<tem:nivCodigo>").append(nivCodigo).append("</tem:nivCodigo>");
//            soapXml.append("<tem:dominfCodigo>").append(dominfCodigo).append("</tem:dominfCodigo>");
//            soapXml.append("<tem:usuario>").append(usuario).append("</tem:usuario>");
//            soapXml.append("<tem:contrasenia>").append(contrasenia).append("</tem:contrasenia>");
//            soapXml.append("</tem:getSujetosObligados_datos_dinamicos>");
//            soapXml.append("</soapenv:Body>");
//            soapXml.append("</soapenv:Envelope>");
//
//            log.info("Consumiendo WS");
//            String requestLog = soapXml.toString();
//            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
//            log.debug("Request : " + reqs);
//
//            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),//"http://200.105.134.19:10080/wsrcbv2/servrcb.asmx",
//                    "text/xml;charset=UTF-8",
//                    "http://tempuri.org/getSujetosObligados_datos_dinamicos",
//                    soapXml.toString());
//            
//            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim())); // original
//            
//            log.debug("response : " + reps);
//
//            Namespace xs = Namespace.getNamespace("http://tempuri.org/");
//            element = response.getRootElement();
//            element = element.getChild("Body", element.getNamespace());
//            element = element.getChild("getSujetosObligados_datos_dinamicosResponse", xs);
//            element = element.getChild("getSujetosObligados_datos_dinamicosResult", xs);
//
//            List<Element> listElementSujetoObligado = element.getChildren("PSujetoObligado", xs);            
//            
//            for (Element elem : listElementSujetoObligado) {
//            	sujetoObligadoBusquedaBean = new SujetoObligadoDatosBean();
//            	                
//            	if (elem.getChildText("ErrCodigo", xs).equals("0")) {
//            		// System.out.println("estoy en el if ... ");
//                	sujetoObligadoBusquedaBean.setCodigoEntidad(elem.getChildText("CodigoEntidad", xs));
//                	sujetoObligadoBusquedaBean.setDescripcionEntidad(!"".equals(elem.getChildText("DescripcionEntidad", xs)) ? elem.getChildText("DescripcionEntidad", xs) : null);
//                	sujetoObligadoBusquedaBean.setTipoEntidad(!"".equals(elem.getChildText("TipoEntidad", xs)) ? elem.getChildText("TipoEntidad", xs) : null);
//                	sujetoObligadoBusquedaBean.setOtroEntidad(!"".equals(elem.getChildText("OtroEntidad", xs)) ? elem.getChildText("OtroEntidad", xs) : null);
//                	sujetoObligadoBusquedaBean.setNitEntidad(!"".equals(elem.getChildText("NitEntidad", xs)) ? elem.getChildText("NitEntidad", xs) : null);
//                	sujetoObligadoBusquedaBean.setLugarEntidad(!"".equals(elem.getChildText("LugarEntidad", xs)) ? elem.getChildText("LugarEntidad", xs) : null);
//                	sujetoObligadoBusquedaBean.setZonaEntidad(!"".equals(elem.getChildText("ZonaEntidad", xs)) ? elem.getChildText("ZonaEntidad", xs) : null);
//                	sujetoObligadoBusquedaBean.setDireccionEntidad(!"".equals(elem.getChildText("DireccionEntidad", xs)) ? elem.getChildText("DireccionEntidad", xs) : null);
//                	sujetoObligadoBusquedaBean.setTelefonoEntidad(!"".equals(elem.getChildText("TelefonoEntidad", xs)) ? elem.getChildText("TelefonoEntidad", xs) : null);
//                	sujetoObligadoBusquedaBean.setCorreoEntidad(!"".equals(elem.getChildText("CorreoEntidad", xs)) ? elem.getChildText("CorreoEntidad", xs) : null);
//                	sujetoObligadoBusquedaBean.setTipoSujetoObligado(!"".equals(elem.getChildText("TipoSujetoObligado", xs)) ? elem.getChildText("TipoSujetoObligado", xs) : null);
//                	sujetoObligadoBusquedaBean.setErrCodigo(!"".equals(elem.getChildText("ErrCodigo", xs)) ? elem.getChildText("ErrCodigo", xs) : null);
//                	sujetoObligadoBusquedaBean.setErrMensaje(!"".equals(elem.getChildText("ErrMensaje", xs)) ? elem.getChildText("ErrMensaje", xs) : null);
//            
//            	} else {
//                	// System.out.println("estoy en el else ... ");
//            		sujetoObligadoBusquedaBean.setErrCodigo(!"".equals(elem.getChildText("ErrCodigo", xs)) ? elem.getChildText("ErrCodigo", xs) : null);
//                	sujetoObligadoBusquedaBean.setErrMensaje(!"".equals(elem.getChildText("ErrMensaje", xs)) ? elem.getChildText("ErrMensaje", xs) : null);
//                }
//                
//                listaSujetoObligado_busqueda.add(sujetoObligadoBusquedaBean);
//            }
//                 
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            log.catching(ex);
//            sujetoObligadoBusquedaBean = new SujetoObligadoDatosBean();
//            sujetoObligadoBusquedaBean.setErrCodigo("1");
//            sujetoObligadoBusquedaBean.setErrMensaje("Error inesperado.");
//            listaSujetoObligado_busqueda.add(sujetoObligadoBusquedaBean);
//        }
//        log.info("finalizando Metodo");
//        return listaSujetoObligado_busqueda;
//    }
//    
//    public static List<FuncionarioResponsableDatosBean> getFuncionarioResponsable_datos_dinamicos(int idEntidad, int nivCodigo, int dominfCodigo, String usuario, String contrasenia) {
//    	List<FuncionarioResponsableDatosBean> listaSujetoObligado_busqueda = new ArrayList<FuncionarioResponsableDatosBean>();
//    	FuncionarioResponsableDatosBean frBean = new FuncionarioResponsableDatosBean();
//        
//    	Element element;
//        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
//        log = LogManager.getLogger("CaronteClient.getSujetosObligados_busqueda");
//        log.info("Iniciando Metodo");
//        
//        try {
//            StringBuilder soapXml = new StringBuilder();
//            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">");
//            soapXml.append("<soapenv:Header/>");
//            soapXml.append("<soapenv:Body>");
//            soapXml.append("<tem:getFuncionarioResponsable_datos_dinamicos>");
//            soapXml.append("<tem:idEntidad>").append(idEntidad).append("</tem:idEntidad>");
//            soapXml.append("<tem:nivCodigo>").append(nivCodigo).append("</tem:nivCodigo>");
//            soapXml.append("<tem:dominfCodigo>").append(dominfCodigo).append("</tem:dominfCodigo>");
//            soapXml.append("<tem:usuario>").append(usuario).append("</tem:usuario>");
//            soapXml.append("<tem:contrasenia>").append(contrasenia).append("</tem:contrasenia>");
//            soapXml.append("</tem:getFuncionarioResponsable_datos_dinamicos>");
//            soapXml.append("</soapenv:Body>");
//            soapXml.append("</soapenv:Envelope>");
//
//            log.info("Consumiendo WS");
//            String requestLog = soapXml.toString();
//            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
//            log.debug("Request : " + reqs);
//
//            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),//"http://200.105.134.19:10080/wsrcbv2/servrcb.asmx",
//                    "text/xml;charset=UTF-8",
//                    "http://tempuri.org/getFuncionarioResponsable_datos_dinamicos",
//                    soapXml.toString());
//            
//            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim())); // original
//            
//            log.debug("response : " + reps);
//
//            Namespace xs = Namespace.getNamespace("http://tempuri.org/");
//            element = response.getRootElement();
//            element = element.getChild("Body", element.getNamespace());
//            element = element.getChild("getFuncionarioResponsable_datos_dinamicosResponse", xs);
//            element = element.getChild("getFuncionarioResponsable_datos_dinamicosResult", xs);
//
//            List<Element> listElementSujetoObligado = element.getChildren("PFuncionarioResponsable", xs);            
//            
//            for (Element elem : listElementSujetoObligado) {
//            	frBean = new FuncionarioResponsableDatosBean();
//            	                
//            	if (elem.getChildText("ErrCodigo", xs).equals("0")) {
//            		// System.out.println("estoy en el if ... ");
//            		frBean.setCodigoFuncionario(elem.getChildText("CodigoFuncionario", xs));
//            		frBean.setNombreFuncionario(!"".equals(elem.getChildText("NombreFuncionario", xs)) ? elem.getChildText("NombreFuncionario", xs) : null);
//            		frBean.setApPaternoFuncionario(!"".equals(elem.getChildText("ApPaternoFuncionario", xs)) ? elem.getChildText("ApPaternoFuncionario", xs) : null);
//            		frBean.setApMaternoFuncionario(!"".equals(elem.getChildText("ApMaternoFuncionario", xs)) ? elem.getChildText("ApMaternoFuncionario", xs) : null);
//            		frBean.setApCasadaFuncionario(!"".equals(elem.getChildText("ApCasadaFuncionario", xs)) ? elem.getChildText("ApCasadaFuncionario", xs) : null);
//            		frBean.setDocumentoFuncionario(!"".equals(elem.getChildText("DocumentoFuncionario", xs)) ? elem.getChildText("DocumentoFuncionario", xs) : null);
//            		frBean.setFechaNacimientoFuncionario(!"".equals(elem.getChildText("FechaNacimientoFuncionario", xs)) ? elem.getChildText("FechaNacimientoFuncionario", xs) : null);
//            		frBean.setTelefonoFuncionario(!"".equals(elem.getChildText("TelefonoFuncionario", xs)) ? elem.getChildText("TelefonoFuncionario", xs) : null);
//            		frBean.setInternoFuncionario(!"".equals(elem.getChildText("InternoFuncionario", xs)) ? elem.getChildText("InternoFuncionario", xs) : null);
//            		frBean.setErrCodigo(!"".equals(elem.getChildText("ErrCodigo", xs)) ? elem.getChildText("ErrCodigo", xs) : null);
//            		frBean.setErrMensaje(!"".equals(elem.getChildText("ErrMensaje", xs)) ? elem.getChildText("ErrMensaje", xs) : null);
//            		
//            	} else {
//                	// System.out.println("estoy en el else ... ");
//            		frBean.setErrCodigo(!"".equals(elem.getChildText("ErrCodigo", xs)) ? elem.getChildText("ErrCodigo", xs) : null);
//            		frBean.setErrMensaje(!"".equals(elem.getChildText("ErrMensaje", xs)) ? elem.getChildText("ErrMensaje", xs) : null);
//                }
//                
//                listaSujetoObligado_busqueda.add(frBean);
//            }
//                 
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            log.catching(ex);
//            frBean = new FuncionarioResponsableDatosBean();
//            frBean.setErrCodigo("1");
//            frBean.setErrMensaje("Error inesperado.");
//            listaSujetoObligado_busqueda.add(frBean);
//        }
//        log.info("finalizando Metodo");
//        return listaSujetoObligado_busqueda;
//    }
//    
//    public static List<AnalistaCumplimientoDatosBean> getAnalistaCumplimiento_datos_dinamicos(int idEntidad, int nivCodigo, int dominfCodigo, String usuario, String contrasenia) {
//    	List<AnalistaCumplimientoDatosBean> listaSujetoObligado_busqueda = new ArrayList<AnalistaCumplimientoDatosBean>();
//    	AnalistaCumplimientoDatosBean acBean = new AnalistaCumplimientoDatosBean();
//        
//    	Element element;
//        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
//        log = LogManager.getLogger("CaronteClient.getSujetosObligados_busqueda");
//        log.info("Iniciando Metodo");
//        
//        try {
//            StringBuilder soapXml = new StringBuilder();
//            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">");
//            soapXml.append("<soapenv:Header/>");
//            soapXml.append("<soapenv:Body>");
//            soapXml.append("<tem:getAnalistaCumplimiento_datos_dinamicos>");
//            soapXml.append("<tem:idEntidad>").append(idEntidad).append("</tem:idEntidad>");
//            soapXml.append("<tem:nivCodigo>").append(nivCodigo).append("</tem:nivCodigo>");
//            soapXml.append("<tem:dominfCodigo>").append(dominfCodigo).append("</tem:dominfCodigo>");
//            soapXml.append("<tem:usuario>").append(usuario).append("</tem:usuario>");
//            soapXml.append("<tem:contrasenia>").append(contrasenia).append("</tem:contrasenia>");
//            soapXml.append("</tem:getAnalistaCumplimiento_datos_dinamicos>");
//            soapXml.append("</soapenv:Body>");
//            soapXml.append("</soapenv:Envelope>");
//
//            log.info("Consumiendo WS");
//            String requestLog = soapXml.toString();
//            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
//            log.debug("Request : " + reqs);
//
//            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),//"http://200.105.134.19:10080/wsrcbv2/servrcb.asmx",
//                    "text/xml;charset=UTF-8",
//                    "http://tempuri.org/getAnalistaCumplimiento_datos_dinamicos",
//                    soapXml.toString());
//            
//            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim())); // original
//            
//            log.debug("response : " + reps);
//
//            Namespace xs = Namespace.getNamespace("http://tempuri.org/");
//            element = response.getRootElement();
//            element = element.getChild("Body", element.getNamespace());
//            element = element.getChild("getAnalistaCumplimiento_datos_dinamicosResponse", xs);
//            element = element.getChild("getAnalistaCumplimiento_datos_dinamicosResult", xs);
//
//            List<Element> listElementSujetoObligado = element.getChildren("PAnalistaCumplimiento", xs);            
//            
//            for (Element elem : listElementSujetoObligado) {
//            	acBean = new AnalistaCumplimientoDatosBean();
//            	                
//            	if (elem.getChildText("ErrCodigo", xs).equals("0")) {
//            		// System.out.println("estoy en el if ... ");
//            		acBean.setCodigoFuncionario(elem.getChildText("CodigoFuncionario", xs));
//            		acBean.setNombreFuncionario(!"".equals(elem.getChildText("NombreFuncionario", xs)) ? elem.getChildText("NombreFuncionario", xs) : null);
//            		acBean.setApPaternoFuncionario(!"".equals(elem.getChildText("ApPaternoFuncionario", xs)) ? elem.getChildText("ApPaternoFuncionario", xs) : null);
//            		acBean.setApMaternoFuncionario(!"".equals(elem.getChildText("ApMaternoFuncionario", xs)) ? elem.getChildText("ApMaternoFuncionario", xs) : null);
//            		acBean.setApCasadaFuncionario(!"".equals(elem.getChildText("ApCasadaFuncionario", xs)) ? elem.getChildText("ApCasadaFuncionario", xs) : null);
//            		acBean.setDocumentoFuncionario(!"".equals(elem.getChildText("DocumentoFuncionario", xs)) ? elem.getChildText("DocumentoFuncionario", xs) : null);
//            		acBean.setFechaNacimientoFuncionario(!"".equals(elem.getChildText("FechaNacimientoFuncionario", xs)) ? elem.getChildText("FechaNacimientoFuncionario", xs) : null);
//            		acBean.setTelefonoFuncionario(!"".equals(elem.getChildText("TelefonoFuncionario", xs)) ? elem.getChildText("TelefonoFuncionario", xs) : null);
//            		acBean.setInternoFuncionario(!"".equals(elem.getChildText("InternoFuncionario", xs)) ? elem.getChildText("InternoFuncionario", xs) : null);
//            		acBean.setErrCodigo(!"".equals(elem.getChildText("ErrCodigo", xs)) ? elem.getChildText("ErrCodigo", xs) : null);
//            		acBean.setErrMensaje(!"".equals(elem.getChildText("ErrMensaje", xs)) ? elem.getChildText("ErrMensaje", xs) : null);
//            		
//            	} else {
//                	// System.out.println("estoy en el else ... ");
//            		acBean.setErrCodigo(!"".equals(elem.getChildText("ErrCodigo", xs)) ? elem.getChildText("ErrCodigo", xs) : null);
//            		acBean.setErrMensaje(!"".equals(elem.getChildText("ErrMensaje", xs)) ? elem.getChildText("ErrMensaje", xs) : null);
//                }
//                
//                listaSujetoObligado_busqueda.add(acBean);
//            }
//                 
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            log.catching(ex);
//            acBean = new AnalistaCumplimientoDatosBean();
//            acBean.setErrCodigo("1");
//            acBean.setErrMensaje("Error inesperado.");
//            listaSujetoObligado_busqueda.add(acBean);
//        }
//        log.info("finalizando Metodo");
//        return listaSujetoObligado_busqueda;
//    }
//    
//    public static List<UsuarioServiciosBean> getUsuario_servicio(String usuario, String contrasenia) {
//    	List<UsuarioServiciosBean> listaSujetoObligado_busqueda = new ArrayList<UsuarioServiciosBean>();
//    	UsuarioServiciosBean acBean = new UsuarioServiciosBean();
//        
//    	Element element;
//        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
//        log = LogManager.getLogger("CaronteClient.getSujetosObligados_busqueda");
//        log.info("Iniciando Metodo");
//        
//        try {
//            StringBuilder soapXml = new StringBuilder();
//            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">");
//            soapXml.append("<soapenv:Header/>");
//            soapXml.append("<soapenv:Body>");
//            soapXml.append("<tem:getUsuario_servicio>");
//            soapXml.append("<tem:usuario>").append(usuario).append("</tem:usuario>");
//            soapXml.append("<tem:contrasenia>").append(contrasenia).append("</tem:contrasenia>");
//            soapXml.append("</tem:getUsuario_servicio>");
//            soapXml.append("</soapenv:Body>");
//            soapXml.append("</soapenv:Envelope>");
//
//            log.info("Consumiendo WS");
//            String requestLog = soapXml.toString();
//            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
//            log.debug("Request : " + reqs);
//
//            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),//"http://200.105.134.19:10080/wsrcbv2/servrcb.asmx",
//                    "text/xml;charset=UTF-8",
//                    "http://tempuri.org/getUsuario_servicio",
//                    soapXml.toString());
//            
//            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim())); // original
//            
//            log.debug("response : " + reps);
//
//            Namespace xs = Namespace.getNamespace("http://tempuri.org/");
//            element = response.getRootElement();
//            element = element.getChild("Body", element.getNamespace());
//            element = element.getChild("getUsuario_servicioResponse", xs);
//            element = element.getChild("getUsuario_servicioResult", xs);
//
//            List<Element> listElementSujetoObligado = element.getChildren("UsuarioServicios", xs);            
//            
//            for (Element elem : listElementSujetoObligado) {
//            	acBean = new UsuarioServiciosBean();
//            	                
//            	if (elem.getChildText("ErrCodigo", xs).equals("0")) {
//            		// System.out.println("estoy en el if ... ");
//            		acBean.setAlias(!"".equals(elem.getChildText("Alias", xs)) ? elem.getChildText("Alias", xs) : null);
//            		acBean.setFechaRegistro(!"".equals(elem.getChildText("FechaRegistro", xs)) ? elem.getChildText("FechaRegistro", xs) : null);
//            		acBean.setEstado(!"".equals(elem.getChildText("Estado", xs)) ? elem.getChildText("Estado", xs) : null);
//            		acBean.setEstadoDescripcion(!"".equals(elem.getChildText("EstadoDescripcion", xs)) ? elem.getChildText("EstadoDescripcion", xs) : null);
//            		acBean.setErrCodigo(!"".equals(elem.getChildText("ErrCodigo", xs)) ? elem.getChildText("ErrCodigo", xs) : null);
//            		acBean.setErrMensaje(!"".equals(elem.getChildText("ErrMensaje", xs)) ? elem.getChildText("ErrMensaje", xs) : null);
//            	} else {
//                	// System.out.println("estoy en el else ... ");
//            		acBean.setAlias(!"".equals(elem.getChildText("Alias", xs)) ? elem.getChildText("Alias", xs) : null);
//            		acBean.setFechaRegistro(!"".equals(elem.getChildText("FechaRegistro", xs)) ? elem.getChildText("FechaRegistro", xs) : null);
//            		acBean.setEstado(!"".equals(elem.getChildText("Estado", xs)) ? elem.getChildText("Estado", xs) : null);
//            		acBean.setEstadoDescripcion(!"".equals(elem.getChildText("EstadoDescripcion", xs)) ? elem.getChildText("EstadoDescripcion", xs) : null);
//            		acBean.setErrCodigo(!"".equals(elem.getChildText("ErrCodigo", xs)) ? elem.getChildText("ErrCodigo", xs) : null);
//            		acBean.setErrMensaje(!"".equals(elem.getChildText("ErrMensaje", xs)) ? elem.getChildText("ErrMensaje", xs) : null);
//                }
//                
//                listaSujetoObligado_busqueda.add(acBean);
//            }
//                 
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            log.catching(ex);
//            acBean = new UsuarioServiciosBean();
//            acBean.setErrCodigo("1");
//            acBean.setErrMensaje("Error inesperado.");
//            listaSujetoObligado_busqueda.add(acBean);
//        }
//        log.info("finalizando Metodo");
//        return listaSujetoObligado_busqueda;
//    }
//}
