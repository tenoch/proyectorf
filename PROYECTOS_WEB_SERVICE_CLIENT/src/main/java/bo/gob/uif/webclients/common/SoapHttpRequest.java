package bo.gob.uif.webclients.common;

import bo.gob.uif.webclients.exceptions.WebServiceConnectionException;
import bo.gob.uif.webclients.exceptions.WebServiceInternalException;
import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Vladimir.Callisaya on 11/08/2016.
 */
public class SoapHttpRequest {

    public static Document sendRequestPOST(String location, String contentType, String soapAction, String xmlRequest) throws Exception {
        Document result = null;
        try {
            URL endPoint = new URL(location);
            HttpURLConnection con = (HttpURLConnection) endPoint.openConnection();
            con.setDoOutput(true);

            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", contentType);
            con.setRequestProperty("SOAPAction", soapAction);

            OutputStream out = con.getOutputStream();
            out.write(xmlRequest.getBytes("UTF-8"));
            out.flush();
            out.close();

            int codResp = con.getResponseCode();
            //System.out.println("CODIGO REPSUESTA: " + codResp);

            if (codResp == HttpURLConnection.HTTP_OK) {
                InputStream input = con.getInputStream();
                SAXBuilder sax = new SAXBuilder();
                result = sax.build(input);
            } else if (codResp == HttpURLConnection.HTTP_INTERNAL_ERROR) {
                //throw new WebServiceInternalException("Error Web Service HttpUrlConnection " + codResp + ". "+con.getResponseMessage());
                InputStream input = con.getErrorStream();
                SAXBuilder sax = new SAXBuilder();
                result = sax.build(input);
                //System.out.println("respuestaMal: " + result.toString());
            } else {
                throw new WebServiceConnectionException("Error Web Service HttpUrlConnection " + codResp + ". " + con.getResponseMessage());
            }
        } catch (WebServiceConnectionException expWs) {
            throw expWs;
        }/*catch(WebServiceInternalException expWs){
         throw expWs;
         }*/ catch (Exception e) {
            throw new WebServiceConnectionException("Error Web Service Connection " + e.getMessage());
        }

        return result;
    }
}
