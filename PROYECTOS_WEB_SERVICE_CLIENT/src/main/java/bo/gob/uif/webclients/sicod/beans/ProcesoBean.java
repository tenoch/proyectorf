package bo.gob.uif.webclients.sicod.beans;

import java.util.Date;

/**
 * Created by Vladimir.Callisaya on 08/12/2016.
 */
public class ProcesoBean {

    private boolean respuesta;
    private String mensaje;
    private long idProceso;
    private Date fechaCreacionProceso;
    private String referencia;
    private long gestion;
    private String estado;

    public boolean isRespuesta() {
        return respuesta;
    }

    public void setRespuesta(boolean respuesta) {
        this.respuesta = respuesta;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public long getIdProceso() {
        return idProceso;
    }

    public void setIdProceso(long idProceso) {
        this.idProceso = idProceso;
    }

    public Date getFechaCreacionProceso() {
        return fechaCreacionProceso;
    }

    public void setFechaCreacionProceso(Date fechaCreacionProceso) {
        this.fechaCreacionProceso = fechaCreacionProceso;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public long getGestion() {
        return gestion;
    }

    public void setGestion(long gestion) {
        this.gestion = gestion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
