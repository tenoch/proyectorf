package bo.gob.uif.webclients.impuestos.beans;

import java.io.Serializable;

/**
 * Created by Vladimir.Callisaya on 10/08/2016.
 *
 * Modificado: Susana Escobar Paz Fecha: 17/05/2018
 */
public class PersonaNatural implements Serializable {

    private long nit;
    private String razonSocial;
    private String descripcionPersonaNatural;
    private Boolean conexion = true;

    public long getNit() {
        return nit;
    }

    public void setNit(long nit) {
        this.nit = nit;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDescripcionPersonaNatural() {
        return descripcionPersonaNatural;
    }

    public void setDescripcionPersonaNatural(String descripcionPersonaNatural) {
        this.descripcionPersonaNatural = descripcionPersonaNatural;
    }

    public Boolean getConexion() {
        return conexion;
    }

    public void setConexion(Boolean conexion) {
        this.conexion = conexion;
    }

}
