/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.gob.uif.webclients.inra.beans;

import java.io.Serializable;

/**
 *
 * @author Susana.Escobar
 *
 * Creado por: Susana Escobar Paz:
 *
 * Descripcion: mapeo de la entidad BeneficiarioBean para almacenado de parte de
 * la respuesta de la consulta al servicio rest del Inra, se usa en la
 * implementacion de metodos de la clase InraClient
 *
 * Fecha: 30/05/2018
 */
public class BeneficiarioBean implements Serializable {

    private String beneficiario;
    private String numeroIdentidad;
    private String fechaNacimiento;
    private String genero;
    private String tipoBeneficiario;
    private String estadoCivil;

    public String getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(String beneficiario) {
        this.beneficiario = beneficiario;
    }

    public String getNumeroIdentidad() {
        return numeroIdentidad;
    }

    public void setNumeroIdentidad(String numeroIdentidad) {
        this.numeroIdentidad = numeroIdentidad;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getTipoBeneficiario() {
        return tipoBeneficiario;
    }

    public void setTipoBeneficiario(String tipoBeneficiario) {
        this.tipoBeneficiario = tipoBeneficiario;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    @Override
    public String toString() {
        return "BeneficiarioBean{"
                + "beneficiario='" + beneficiario + '\''
                + ", numeroIdentidad='" + numeroIdentidad + '\''
                + ", fechaNacimiento='" + fechaNacimiento + '\''
                + ", genero='" + genero + '\''
                + ", tipoBeneficiario='" + tipoBeneficiario + '\''
                + ", estadoCivil=" + estadoCivil
                + '}';
    }
}
