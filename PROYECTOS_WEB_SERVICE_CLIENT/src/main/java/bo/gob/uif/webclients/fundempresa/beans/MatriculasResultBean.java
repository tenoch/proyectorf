package bo.gob.uif.webclients.fundempresa.beans;

import java.io.Serializable;

/**
 * Created by Vladimir.Callisaya on 05/09/2016.
 */
public class MatriculasResultBean implements Serializable {

    private String ctrResult;
    private String idMatricula;
    private String nit;
    private String razonSocial;
    private String ctrEstado;

    public String getCtrResult() {
        return ctrResult;
    }

    public void setCtrResult(String ctrResult) {
        this.ctrResult = ctrResult;
    }

    public String getIdMatricula() {
        return idMatricula;
    }

    public void setIdMatricula(String idMatricula) {
        this.idMatricula = idMatricula;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getCtrEstado() {
        return ctrEstado;
    }

    public void setCtrEstado(String ctrEstado) {
        this.ctrEstado = ctrEstado;
    }

    @Override
    public String toString() {
        return "MatriculasResultBean{"
                + "ctrResult='" + ctrResult + '\''
                + ", idMatricula='" + idMatricula + '\''
                + ", nit='" + nit + '\''
                + ", razonSocial='" + razonSocial + '\''
                + ", ctrEstado='" + ctrEstado + '\''
                + '}';
    }
}
