package bo.gob.uif.webclients.caronte.beans;

import java.io.Serializable;
import java.util.List;

import bo.gob.uif.webclients.fundempresa.beans.MatriculaDatosSucBean;

public class SujetoObligadoBusquedaBean implements Serializable {

    private String idEntidad;

    private String sujetoObligado;
    private String tipoEmpresa;
    private String tipoSujetoObligado;

    private String ofcen_ciudad_localidad;
    private String estado;
    private String err_codigo;
    private String err_mensaje;

    public String getSujetoObligado() {
        return sujetoObligado;
    }

    public void setSujetoObligado(String sujetoObligado) {
        this.sujetoObligado = sujetoObligado;
    }

    public String getTipoEmpresa() {
        return tipoEmpresa;
    }

    public void setTipoEmpresa(String tipoEmpresa) {
        this.tipoEmpresa = tipoEmpresa;
    }

    public String getOfcen_ciudad_localidad() {
        return ofcen_ciudad_localidad;
    }

    public void setOfcen_ciudad_localidad(String ofcen_ciudad_localidad) {
        this.ofcen_ciudad_localidad = ofcen_ciudad_localidad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getErr_mensaje() {
        return err_mensaje;
    }

    public void setErr_mensaje(String err_mensaje) {
        this.err_mensaje = err_mensaje;
    }

    public String getTipoSujetoObligado() {
        return tipoSujetoObligado;
    }

    public void setTipoSujetoObligado(String tipoSujetoObligado) {
        this.tipoSujetoObligado = tipoSujetoObligado;
    }

    public String getIdEntidad() {
        return idEntidad;
    }

    public void setIdEntidad(String idEntidad) {
        this.idEntidad = idEntidad;
    }

    public String getErr_codigo() {
        return err_codigo;
    }

    public void setErr_codigo(String err_codigo) {
        this.err_codigo = err_codigo;
    }
}
