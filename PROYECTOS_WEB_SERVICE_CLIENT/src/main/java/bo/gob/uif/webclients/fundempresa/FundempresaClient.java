package bo.gob.uif.webclients.fundempresa;

import bo.gob.uif.webclients.cfg.MainWebServicesConfig;
import bo.gob.uif.webclients.cfg.WebServiceConfig;
import bo.gob.uif.webclients.common.SoapHttpRequest;
import bo.gob.uif.webclients.common.StringTokenizer;
import bo.gob.uif.webclients.fundempresa.beans.*;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.output.XMLOutputter;

/**
 * Created by Vladimir.Callisaya on 01/09/2016.
 *
 * Modificado: Susana Escobar Paz Fecha: 17/05/2018
 */
public class FundempresaClient {

    public static WebServiceConfig wsConfig = MainWebServicesConfig.INSTANCE.getWebServiceConfig("FUNDEMPRESAConsultaSerWeb");
    private static Logger log;
    private static StringTokenizer st = new StringTokenizer();

    public static List<MatriculaActividadBean> srvActividades(String idMatricula) {
        List<MatriculaActividadBean> listaMatriculaActividad = new ArrayList<MatriculaActividadBean>();
        //String idContrato = "02UIF032016";
        //String keyContrato = "34{mPTwm?Htqj)8";
        MatriculaActividadBean matriculaActividadBean = new MatriculaActividadBean();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("FundempresaClient.srvActividades");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wsr=\"http://www.fundempresa.org.bo:10080/wsrcbv2\">");
            soapXml.append("<soap:Header/>");
            soapXml.append("<soap:Body>");
            soapXml.append("<wsr:SrvActividades>");
            soapXml.append("<wsr:idContrato>").append(wsConfig.getIdContract()).append("</wsr:idContrato>");
            soapXml.append("<wsr:keyContrato>").append(wsConfig.getKeyContract()).append("</wsr:keyContrato>");
            soapXml.append("<wsr:IdMatricula>").append(idMatricula).append("</wsr:IdMatricula>");
            soapXml.append("</wsr:SrvActividades>");
            soapXml.append("</soap:Body>");
            soapXml.append("</soap:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),//"http://200.105.134.19:10080/wsrcbv2/servrcb.asmx",
                    "text/xml;charset=UTF-8",
                    "http://www.fundempresa.org.bo:10080/wsrcbv2/SrvActividades",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + reps);

            Namespace ns = Namespace.getNamespace("http://www.fundempresa.org.bo:10080/wsrcbv2");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("SrvActividadesResponse", ns);
            element = element.getChild("SrvActividadesResult", ns);
            List<Element> listElementMatriculaActividad = element.getChildren("MatriculaActividad", ns);

            for (Element elem : listElementMatriculaActividad) {
                matriculaActividadBean = new MatriculaActividadBean();

                if (elem.getChildText("CtrResult", ns).equals("D-EXIST")) {
                    matriculaActividadBean.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                    matriculaActividadBean.setIdMatricula(!"".equals(elem.getChildText("IdMatricula", ns)) ? elem.getChildText("IdMatricula", ns) : null);
                    matriculaActividadBean.setTxtActividad(!"".equals(elem.getChildText("TxtActividad", ns)) ? elem.getChildText("TxtActividad", ns) : null);
                    matriculaActividadBean.setSeccion(!"".equals(elem.getChildText("Seccion", ns)) ? elem.getChildText("Seccion", ns) : null);
                    matriculaActividadBean.setDivision(!"".equals(elem.getChildText("Division", ns)) ? elem.getChildText("Division", ns) : null);
                    matriculaActividadBean.setClase(!"".equals(elem.getChildText("Clase", ns)) ? elem.getChildText("Clase", ns) : null);
                    matriculaActividadBean.setVersionClasificador(!"".equals(elem.getChildText("VersionClasificador", ns)) ? elem.getChildText("VersionClasificador", ns) : null);
                } else {
                    matriculaActividadBean.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                }
                listaMatriculaActividad.add(matriculaActividadBean);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
            log.catching(ex);
            matriculaActividadBean = new MatriculaActividadBean();
            matriculaActividadBean.setCtrResult("0");
            listaMatriculaActividad.add(matriculaActividadBean);
        }
        log.info("finalizando Metodo");
        return listaMatriculaActividad;
    }

    public static List<InfoMatriculaBean> srvInfoMatricula(String idMatricula) {
        List<InfoMatriculaBean> listaInfoMatricula = new ArrayList<InfoMatriculaBean>();
        //String idContrato = "02UIF032016";
        //String keyContrato = "34{mPTwm?Htqj)8";
        InfoMatriculaBean infoMatricula = new InfoMatriculaBean();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("FundempresaClient.srvInfoMatricula");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wsr=\"http://www.fundempresa.org.bo:10080/wsrcbv2\">");
            soapXml.append("<soap:Header/>");
            soapXml.append("<soap:Body>");
            soapXml.append("<wsr:SrvInfoMatricula>");
            soapXml.append("<wsr:idContrato>").append(wsConfig.getIdContract()).append("</wsr:idContrato>");
            soapXml.append("<wsr:keyContrato>").append(wsConfig.getKeyContract()).append("</wsr:keyContrato>");
            soapXml.append("<wsr:IdMatricula>").append(idMatricula).append("</wsr:IdMatricula>");
            soapXml.append("</wsr:SrvInfoMatricula>");
            soapXml.append("</soap:Body>");
            soapXml.append("</soap:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),//"http://200.105.134.19:10080/wsrcbv2/servrcb.asmx",
                    "text/xml;charset=UTF-8",
                    "http://www.fundempresa.org.bo:10080/wsrcbv2/SrvInfoMatricula",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + reps);

            Namespace ns = Namespace.getNamespace("http://www.fundempresa.org.bo:10080/wsrcbv2");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("SrvInfoMatriculaResponse", ns);
            element = element.getChild("SrvInfoMatriculaResult", ns);
            List<Element> listElementInfoMatricula = element.getChildren("infoMatricula", ns);

            for (Element elem : listElementInfoMatricula) {
                infoMatricula = new InfoMatriculaBean();
                //List<MatriculaDatosSucBean> listaMatriculaDatosSucBean = new ArrayList<MatriculaDatosSucBean>();
                if (elem.getChildText("CtrResult", ns).equals("D-EXIST")) {
                    infoMatricula.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                    infoMatricula.setIdMatricula(!"".equals(elem.getChildText("IdMatricula", ns)) ? elem.getChildText("IdMatricula", ns) : null);
                    infoMatricula.setRazonSocial(!"".equals(elem.getChildText("RazonSocial", ns)) ? elem.getChildText("RazonSocial", ns) : null);
                    infoMatricula.setTipoSocietario(!"".equals(elem.getChildText("TipoSocietario", ns)) ? elem.getChildText("TipoSocietario", ns) : null);
                    infoMatricula.setFechaInscripcion(!"".equals(elem.getChildText("FechaInscripcion", ns)) ? elem.getChildText("FechaInscripcion", ns) : null);
                    infoMatricula.setFechaUltRenovacion(!"".equals(elem.getChildText("FechaUltRenovacion", ns)) ? elem.getChildText("FechaUltRenovacion", ns) : null);
                    infoMatricula.setUltGestionRenovada(!"".equals(elem.getChildText("UltGestionRenovada", ns)) ? elem.getChildText("UltGestionRenovada", ns) : null);
                    infoMatricula.setNit(!"".equals(elem.getChildText("Nit", ns)) ? elem.getChildText("Nit", ns) : null);
                    infoMatricula.setDepartamento(!"".equals(elem.getChildText("Departamento", ns)) ? elem.getChildText("Departamento", ns) : null);
                    infoMatricula.setProvincia(!"".equals(elem.getChildText("Provincia", ns)) ? elem.getChildText("Provincia", ns) : null);
                    infoMatricula.setMunicipio(!"".equals(elem.getChildText("Municipio", ns)) ? elem.getChildText("Municipio", ns) : null);
                    infoMatricula.setCalleAv(!"".equals(elem.getChildText("CalleAv", ns)) ? elem.getChildText("CalleAv", ns) : null);
                    infoMatricula.setEntreCalles(!"".equals(elem.getChildText("EntreCalles", ns)) ? elem.getChildText("EntreCalles", ns) : null);
                    infoMatricula.setNro(!"".equals(elem.getChildText("Nro", ns)) ? elem.getChildText("Nro", ns) : null);
                    infoMatricula.setUv(!"".equals(elem.getChildText("Uv", ns)) ? elem.getChildText("Uv", ns) : null);
                    infoMatricula.setMza(!"".equals(elem.getChildText("Mza", ns)) ? elem.getChildText("Mza", ns) : null);
                    infoMatricula.setEdificio(!"".equals(elem.getChildText("Edificio", ns)) ? elem.getChildText("Edificio", ns) : null);
                    infoMatricula.setPiso(!"".equals(elem.getChildText("Piso", ns)) ? elem.getChildText("Piso", ns) : null);
                    infoMatricula.setNroOficina(!"".equals(elem.getChildText("NroOficina", ns)) ? elem.getChildText("NroOficina", ns) : null);
                    infoMatricula.setZona(!"".equals(elem.getChildText("Zona", ns)) ? elem.getChildText("Zona", ns) : null);
                    infoMatricula.setCorreoElectronico(!"".equals(elem.getChildText("CorreoElectronico", ns)) ? elem.getChildText("CorreoElectronico", ns) : null);
                    infoMatricula.setActividad(!"".equals(elem.getChildText("Actividad", ns)) ? elem.getChildText("Actividad", ns) : null);
                    infoMatricula.setClaseCIIU(!"".equals(elem.getChildText("ClaseCIIU", ns)) ? elem.getChildText("ClaseCIIU", ns) : null);
                    infoMatricula.setCtrEstado(!"".equals(elem.getChildText("CtrEstado", ns)) ? elem.getChildText("CtrEstado", ns) : null);

                    element = elem.getChild("MatriculaDatosSucList1", ns);
                    List<Element> elemMatDatSuc = element.getChildren("MatriculaDatosSuc", ns);
                    List<MatriculaDatosSucBean> listaSuc = new ArrayList<MatriculaDatosSucBean>();
                    for (Element elemSuc : elemMatDatSuc) {
                        MatriculaDatosSucBean mat = new MatriculaDatosSucBean();
                        mat.setIdSuc(!"".equals(elemSuc.getChildText("IdSuc", ns)) ? Integer.parseInt(elemSuc.getChildText("IdSuc", ns)) : null);
                        mat.setSucursal(!"".equals(elemSuc.getChildText("Sucursal", ns)) ? elemSuc.getChildText("Sucursal", ns) : null);
                        mat.setDepartamento(!"".equals(elemSuc.getChildText("Departamento", ns)) ? elemSuc.getChildText("Departamento", ns) : null);
                        mat.setMunicipio(!"".equals(elemSuc.getChildText("Municipio", ns)) ? elemSuc.getChildText("Municipio", ns) : null);
                        mat.setProvincia(!"".equals(elemSuc.getChildText("Provincia", ns)) ? elemSuc.getChildText("Provincia", ns) : null);
                        mat.setDireccion(!"".equals(elemSuc.getChildText("Direccion", ns)) ? elemSuc.getChildText("Direccion", ns) : null);
                        mat.setZona(!"".equals(elemSuc.getChildText("Zona", ns)) ? elemSuc.getChildText("Zona", ns) : null);
                        mat.setTelefono(!"".equals(elemSuc.getChildText("Telefono", ns)) ? elemSuc.getChildText("Telefono", ns) : null);
                        mat.setIdClase(!"".equals(elemSuc.getChildText("IdClase", ns)) ? elemSuc.getChildText("IdClase", ns) : null);
                        mat.setNumId(!"".equals(elemSuc.getChildText("NumId", ns)) ? elemSuc.getChildText("NumId", ns) : null);
                        mat.setRepresentante(!"".equals(elemSuc.getChildText("Representante", ns)) ? elemSuc.getChildText("Representante", ns) : null);
                        listaSuc.add(mat);
                    }

                    infoMatricula.setMatriculaDatosSucList(listaSuc);

                } else {
                    infoMatricula.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                }

                listaInfoMatricula.add(infoMatricula);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
            log.catching(ex);
            infoMatricula = new InfoMatriculaBean();
            infoMatricula.setCtrResult("0");
            listaInfoMatricula.add(infoMatricula);
        }
        log.info("Finalizando Metodo");
        return listaInfoMatricula;
    }

    public static List<MatriculasResultBean> srvMatriculaConsultaNit(String nit) {
        List<MatriculasResultBean> listaMatriculasResultbean = new ArrayList<MatriculasResultBean>();
        //String idContrato = "02UIF032016";
        //String keyContrato = "34{mPTwm?Htqj)8";
        MatriculasResultBean matriculasResultBean = new MatriculasResultBean();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("FundempresaClient.srvMatriculaConsultaNit");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsr=\"http://www.fundempresa.org.bo:10080/wsrcbv2\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append(" <soapenv:Body>");
            soapXml.append("<wsr:SrvMatriculaConsultaNit>");
            soapXml.append("<wsr:idContrato>").append(wsConfig.getIdContract()).append("</wsr:idContrato>");
            soapXml.append("<wsr:keyContrato>").append(wsConfig.getKeyContract()).append("</wsr:keyContrato>");
            soapXml.append("<wsr:IdNit>").append(nit).append("</wsr:IdNit>");
            soapXml.append("</wsr:SrvMatriculaConsultaNit>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),//"http://200.105.134.19:10080/wsrcbv2/servrcb.asmx",
                    "text/xml;charset=UTF-8",
                    "http://www.fundempresa.org.bo:10080/wsrcbv2/SrvMatriculaConsultaNit",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + reps);

            Namespace ns = Namespace.getNamespace("http://www.fundempresa.org.bo:10080/wsrcbv2");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("SrvMatriculaConsultaNitResponse", ns);
            element = element.getChild("SrvMatriculaConsultaNitResult", ns);
            List<Element> listElementMatriculasresult = element.getChildren("MatriculasResult", ns);

            for (Element elem : listElementMatriculasresult) {
                matriculasResultBean = new MatriculasResultBean();

                if (elem.getChildText("CtrResult", ns).equals("D-EXIST")) {
                    matriculasResultBean.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                    matriculasResultBean.setIdMatricula(!"".equals(elem.getChildText("IdMatricula", ns)) ? elem.getChildText("IdMatricula", ns) : null);
                    matriculasResultBean.setNit(!"".equals(elem.getChildText("Nit", ns)) ? elem.getChildText("Nit", ns) : null);
                    matriculasResultBean.setRazonSocial(!"".equals(elem.getChildText("RazonSocial", ns)) ? elem.getChildText("RazonSocial", ns) : null);
                    matriculasResultBean.setCtrEstado(!"".equals(elem.getChildText("CtrEstado", ns)) ? elem.getChildText("CtrEstado", ns) : null);
                } else {
                    matriculasResultBean.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                }
                listaMatriculasResultbean.add(matriculasResultBean);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();            
            matriculasResultBean = new MatriculasResultBean();
            matriculasResultBean.setCtrResult("0");
            listaMatriculasResultbean.add(matriculasResultBean);
            log.catching(ex);
        }
        log.info("Finalizando Metodo");
        return listaMatriculasResultbean;
    }

    public static List<BusquedaRazonSocialBean> srvMatriculaConsultaRazon(String consulta) {
        List<BusquedaRazonSocialBean> listaBusquedaRazonSocialBean = new ArrayList<BusquedaRazonSocialBean>();
        //String idContrato = "02UIF032016";
        //String keyContrato = "34{mPTwm?Htqj)8";
        BusquedaRazonSocialBean busquedaRazonSocialBean = new BusquedaRazonSocialBean();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("FundempresaClient.srvMatriculaConsultaRazon");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wsr=\"http://www.fundempresa.org.bo:10080/wsrcbv2\">");
            soapXml.append("<soap:Header/>");
            soapXml.append("<soap:Body>");
            soapXml.append("<wsr:SrvMatriculaConsultaRazon>");
            soapXml.append("<wsr:idContrato>").append(wsConfig.getIdContract()).append("</wsr:idContrato>");
            soapXml.append("<wsr:keyContrato>").append(wsConfig.getKeyContract()).append("</wsr:keyContrato>");
            soapXml.append("<wsr:txtConsulta>").append(consulta).append("</wsr:txtConsulta>");
            soapXml.append("</wsr:SrvMatriculaConsultaRazon>");
            soapXml.append("</soap:Body>");
            soapXml.append("</soap:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),//"http://200.105.134.19:10080/wsrcbv2/servrcb.asmx",
                    "text/xml;charset=UTF-8",
                    "http://www.fundempresa.org.bo:10080/wsrcbv2/SrvMatriculaConsultaRazon",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + reps);

            Namespace ns = Namespace.getNamespace("http://www.fundempresa.org.bo:10080/wsrcbv2");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("SrvMatriculaConsultaRazonResponse", ns);
            element = element.getChild("SrvMatriculaConsultaRazonResult", ns);
            List<Element> listElementBusquedaRazonSocial = element.getChildren("busquedaRazonSocial", ns);

            for (Element elem : listElementBusquedaRazonSocial) {
                busquedaRazonSocialBean = new BusquedaRazonSocialBean();
                if (elem.getChildText("CtrResult", ns).equals("D-EXIST")) {
                    busquedaRazonSocialBean.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                    busquedaRazonSocialBean.setIdMatricula(!"".equals(elem.getChildText("IdMatricula", ns)) ? elem.getChildText("IdMatricula", ns) : null);
                    busquedaRazonSocialBean.setRazonSocial(!"".equals(elem.getChildText("RazonSocial", ns)) ? elem.getChildText("RazonSocial", ns) : null);
                    busquedaRazonSocialBean.setNit(!"".equals(elem.getChildText("Nit", ns)) ? elem.getChildText("Nit", ns) : null);
                } else {
                    busquedaRazonSocialBean.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                }
                listaBusquedaRazonSocialBean.add(busquedaRazonSocialBean);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
            busquedaRazonSocialBean = new BusquedaRazonSocialBean();
            busquedaRazonSocialBean.setCtrResult("0");
            listaBusquedaRazonSocialBean.add(busquedaRazonSocialBean);
            log.catching(ex);
        }
        log.info("finalizando Metodo");
        return listaBusquedaRazonSocialBean;
    }

    public static List<MatriculaResultSucBean> srvMatriculaListSuc(String idMatricula) {
        List<MatriculaResultSucBean> listaMatriculaResultSucBean = new ArrayList<MatriculaResultSucBean>();
        //String idContrato = "02UIF032016";
        //String keyContrato = "34{mPTwm?Htqj)8";
        MatriculaResultSucBean matriculaResultSucBean = new MatriculaResultSucBean();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("FundempresaClient.srvMatriculaListSuc");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wsr=\"http://www.fundempresa.org.bo:10080/wsrcbv2\">");
            soapXml.append("<soap:Header/>");
            soapXml.append("<soap:Body>");
            soapXml.append("<wsr:SrvMatriculaListSuc>");
            soapXml.append("<wsr:idContrato>").append(wsConfig.getIdContract()).append("</wsr:idContrato>");
            soapXml.append("<wsr:keyContrato>").append(wsConfig.getKeyContract()).append("</wsr:keyContrato>");
            soapXml.append("<wsr:IdMatricula>").append(idMatricula).append("</wsr:IdMatricula>");
            soapXml.append("</wsr:SrvMatriculaListSuc>\n");
            soapXml.append("</soap:Body>");
            soapXml.append("</soap:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),//"http://200.105.134.19:10080/wsrcbv2/servrcb.asmx",
                    "text/xml;charset=UTF-8",
                    "http://www.fundempresa.org.bo:10080/wsrcbv2/SrvMatriculaListSuc",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + reps);

            Namespace ns = Namespace.getNamespace("http://www.fundempresa.org.bo:10080/wsrcbv2");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("SrvMatriculaListSucResponse", ns);
            element = element.getChild("SrvMatriculaListSucResult", ns);
            List<Element> listElementMatriculaResultSuc = element.getChildren("MatriculaResultSuc", ns);

            for (Element elem : listElementMatriculaResultSuc) {
                matriculaResultSucBean = new MatriculaResultSucBean();
                if (elem.getChildText("CtrResult", ns).equals("D-EXIST")) {
                    matriculaResultSucBean.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                    matriculaResultSucBean.setIdMatricula(!"".equals(elem.getChildText("IdMatricula", ns)) ? elem.getChildText("IdMatricula", ns) : null);
                    matriculaResultSucBean.setRazonSocial(!"".equals(elem.getChildText("RazonSocial", ns)) ? elem.getChildText("RazonSocial", ns) : null);
                    matriculaResultSucBean.setFechaInscripcion(!"".equals(elem.getChildText("FechaInscripcion", ns)) ? elem.getChildText("FechaInscripcion", ns) : null);
                    matriculaResultSucBean.setFechaUltRenovacion(!"".equals(elem.getChildText("FechaUltRenovacion", ns)) ? elem.getChildText("FechaUltRenovacion", ns) : null);
                    matriculaResultSucBean.setUltGestionRenovada(!"".equals(elem.getChildText("UltGestionRenovada", ns)) ? elem.getChildText("UltGestionRenovada", ns) : null);
                    matriculaResultSucBean.setNit(!"".equals(elem.getChildText("Nit", ns)) ? elem.getChildText("Nit", ns) : null);
                    matriculaResultSucBean.setCtrEstado(!"".equals(elem.getChildText("CtrEstado", ns)) ? elem.getChildText("CtrEstado", ns) : null);

                    element = elem.getChild("MatriculaDatosSucList1", ns);
                    List<Element> elemMatDatSuc = element.getChildren("MatriculaDatosSuc", ns);
                    List<MatriculaDatosSucBean> listaSuc = new ArrayList<MatriculaDatosSucBean>();
                    for (Element el : elemMatDatSuc) {
                        MatriculaDatosSucBean mat = new MatriculaDatosSucBean();
                        mat.setIdSuc(!"".equals(el.getChildText("IdSuc", ns)) ? Integer.parseInt(el.getChildText("IdSuc", ns)) : null);
                        mat.setSucursal(!"".equals(el.getChildText("Sucursal", ns)) ? el.getChildText("Sucursal", ns) : null);
                        mat.setDepartamento(!"".equals(el.getChildText("Departamento", ns)) ? el.getChildText("Departamento", ns) : null);
                        mat.setMunicipio(!"".equals(el.getChildText("Municipio", ns)) ? el.getChildText("Municipio", ns) : null);
                        mat.setProvincia(!"".equals(el.getChildText("Provincia", ns)) ? el.getChildText("Provincia", ns) : null);
                        mat.setDireccion(!"".equals(el.getChildText("Direccion", ns)) ? el.getChildText("Direccion", ns) : null);
                        mat.setZona(!"".equals(el.getChildText("Zona", ns)) ? el.getChildText("Zona", ns) : null);
                        mat.setTelefono(!"".equals(el.getChildText("Telefono", ns)) ? el.getChildText("Telefono", ns) : null);
                        mat.setIdClase(!"".equals(el.getChildText("IdClase", ns)) ? el.getChildText("IdClase", ns) : null);
                        mat.setNumId(!"".equals(el.getChildText("NumId", ns)) ? el.getChildText("NumId", ns) : null);
                        mat.setRepresentante(!"".equals(el.getChildText("Representante", ns)) ? el.getChildText("Representante", ns) : null);
                        listaSuc.add(mat);

                    }
                    matriculaResultSucBean.setListaMatriculaDatosSucBean(listaSuc);

                } else {
                    matriculaResultSucBean.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                }
                listaMatriculaResultSucBean.add(matriculaResultSucBean);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
            log.catching(ex);
            matriculaResultSucBean = new MatriculaResultSucBean();
            matriculaResultSucBean.setCtrResult("0");
            listaMatriculaResultSucBean.add(matriculaResultSucBean);
        }
        log.info("Finalizando Metodo");
        return listaMatriculaResultSucBean;
    }

    public static List<MatriculaDatosVigenciaBean> srvMatriculaVigencia(String idMatricula) {
        List<MatriculaDatosVigenciaBean> listaMatDatVig = new ArrayList<MatriculaDatosVigenciaBean>();
        //String idContrato = "02UIF032016";
        //String keyContrato = "34{mPTwm?Htqj)8";
        MatriculaDatosVigenciaBean tmpMatDatVig = new MatriculaDatosVigenciaBean();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("FundempresaClient.srvMatriculaVigencia");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wsr=\"http://www.fundempresa.org.bo:10080/wsrcbv2\">");
            soapXml.append("<soap:Header/>");
            soapXml.append("<soap:Body>");
            soapXml.append("<wsr:SrvMatriculaVigencia>");
            soapXml.append("<wsr:idContrato>").append(wsConfig.getIdContract()).append("</wsr:idContrato>");
            soapXml.append("<wsr:keyContrato>").append(wsConfig.getKeyContract()).append("</wsr:keyContrato>");
            soapXml.append("<wsr:IdMatricula>").append(idMatricula).append("</wsr:IdMatricula>");
            soapXml.append("</wsr:SrvMatriculaVigencia>");
            soapXml.append("</soap:Body>");
            soapXml.append("</soap:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),//"http://200.105.134.19:10080/wsrcbv2/servrcb.asmx",
                    "text/xml;charset=UTF-8",
                    "http://www.fundempresa.org.bo:10080/wsrcbv2/SrvMatriculaVigencia",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + reps);

            Namespace ns = Namespace.getNamespace("http://www.fundempresa.org.bo:10080/wsrcbv2");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("SrvMatriculaVigenciaResponse", ns);
            element = element.getChild("SrvMatriculaVigenciaResult", ns);
            List<Element> listElementMatriculaDatosVigencia = element.getChildren("MatriculaDatosVigencia", ns);

            for (Element elem : listElementMatriculaDatosVigencia) {
                tmpMatDatVig = new MatriculaDatosVigenciaBean();
                if (elem.getChildText("CtrResult", ns).equals("D-EXIST")) {
                    tmpMatDatVig.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                    tmpMatDatVig.setIdMatricula(!"".equals(elem.getChildText("IdMatricula", ns)) ? elem.getChildText("IdMatricula", ns) : null);
                    tmpMatDatVig.setRazonSocial(!"".equals(elem.getChildText("RazonSocial", ns)) ? elem.getChildText("RazonSocial", ns) : null);
                    tmpMatDatVig.setFechaInscripcion(!"".equals(elem.getChildText("FechaInscripcion", ns)) ? elem.getChildText("FechaInscripcion", ns) : null);
                    tmpMatDatVig.setFechaUltRenovacion(!"".equals(elem.getChildText("FechaUltRenovacion", ns)) ? elem.getChildText("FechaUltRenovacion", ns) : null);
                    tmpMatDatVig.setUltGestionRenovada(!"".equals(elem.getChildText("UltGestionRenovada", ns)) ? elem.getChildText("UltGestionRenovada", ns) : null);
                    tmpMatDatVig.setVigenciaMatricula(!"".equals(elem.getChildText("VigenciaMatricula", ns)) ? elem.getChildText("VigenciaMatricula", ns) : null);
                    tmpMatDatVig.setFecVigenciaMatricula(!"".equals(elem.getChildText("FecVigenciaMatricula", ns)) ? elem.getChildText("FecVigenciaMatricula", ns) : null);
                    tmpMatDatVig.setNit(!"".equals(elem.getChildText("Nit", ns)) ? elem.getChildText("Nit", ns) : null);
                    tmpMatDatVig.setCtrEstado(!"".equals(elem.getChildText("CtrEstado", ns)) ? elem.getChildText("CtrEstado", ns) : null);
                } else {
                    tmpMatDatVig.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                }
                listaMatDatVig.add(tmpMatDatVig);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
            log.catching(ex);
            tmpMatDatVig = new MatriculaDatosVigenciaBean();
            tmpMatDatVig.setCtrResult("0");
            listaMatDatVig.add(tmpMatDatVig);
        }
        log.info("Finalizando Metodo");
        return listaMatDatVig;
    }

    public static List<RepresentanteBean> srvRepresentante(String idMatricula) {
        List<RepresentanteBean> listaRepresetantes = new ArrayList<RepresentanteBean>();
        //String idContrato = "02UIF032016";
        //String keyContrato = "34{mPTwm?Htqj)8";
        RepresentanteBean tmpRepresentante = new RepresentanteBean();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("FundempresaClient.srvRepresentante");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wsr=\"http://www.fundempresa.org.bo:10080/wsrcbv2\">");
            soapXml.append("<soap:Header/>");
            soapXml.append("<soap:Body>");
            soapXml.append("<wsr:SrvRepresentante>");
            soapXml.append("<wsr:idContrato>").append(wsConfig.getIdContract()).append("</wsr:idContrato>");
            soapXml.append("<wsr:keyContrato>").append(wsConfig.getKeyContract()).append("</wsr:keyContrato>");
            soapXml.append("<wsr:IdMatricula>").append(idMatricula).append("</wsr:IdMatricula>");
            soapXml.append("</wsr:SrvRepresentante>");
            soapXml.append("</soap:Body>");
            soapXml.append("</soap:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),//"http://200.105.134.19:10080/wsrcbv2/servrcb.asmx",
                    "text/xml;charset=UTF-8",
                    "http://www.fundempresa.org.bo:10080/wsrcbv2/SrvRepresentante",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + reps);

            Namespace ns = Namespace.getNamespace("http://www.fundempresa.org.bo:10080/wsrcbv2");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("SrvRepresentanteResponse", ns);
            element = element.getChild("SrvRepresentanteResult", ns);
            List<Element> listElementRpresentantes = element.getChildren("Representantes", ns);

            for (Element elem : listElementRpresentantes) {
                tmpRepresentante = new RepresentanteBean();
                if (elem.getChildText("CtrResult", ns).equals("D-EXIST")) {
                    tmpRepresentante.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                    tmpRepresentante.setIdMatricula(!"".equals(elem.getChildText("IdMatricula", ns)) ? elem.getChildText("IdMatricula", ns) : null);
                    tmpRepresentante.setNombreVinculo(!"".equals(elem.getChildText("NombreVinculo", ns)) ? elem.getChildText("NombreVinculo", ns) : null);
                    tmpRepresentante.setNumId(!"".equals(elem.getChildText("NumId", ns)) ? elem.getChildText("NumId", ns) : null);
                    tmpRepresentante.setIdClase(!"".equals(elem.getChildText("IdClase", ns)) ? elem.getChildText("IdClase", ns) : null);
                    tmpRepresentante.setFecRegistro(!"".equals(elem.getChildText("FecRegistro", ns)) ? elem.getChildText("FecRegistro", ns) : null);
                    tmpRepresentante.setNumDoc(!"".equals(elem.getChildText("NumDoc", ns)) ? elem.getChildText("NumDoc", ns) : null);
                    tmpRepresentante.setFecDocumento(!"".equals(elem.getChildText("FecDocumento", ns)) ? elem.getChildText("FecDocumento", ns) : null);
                    tmpRepresentante.setNoticiaDocumento(!"".equals(elem.getChildText("NoticiaDocumento", ns)) ? elem.getChildText("NoticiaDocumento", ns) : null);
                    tmpRepresentante.setTipoVinculo(!"".equals(elem.getChildText("TipoVinculo", ns)) ? elem.getChildText("TipoVinculo", ns) : null);
                    tmpRepresentante.setIdLibro(!"".equals(elem.getChildText("IdLibro", ns)) ? elem.getChildText("IdLibro", ns) : null);
                    tmpRepresentante.setNumReg(!"".equals(elem.getChildText("NumReg", ns)) ? elem.getChildText("NumReg", ns) : null);
                } else {
                    tmpRepresentante.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                }
                listaRepresetantes.add(tmpRepresentante);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
            log.catching(ex);
            tmpRepresentante = new RepresentanteBean();
            tmpRepresentante.setCtrResult("0");
            listaRepresetantes.add(tmpRepresentante);
        }
        log.info("Finalizando Metodo");
        return listaRepresetantes;
    }

    /**
     *
     * @param idMatricula
     * @return List<InfomatriculaBean> el listado solo devuelve los siguientes
     * datos llenos: CtrResult, IdMatricula, RazonSocial, FechaInscripcion,
     * FechaUltRenovacion, UltGestionRenovada, Nit, CtrEstado
     */
    public static List<InfoMatriculaBean> srvMatricula(String idMatricula) {
        List<InfoMatriculaBean> listaMatriculas = new ArrayList<InfoMatriculaBean>();
        //String idContrato = "02UIF032016";
        //String keyContrato = "34{mPTwm?Htqj)8";
        InfoMatriculaBean tmpMatricula = new InfoMatriculaBean();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("FundempresaClient.srvMatricula");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wsr=\"http://www.fundempresa.org.bo:10080/wsrcbv2\">");
            soapXml.append("<soap:Header/>");
            soapXml.append("<soap:Body>");
            soapXml.append("<wsr:SrvMatricula>");
            soapXml.append("<wsr:idContrato>").append(wsConfig.getIdContract()).append("</wsr:idContrato>");
            soapXml.append("<wsr:keyContrato>").append(wsConfig.getKeyContract()).append("</wsr:keyContrato>");
            soapXml.append("<wsr:IdMatricula>").append(idMatricula).append("</wsr:IdMatricula>");
            soapXml.append("</wsr:SrvMatricula>");
            soapXml.append("</soap:Body>");
            soapXml.append("</soap:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),//"http://200.105.134.19:10080/wsrcbv2/servrcb.asmx",
                    "text/xml;charset=UTF-8",
                    "http://www.fundempresa.org.bo:10080/wsrcbv2/SrvMatricula",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + reps);

            Namespace ns = Namespace.getNamespace("http://www.fundempresa.org.bo:10080/wsrcbv2");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("SrvMatriculaResponse", ns);
            element = element.getChild("SrvMatriculaResult", ns);
            List<Element> listElementMatriculas = element.getChildren("MatriculaDatos", ns);

            for (Element elem : listElementMatriculas) {
                tmpMatricula = new InfoMatriculaBean();
                if (elem.getChildText("CtrResult", ns).equals("D-EXIST")) {
                    tmpMatricula.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                    tmpMatricula.setIdMatricula(!"".equals(elem.getChildText("IdMatricula", ns)) ? elem.getChildText("IdMatricula", ns) : null);
                    tmpMatricula.setRazonSocial(!"".equals(elem.getChildText("RazonSocial", ns)) ? elem.getChildText("RazonSocial", ns) : null);
                    tmpMatricula.setFechaInscripcion(!"".equals(elem.getChildText("FechaInscripcion", ns)) ? elem.getChildText("FechaInscripcion", ns) : null);
                    tmpMatricula.setFechaUltRenovacion(!"".equals(elem.getChildText("FechaUltRenovacion", ns)) ? elem.getChildText("FechaUltRenovacion", ns) : null);
                    tmpMatricula.setUltGestionRenovada(!"".equals(elem.getChildText("UltGestionRenovada", ns)) ? elem.getChildText("UltGestionRenovada", ns) : null);
                    tmpMatricula.setNit(!"".equals(elem.getChildText("Nit", ns)) ? elem.getChildText("Nit", ns) : null);
                    tmpMatricula.setCtrEstado(!"".equals(elem.getChildText("CtrEstado", ns)) ? elem.getChildText("CtrEstado", ns) : null);
                } else {
                    tmpMatricula.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                }
                listaMatriculas.add(tmpMatricula);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
            log.catching(ex);
            tmpMatricula = new InfoMatriculaBean();
            tmpMatricula.setCtrResult("0");
            listaMatriculas.add(tmpMatricula);
        }
        log.info("Finalizando Metodo");
        return listaMatriculas;
    }

    public static List<ImagenDatosBean> srvImagen(String idMatricula, String idLibro, String numReg) {

        List<ImagenDatosBean> listaImagenes = new ArrayList<ImagenDatosBean>();
        //String idContrato = "02UIF032016";
        //String keyContrato = "34{mPTwm?Htqj)8";
        ImagenDatosBean tmpImagen = new ImagenDatosBean();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("FundempresaClient.srvImagen");
        log.info("Iniciando Metodo");
        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wsr=\"http://www.fundempresa.org.bo:10080/wsrcbv2\">");
            soapXml.append("<soap:Header/>");
            soapXml.append("<soap:Body>");
            soapXml.append("<wsr:SrvImagen>");
            soapXml.append("<wsr:idContrato>").append(wsConfig.getIdContract()).append("</wsr:idContrato>");
            soapXml.append("<wsr:keyContrato>").append(wsConfig.getKeyContract()).append("</wsr:keyContrato>");
            soapXml.append("<wsr:IdMatricula>").append(idMatricula).append("</wsr:IdMatricula>");
            soapXml.append("<wsr:idLibro>").append(idLibro).append("</wsr:idLibro>");
            soapXml.append("<wsr:numReg>").append(numReg).append("</wsr:numReg>");
            soapXml.append("</wsr:SrvImagen>");
            soapXml.append("</soap:Body>");
            soapXml.append("</soap:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),//"http://200.105.134.19:10080/wsrcbv2/servrcb.asmx",
                    "text/xml;charset=UTF-8",
                    "http://www.fundempresa.org.bo:10080/wsrcbv2/SrvImagen",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + reps);

            Namespace ns = Namespace.getNamespace("http://www.fundempresa.org.bo:10080/wsrcbv2");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("SrvImagenResponse", ns);
            element = element.getChild("SrvImagenResult", ns);
            List<Element> listElementImagenDatos = element.getChildren("ImagenDatos", ns);

            for (Element elem : listElementImagenDatos) {
                tmpImagen = new ImagenDatosBean();
                if (elem.getChildText("CtrResult", ns).equals("D-EXIST")) {
                    tmpImagen.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                    tmpImagen.setIdMatricula(!"".equals(elem.getChildText("IdMatricula", ns)) ? elem.getChildText("IdMatricula", ns) : null);
                    tmpImagen.setNumReg(!"".equals(elem.getChildText("NumReg", ns)) ? elem.getChildText("NumReg", ns) : null);
                    tmpImagen.setIdLibro(!"".equals(elem.getChildText("IdLibro", ns)) ? elem.getChildText("IdLibro", ns) : null);
                    tmpImagen.setCodImagen(!"".equals(elem.getChildText("CodImagen", ns)) ? elem.getChildText("CodImagen", ns) : null);
                    tmpImagen.setRutaImagen(!"".equals(elem.getChildText("RutaImagen", ns)) ? "http://200.105.134.19:10080/" + elem.getChildText("RutaImagen", ns) : null);
                } else {
                    tmpImagen.setCtrResult(!"".equals(elem.getChildText("CtrResult", ns)) ? elem.getChildText("CtrResult", ns) : null);
                }
                listaImagenes.add(tmpImagen);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
            log.catching(ex);
            tmpImagen = new ImagenDatosBean();
            tmpImagen.setCtrResult("0");
            listaImagenes.add(tmpImagen);
        }
        log.info("Finalizando Metodo");
        return listaImagenes;
    }
}
