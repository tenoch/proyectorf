package bo.gob.uif.webclients.sicod.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.io.InputStream;

public class DocumentoBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private String idDocumento;
    private String tipoDocumento;
    private String clasificadorDocumento;
    private String tipoDocumentoDesc;
    private String clasificadorDocumentoDesc;
    private Date fechaDocumento;
    private Date fechaCreacion;
    private String numeroDocumento;
    private String referenciaDocumento;
    private String estado;
    private String conclusiones;
    private String idArea;
    private String area;
    private String idProceso;
    private String cuerpo;
    private String codigoUid;
    private String creador;
    private String emisor;
    private String via;
    private String receptor;
    private Boolean adjuntoWord;
    private boolean texto;
    private Boolean coneccion;

    public DocumentoBean() {
        super();
        adjuntoWord = Boolean.FALSE;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(String idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getClasificadorDocumento() {
        return clasificadorDocumento;
    }

    public void setClasificadorDocumento(String clasificadorDocumento) {
        this.clasificadorDocumento = clasificadorDocumento;
    }

    public String getTipoDocumentoDesc() {
        return tipoDocumentoDesc;
    }

    public void setTipoDocumentoDesc(String tipoDocumentoDesc) {
        this.tipoDocumentoDesc = tipoDocumentoDesc;
    }

    public String getClasificadorDocumentoDesc() {
        return clasificadorDocumentoDesc;
    }

    public void setClasificadorDocumentoDesc(String clasificadorDocumentoDesc) {
        this.clasificadorDocumentoDesc = clasificadorDocumentoDesc;
    }

    public Date getFechaDocumento() {
        return fechaDocumento;
    }

    public void setFechaDocumento(Date fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getReferenciaDocumento() {
        return referenciaDocumento;
    }

    public void setReferenciaDocumento(String referenciaDocumento) {
        this.referenciaDocumento = referenciaDocumento;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getConclusiones() {
        return conclusiones;
    }

    public void setConclusiones(String conclusiones) {
        this.conclusiones = conclusiones;
    }

    public String getIdArea() {
        return idArea;
    }

    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getIdProceso() {
        return idProceso;
    }

    public void setIdProceso(String idProceso) {
        this.idProceso = idProceso;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public String getCodigoUid() {
        return codigoUid;
    }

    public void setCodigoUid(String codigoUid) {
        this.codigoUid = codigoUid;
    }

    public String getCreador() {
        return creador;
    }

    public void setCreador(String creador) {
        this.creador = creador;
    }

    public String getEmisor() {
        return emisor;
    }

    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getReceptor() {
        return receptor;
    }

    public void setReceptor(String receptor) {
        this.receptor = receptor;
    }

    public Boolean getAdjuntoWord() {
        return adjuntoWord;
    }

    public void setAdjuntoWord(Boolean adjuntoWord) {
        this.adjuntoWord = adjuntoWord;
    }

    public boolean isTexto() {
        return texto;
    }

    public void setTexto(boolean texto) {
        this.texto = texto;
    }

    public Boolean getConeccion() {
        return coneccion;
    }

    public void setConeccion(Boolean coneccion) {
        this.coneccion = coneccion;
    }

}
