package bo.gob.uif.webclients.common;

import java.util.UUID;

/**
 * Created by Vladimir.Callisaya on 03/09/2016.
 */
public class StringTokenizer {

    String idOne;

    public StringTokenizer() {
        idOne = UUID.randomUUID().toString();
    }

    public String getId() {
        String codigo = "[" + idOne + "]";
        return codigo;
    }

    public StringBuffer tokenizerString(StringBuffer entrada) {
        StringBuffer toke;
        toke = new StringBuffer();
        toke.append(this.getId()).append(" ");
        toke.append(entrada);
        return toke;
    }

    public StringBuilder tokenizerString(StringBuilder entrada) {
        StringBuilder toke;
        toke = new StringBuilder();
        toke.append(this.getId()).append(" ");
        toke.append(entrada);
        return toke;
    }

    public static void main(String[] args) {
        StringTokenizer st = new StringTokenizer();
        System.out.println(st.getId());
    }

}
