package bo.gob.uif.webclients.segip;

import bo.gob.uif.webclients.cfg.MainWebServicesConfig;
import bo.gob.uif.webclients.cfg.WebServiceConfig;
import bo.gob.uif.webclients.common.SoapHttpRequest;
import bo.gob.uif.webclients.common.StringTokenizer;
import bo.gob.uif.webclients.segip.beans.DatosPersonaBean;
import bo.gob.uif.webclients.segip.beans.DatosRespuestaBean;
import bo.gob.uif.webclients.segip.beans.UsuarioFinalAltaBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.XMLOutputter;
import org.json.JSONObject;
import sun.misc.BASE64Decoder;
import java.text.SimpleDateFormat;

/**
 * Created by Vladimir.Callisaya on 08/05/2017.
 *
 * Modificado: Susana Escobar Paz Fecha: 17/05/2018
 */
public class SegipClient {

    public static WebServiceConfig wsConfig = MainWebServicesConfig.INSTANCE.getWebServiceConfig("SEGIPConsultaSerWeb");
    private static Logger log;
    private static StringTokenizer st = new StringTokenizer();
    public static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public static DatosRespuestaBean consultaDatoPersonaEnJson(String pClaveAccesoUsuarioFinal, String pNumeroAutorizacion, String pNumeroDocumento, String pComplemento, String pNombre, String pPrimerApellido, String pSegundoApellido, String pFechaNacimiento) {
        String pCodigoInstitucion = wsConfig.getCodigoInstitucion(); //"149";
        String pUsuario = wsConfig.getUsuarioConvenio(); //"uif.prueba";
        String pContrasenia = wsConfig.getContrasenaConvenio(); //"Segip2017";

        DatosRespuestaBean respuesta = new DatosRespuestaBean();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SegipClient.consultaDatoPersonaEnArray");
        log.info("Iniciando Metodo consultaDatoPersonaEnArray");

        JSONObject personaSegip;

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<tem:ConsultaDatoPersonaEnJson>");
            soapXml.append("<tem:pCodigoInstitucion>").append(pCodigoInstitucion).append("</tem:pCodigoInstitucion>");
            soapXml.append("<tem:pUsuario>").append(pUsuario).append("</tem:pUsuario>");
            soapXml.append("<tem:pContrasenia>").append(pContrasenia).append("</tem:pContrasenia>");
            soapXml.append("<tem:pClaveAccesoUsuarioFinal>").append(pClaveAccesoUsuarioFinal).append("</tem:pClaveAccesoUsuarioFinal>");
            soapXml.append("<tem:pNumeroAutorizacion>").append(pNumeroAutorizacion).append("</tem:pNumeroAutorizacion>");
            soapXml.append("<tem:pNumeroDocumento>").append(pNumeroDocumento).append("</tem:pNumeroDocumento>");
            soapXml.append("<tem:pComplemento>").append(pComplemento).append("</tem:pComplemento>");
            soapXml.append("<tem:pNombre>").append(pNombre).append("</tem:pNombre>");
            soapXml.append("<tem:pPrimerApellido>").append(pPrimerApellido).append("</tem:pPrimerApellido>");
            soapXml.append("<tem:pSegundoApellido>").append(pSegundoApellido).append("</tem:pSegundoApellido>");
            soapXml.append("<tem:pFechaNacimiento>").append(pFechaNacimiento).append("</tem:pFechaNacimiento>");
            soapXml.append("</tem:ConsultaDatoPersonaEnJson>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo el Servicio Web del SEGIP");
            String requestLog = soapXml.toString();
            StringBuilder req = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + req);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),
                    "text/xml;charset=UTF-8",
                    "http://tempuri.org/IServicioExternoInstitucion/ConsultaDatoPersonaEnJson",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("Response : " + reps);

            Namespace ns = Namespace.getNamespace("http://tempuri.org/");
            Namespace ns2 = Namespace.getNamespace("http://schemas.datacontract.org/2004/07/Framework.Comun.Resultado");
            Namespace ns3 = Namespace.getNamespace("http://schemas.datacontract.org/2004/07/Convenio.Core.Modelo.Dto.Proceso.ServicioExterno");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("ConsultaDatoPersonaEnJsonResponse", ns);
            element = element.getChild("ConsultaDatoPersonaEnJsonResult", ns);

            respuesta.setEsValido(!"".equals(element.getChildText("EsValido", ns2)) ? Boolean.valueOf(element.getChildText("EsValido", ns2)) : false);
            respuesta.setMensaje(!"".equals(element.getChildText("Mensaje", ns2)) ? element.getChildText("Mensaje", ns2) : null);
            respuesta.setTipoMensaje(!"".equals(element.getChildText("TipoMensaje", ns2)) ? element.getChildText("TipoMensaje", ns2) : null);
            respuesta.setCodigoRespuesta(!"".equals(element.getChildText("CodigoRespuesta", ns2)) ? Integer.parseInt(element.getChildText("CodigoRespuesta", ns2)) : 0);
            respuesta.setCodigoUnico(!"".equals(element.getChildText("CodigoUnico", ns2)) ? element.getChildText("CodigoUnico", ns2) : null);
            respuesta.setDescripcionRespuesta(!"".equals(element.getChildText("DescripcionRespuesta", ns2)) ? element.getChildText("DescripcionRespuesta", ns2) : null);

            if (respuesta.getCodigoRespuesta() == 2) {
                personaSegip = new JSONObject(element.getChildText("DatosPersonaEnFormatoJson", ns3));
                //log.debug("ci: " + personaSegip.getString("NumeroDocumento"));
                DatosPersonaBean personaBeanSegip = new DatosPersonaBean();
                personaBeanSegip.setComplementoVisible(personaSegip.getString("ComplementoVisible"));
                personaBeanSegip.setNumeroDocumento(personaSegip.getString("NumeroDocumento"));
                personaBeanSegip.setComplemento(personaSegip.getString("Complemento"));
                personaBeanSegip.setNombres(personaSegip.getString("Nombres"));
                personaBeanSegip.setPrimerApellido(personaSegip.getString("PrimerApellido"));
                personaBeanSegip.setSegundoApellido(personaSegip.getString("SegundoApellido"));
                personaBeanSegip.setFechaNacimiento(sdf.parse(personaSegip.getString("FechaNacimiento")));

                respuesta.setPersona(personaBeanSegip);
            }
        } catch (Exception ex) {
            log.catching(ex);
            respuesta.setMensaje("Mensaje");
            respuesta.setDescripcionRespuesta("No hay conexión con el servicio");
        }

        return respuesta;
    }

    public static DatosRespuestaBean consultaDatoPersonaCertificacion(String pClaveAccesoUsuarioFinal, String pNumeroAutorizacion, String pNumeroDocumento, String pComplemento, String pNombre, String pPrimerApellido, String pSegundoApellido, String pFechaNacimiento) {
        String pCodigoInstitucion = wsConfig.getCodigoInstitucion(); //"149";
        String pUsuario = wsConfig.getUsuarioConvenio(); //"uif.prueba";
        String pContrasenia = wsConfig.getContrasenaConvenio(); //"Segip2017";

        DatosRespuestaBean respuesta = new DatosRespuestaBean();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SegipClient.consultaDatoPersonaCertificacionPdf");
        log.info("Iniciando Metodo consultaDatoPersonaCertificacion");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<tem:ConsultaDatoPersonaCertificacion>");
            soapXml.append("<tem:pCodigoInstitucion>").append(pCodigoInstitucion).append("</tem:pCodigoInstitucion>");
            soapXml.append("<tem:pUsuario>").append(pUsuario).append("</tem:pUsuario>");
            soapXml.append("<tem:pContrasenia>").append(pContrasenia).append("</tem:pContrasenia>");
            soapXml.append("<tem:pClaveAccesoUsuarioFinal>").append(pClaveAccesoUsuarioFinal).append("</tem:pClaveAccesoUsuarioFinal>");
            soapXml.append("<tem:pNumeroAutorizacion>").append(pNumeroAutorizacion).append("</tem:pNumeroAutorizacion>");
            soapXml.append("<tem:pNumeroDocumento>").append(pNumeroDocumento).append("</tem:pNumeroDocumento>");
            soapXml.append("<tem:pComplemento>").append(pComplemento).append("</tem:pComplemento>");
            soapXml.append("<tem:pNombre>").append(pNombre).append("</tem:pNombre>");
            soapXml.append("<tem:pPrimerApellido>").append(pPrimerApellido).append("</tem:pPrimerApellido>");
            soapXml.append("<tem:pSegundoApellido>").append(pSegundoApellido).append("</tem:pSegundoApellido>");
            soapXml.append("<tem:pFechaNacimiento>").append(pFechaNacimiento).append("</tem:pFechaNacimiento>");
            soapXml.append("</tem:ConsultaDatoPersonaCertificacion>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo el Servicio Web del SEGIP (Certificacion PDF)");
            String requestLog = soapXml.toString();
            StringBuilder req = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + req);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),
                    "text/xml;charset=UTF-8",
                    "http://tempuri.org/IServicioExternoInstitucion/ConsultaDatoPersonaCertificacion",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("Response : " + reps);

            Namespace ns = Namespace.getNamespace("http://tempuri.org/");
            Namespace ns2 = Namespace.getNamespace("http://schemas.datacontract.org/2004/07/Framework.Comun.Resultado");
            Namespace ns3 = Namespace.getNamespace("http://schemas.datacontract.org/2004/07/Convenio.Core.Modelo.Dto.Proceso.ServicioExterno");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("ConsultaDatoPersonaCertificacionResponse", ns);
            element = element.getChild("ConsultaDatoPersonaCertificacionResult", ns);

            respuesta.setEsValido(!"".equals(element.getChildText("EsValido", ns2)) ? Boolean.valueOf(element.getChildText("EsValido", ns2)) : false);
            respuesta.setMensaje(!"".equals(element.getChildText("Mensaje", ns2)) ? element.getChildText("Mensaje", ns2) : null);
            respuesta.setTipoMensaje(!"".equals(element.getChildText("TipoMensaje", ns2)) ? element.getChildText("TipoMensaje", ns2) : null);
            respuesta.setCodigoRespuesta(!"".equals(element.getChildText("CodigoRespuesta", ns2)) ? Integer.parseInt(element.getChildText("CodigoRespuesta", ns2)) : 0);
            respuesta.setCodigoUnico(!"".equals(element.getChildText("CodigoUnico", ns2)) ? element.getChildText("CodigoUnico", ns2) : null);
            respuesta.setDescripcionRespuesta(!"".equals(element.getChildText("DescripcionRespuesta", ns2)) ? element.getChildText("DescripcionRespuesta", ns2) : null);

            if (respuesta.getCodigoRespuesta() == 2) {
                BASE64Decoder decoder = new BASE64Decoder();
                respuesta.setCertificacionPdf(decoder.decodeBuffer(element.getChildText("ReporteCertificacion", ns3)));
            }

        } catch (Exception ex) {
            log.catching(ex);
            respuesta.setMensaje("Mensaje");
            respuesta.setDescripcionRespuesta("No hay conexión con el servicio");
        }
        return respuesta;
    }

    public static UsuarioFinalAltaBean creacionUsuarioFinal(String pNumeroCedula, String pComplemento, String pNombre, String pPrimerApellido, String pSegundoApellido, String pFechaNacimiento, String pNombreInstitucion, String pNombreCargo, String pDepartamento, String pProvincia, String pSeccionMunicipal, String pLocalidad, String pOficina, String pCorreoElectronico, String pTelefono, String pTelefonoOficina) {
        String pCodigoInstitucion = wsConfig.getCodigoInstitucion(); //"149";
        String pUsuario = wsConfig.getUsuarioConvenio(); //"uif.prueba";
        String pContrasenia = wsConfig.getContrasenaConvenio(); //"Segip2017";

        UsuarioFinalAltaBean respuesta = new UsuarioFinalAltaBean();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SegipClient.creacionUsuarioAlta");
        log.info("Iniciando Metodo creacionUsuarioFinal");

        //JSONObject personaSegip;
        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<tem:UsuarioFinalAlta>");
            soapXml.append("<tem:pIdInstitucion>").append(pCodigoInstitucion).append("</tem:pIdInstitucion>");
            soapXml.append("<tem:pUsuario>").append(pUsuario).append("</tem:pUsuario>");
            soapXml.append("<tem:pContrasenia>").append(pContrasenia).append("</tem:pContrasenia>");
            soapXml.append("<tem:pNumeroCedula>").append(pNumeroCedula).append("</tem:pNumeroCedula>");
            soapXml.append("<tem:pComplemento>").append(pComplemento).append("</tem:pComplemento>");
            soapXml.append("<tem:pNombre>").append(pNombre).append("</tem:pNombre>");
            soapXml.append("<tem:pPrimerApellido>").append(pPrimerApellido).append("</tem:pPrimerApellido>");
            soapXml.append("<tem:pSegundoApellido>").append(pSegundoApellido).append("</tem:pSegundoApellido>");
            soapXml.append("<tem:pFechaNacimiento>").append(pFechaNacimiento).append("</tem:pFechaNacimiento>");
            soapXml.append("<tem:pNombreInstitucion>").append(pNombreInstitucion).append("</tem:pNombreInstitucion>");
            soapXml.append("<tem:pNombreCargo>").append(pNombreCargo).append("</tem:pNombreCargo>");
            soapXml.append("<tem:pDepartamento>").append(pDepartamento).append("</tem:pDepartamento>");
            soapXml.append("<tem:pProvincia>").append(pProvincia).append("</tem:pProvincia>");
            soapXml.append("<tem:pSeccionMunicipal>").append(pSeccionMunicipal).append("</tem:pSeccionMunicipal>");
            soapXml.append("<tem:pLocalidad>").append(pLocalidad).append("</tem:pLocalidad>");
            soapXml.append("<tem:pOficina>").append(pOficina).append("</tem:pOficina>");
            soapXml.append("<tem:pCorreoElectronico>").append(pCorreoElectronico).append("</tem:pCorreoElectronico>");
            soapXml.append("<tem:pTelefono>").append(pTelefono).append("</tem:pTelefono>");
            soapXml.append("<tem:pTelefonoOficina>").append(pTelefonoOficina).append("</tem:pTelefonoOficina>");
            soapXml.append("</tem:UsuarioFinalAlta>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo el Servicio Web del SEGIP");
            String requestLog = soapXml.toString();
            StringBuilder req = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + req);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation2(),
                    "text/xml;charset=UTF-8",
                    "http://tempuri.org/IServicioExternoComun/UsuarioFinalAlta",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("Response : " + reps);

            Namespace ns = Namespace.getNamespace("http://tempuri.org/");
            Namespace ns2 = Namespace.getNamespace("http://schemas.datacontract.org/2004/07/Comun.Core.Modelo.Dto.Autorizacion.Proceso");
            //Namespace ns3 = Namespace.getNamespace("http://schemas.datacontract.org/2004/07/Convenio.Core.Modelo.Dto.Proceso.ServicioExterno");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("UsuarioFinalAltaResponse", ns);
            element = element.getChild("UsuarioFinalAltaResult", ns);

            respuesta.setClaveAcceso(!"".equals(element.getChildText("ClaveAcceso", ns2)) ? element.getChildText("ClaveAcceso", ns2) : null);
            respuesta.setEsValido(!"".equals(element.getChildText("EsValido", ns2)) ? Boolean.valueOf(element.getChildText("EsValido", ns2)) : false);
            respuesta.setMensaje(!"".equals(element.getChildText("Mensaje", ns2)) ? element.getChildText("Mensaje", ns2) : null);
            respuesta.setTipoMensaje(!"".equals(element.getChildText("TipoMensaje", ns2)) ? element.getChildText("TipoMensaje", ns2) : null);

        } catch (Exception ex) {
            log.catching(ex);
        }

        return respuesta;
    }

    public static UsuarioFinalAltaBean habilitaInhabilitaUsuarioFinal(String claveAcceso, boolean estado) {
        String pCodigoInstitucion = wsConfig.getCodigoInstitucion();
        String pUsuario = wsConfig.getUsuarioConvenio();
        String pContrasenia = wsConfig.getContrasenaConvenio();

        UsuarioFinalAltaBean respuesta = new UsuarioFinalAltaBean();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SegipClient.usuarioFinalHabilitaInhabilita");
        log.info("Iniciando Metodo habilitaInhabilitaUsuarioFinal");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<tem:UsuarioFinalHabilitaInhabilita>");
            soapXml.append("<tem:pIdInstitucion>").append(pCodigoInstitucion).append("</tem:pIdInstitucion>");
            soapXml.append("<tem:pUsuario>").append(pUsuario).append("</tem:pUsuario>");
            soapXml.append("<tem:pContrasenia>").append(pContrasenia).append("</tem:pContrasenia>");
            soapXml.append("<tem:pClaveAcceso>").append(claveAcceso).append("</tem:pClaveAcceso>");
            soapXml.append("<tem:pEstado>").append(estado).append("</tem:pEstado>\n");
            soapXml.append("</tem:UsuarioFinalHabilitaInhabilita>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo el Servicio Web del SEGIP");
            String requestLog = soapXml.toString();
            StringBuilder req = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + req);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation2(),
                    "text/xml;charset=UTF-8",
                    "http://tempuri.org/IServicioExternoComun/UsuarioFinalHabilitaInhabilita",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("Response : " + reps);

            Namespace ns = Namespace.getNamespace("http://tempuri.org/");
            Namespace ns2 = Namespace.getNamespace("http://schemas.datacontract.org/2004/07/Comun.Core.Modelo.Dto.Autorizacion.Proceso");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("UsuarioFinalHabilitaInhabilitaResponse", ns);
            element = element.getChild("UsuarioFinalHabilitaInhabilitaResult", ns);

            respuesta.setClaveAcceso(!"".equals(element.getChildText("ClaveAcceso", ns2)) ? element.getChildText("ClaveAcceso", ns2) : null);
            respuesta.setEsValido(!"".equals(element.getChildText("EsValido", ns2)) ? Boolean.valueOf(element.getChildText("EsValido", ns2)) : false);
            respuesta.setMensaje(!"".equals(element.getChildText("Mensaje", ns2)) ? element.getChildText("Mensaje", ns2) : null);
            respuesta.setTipoMensaje(!"".equals(element.getChildText("TipoMensaje", ns2)) ? element.getChildText("TipoMensaje", ns2) : null);

        } catch (Exception ex) {
            log.catching(ex);
        }

        return respuesta;
    }

    public static UsuarioFinalAltaBean modificacionUsuarioFinal(String claveAcceso, String pNombreInstitucion, String pCargo, String pDepartamento, String pProvincia, String pSeccion, String pLocalidad, String pOficina, String pCorreoElectronico, String pTelefono, String pTelefonoOficina) {
        String pCodigoInstitucion = wsConfig.getCodigoInstitucion();
        String pUsuario = wsConfig.getUsuarioConvenio();
        String pContrasenia = wsConfig.getContrasenaConvenio();

        UsuarioFinalAltaBean respuesta = new UsuarioFinalAltaBean();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SegipClient.usuarioFinalModifica");
        log.info("Iniciando Metodo modificacionUsuarioFinal");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<tem:UsuarioFinalModifica>");
            soapXml.append("<tem:pIdInstitucion>").append(pCodigoInstitucion).append("</tem:pIdInstitucion>");
            soapXml.append("<tem:pUsuario>").append(pUsuario).append("</tem:pUsuario>");
            soapXml.append("<tem:pContrasenia>").append(pContrasenia).append("</tem:pContrasenia>");
            soapXml.append("<tem:pClaveAcceso>").append(claveAcceso).append("</tem:pClaveAcceso>");
            soapXml.append("<tem:pNombreInstitucion>").append(pNombreInstitucion).append("</tem:pNombreInstitucion>");
            soapXml.append("<tem:pCargo>").append(pCargo).append("</tem:pCargo>");
            soapXml.append("<tem:pDepartamento>").append(pDepartamento).append("</tem:pDepartamento>");
            soapXml.append("<tem:pProvincia>").append(pProvincia).append("</tem:pProvincia>");
            soapXml.append("<tem:pSeccion>").append(pSeccion).append("</tem:pSeccion>");
            soapXml.append("<tem:pLocalidad>").append(pLocalidad).append("</tem:pLocalidad>");
            soapXml.append("<tem:pOficina>").append(pOficina).append("</tem:pOficina>");
            soapXml.append("<tem:pCorreoElectronico>").append(pCorreoElectronico).append("</tem:pCorreoElectronico>");
            soapXml.append("<tem:pTelefono>").append(pTelefono).append("</tem:pTelefono>");
            soapXml.append("<tem:pTelefonoOficina>").append(pTelefonoOficina).append("</tem:pTelefonoOficina>");
            soapXml.append("</tem:UsuarioFinalModifica>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo el Servicio Web del SEGIP");
            String requestLog = soapXml.toString();
            StringBuilder req = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + req);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation2(),
                    "text/xml;charset=UTF-8",
                    "http://tempuri.org/IServicioExternoComun/UsuarioFinalModifica",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("Response : " + reps);

            Namespace ns = Namespace.getNamespace("http://tempuri.org/");
            Namespace ns2 = Namespace.getNamespace("http://schemas.datacontract.org/2004/07/Comun.Core.Modelo.Dto.Autorizacion.Proceso");
            //Namespace ns3 = Namespace.getNamespace("http://schemas.datacontract.org/2004/07/Convenio.Core.Modelo.Dto.Proceso.ServicioExterno");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("UsuarioFinalModificaResponse", ns);
            element = element.getChild("UsuarioFinalModificaResult", ns);

            respuesta.setClaveAcceso(!"".equals(element.getChildText("ClaveAcceso", ns2)) ? element.getChildText("ClaveAcceso", ns2) : null);
            respuesta.setEsValido(!"".equals(element.getChildText("EsValido", ns2)) ? Boolean.valueOf(element.getChildText("EsValido", ns2)) : false);
            respuesta.setMensaje(!"".equals(element.getChildText("Mensaje", ns2)) ? element.getChildText("Mensaje", ns2) : null);
            respuesta.setTipoMensaje(!"".equals(element.getChildText("TipoMensaje", ns2)) ? element.getChildText("TipoMensaje", ns2) : null);

        } catch (Exception ex) {
            log.catching(ex);
        }

        return respuesta;
    }
}
