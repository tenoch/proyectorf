package bo.gob.uif.webclients.sicod;

import bo.gob.uif.webclients.cfg.MainWebServicesConfig;
import bo.gob.uif.webclients.cfg.WebServiceConfig;
import bo.gob.uif.webclients.common.SoapHttpRequest;
import bo.gob.uif.webclients.common.StringTokenizer;
import bo.gob.uif.webclients.sicod.beans.AsignacionBean;
import bo.gob.uif.webclients.sicod.beans.AsignacionTramiteRespBean;
import bo.gob.uif.webclients.sicod.beans.DocumentoBean;
import bo.gob.uif.webclients.sicod.beans.ProcesoBean;
import bo.gob.uif.webclients.sicod.beans.TramiteAsignacionBean;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.XMLOutputter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sun.misc.BASE64Decoder;

/**
 * Created by Vladimir.Callisaya on 12/08/2016.
 */
public class SicodClient {

    public static SimpleDateFormat sdf = new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss");

    public static WebServiceConfig wsConfig = MainWebServicesConfig.INSTANCE
            .getWebServiceConfig("SICODConsultaSerWeb");
    private static Logger log;
    private static StringTokenizer st = new StringTokenizer();

    public static String asignarTramiteSicod(String idProceso,
            String idUsuarioOrigen, List<TramiteAsignacionBean> destinos,
            String idUser, String terminal) {
        String result = "";
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SicodClient.asignarTramiteSicod");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws.seguimiento.siasi/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<ws:asignar>");
            soapXml.append("<idProceso>").append(idProceso)
                    .append("</idProceso>");
            soapXml.append("<idUsuarioOrigen>").append(idUsuarioOrigen)
                    .append("</idUsuarioOrigen>\n");
            for (TramiteAsignacionBean destino : destinos) {
                soapXml.append("<destino>");
                soapXml.append("<fojas>").append(destino.getFojas())
                        .append("</fojas>");
                soapXml.append("<idUsuarioDestino>")
                        .append(destino.getIdUsuarioDestino())
                        .append("</idUsuarioDestino>");
                soapXml.append("<nombreDestino>")
                        .append(destino.getNombredestino())
                        .append("</nombreDestino>");
                soapXml.append("<numCopia>").append(destino.getNumCopia())
                        .append("</numCopia>");
                soapXml.append("<proveido>").append(destino.getProveido())
                        .append("</proveido>");
                soapXml.append("<tipoAsignacion>")
                        .append(destino.getTipoAsignacion())
                        .append("</tipoAsignacion>\n");
                soapXml.append("<tipoAsignacionDesc>")
                        .append(destino.getTipoAsignacionDesc())
                        .append("</tipoAsignacionDesc>\n");
                soapXml.append("</destino>");
            }
            soapXml.append("<idUser>").append(idUser).append("</idUser>");
            soapXml.append("<terminal>").append(terminal).append("</terminal>");
            soapXml.append("</ws:asignar>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder())
                    .append(requestLog));
            log.debug("Request : " + reqs);
            Document response = SoapHttpRequest.sendRequestPOST(
                    wsConfig.getLocation2(),// "http://172.17.0.58:8080/AsignacionWS",
                    "text/xml;charset=UTF-8", "", soapXml.toString());

            StringBuilder resp = st.tokenizerString((new StringBuilder())
                    .append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + resp);

            Namespace ns = Namespace.getNamespace("ns2",
                    "http://ws.seguimiento.siasi/");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("asignarResponse", ns);
            result = element.getChildText("return");

        } catch (Exception e) {
            e.printStackTrace();
            log.catching(e);
        }
        log.info("Finalizando Metodo");
        return result;
    }

    public static List<AsignacionTramiteRespBean> asignarTramiteSicodNuevo(
            String idProceso, String idAsignacionOrigen,
            String idUsuarioOrigen, List<TramiteAsignacionBean> destinos,
            String idUser, String terminal) {
        List<AsignacionTramiteRespBean> result = new ArrayList<AsignacionTramiteRespBean>();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SicodClient.asignarTramiteSicod");
        log.info("Iniciando Metodo");

        try {

            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://www.services.uif/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<ser:asignarTramiteSicod>");
            soapXml.append("<ser:idProceso>").append(idProceso)
                    .append("</ser:idProceso>");
            soapXml.append("<ser:idAsignacionOrigen>")
                    .append(idAsignacionOrigen)
                    .append("</ser:idAsignacionOrigen>");
            soapXml.append("<ser:idUsuarioOrigen>").append(idUsuarioOrigen)
                    .append("</ser:idUsuarioOrigen>");
            for (TramiteAsignacionBean destino : destinos) {
                soapXml.append("<ser:destino>");
                soapXml.append("<fojas>").append(destino.getFojas())
                        .append("</fojas>");
                soapXml.append("<idUsuarioDestino>")
                        .append(destino.getIdUsuarioDestino())
                        .append("</idUsuarioDestino>");
                soapXml.append("<nombreDestino>")
                        .append(destino.getNombredestino())
                        .append("</nombreDestino>");
                soapXml.append("<numCopia>").append(destino.getNumCopia())
                        .append("</numCopia>");
                soapXml.append("<proveido>").append(destino.getProveido())
                        .append("</proveido>");
                soapXml.append("<tipoAsignacion>")
                        .append(destino.getTipoAsignacion())
                        .append("</tipoAsignacion>");
                soapXml.append("<tipoAsignacionDesc>")
                        .append(destino.getTipoAsignacionDesc())
                        .append("</tipoAsignacionDesc>");
                soapXml.append("</ser:destino>");
            }
            soapXml.append("<ser:idUser>").append(idUser)
                    .append("</ser:idUser>");
            soapXml.append("<ser:terminal>").append(terminal)
                    .append("</ser:terminal>");
            soapXml.append("</ser:asignarTramiteSicod>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder())
                    .append(requestLog));
            log.debug("Request : " + reqs);
            Document response = SoapHttpRequest.sendRequestPOST(
                    wsConfig.getLocation(),// "http://172.17.0.58:8080/AsignacionWS",
                    "text/xml;charset=UTF-8", "", soapXml.toString());

            StringBuilder resp = st.tokenizerString((new StringBuilder())
                    .append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + resp);

            // Namespace ns = Namespace.getNamespace("ns2",
            // "http://ws.seguimiento.siasi/");
            Namespace ns = Namespace.getNamespace("http://www.services.uif/");
            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("asignarTramiteSicodResponse", ns);

            List<Element> listRespReturn = element.getChildren("return", ns);

            for (Element elem : listRespReturn) {

                AsignacionTramiteRespBean asignacionTramiteResp = new AsignacionTramiteRespBean();

                asignacionTramiteResp.setIdAsignacion(Long.parseLong(elem
                        .getChildText("idAsignacion")));
                asignacionTramiteResp.setIdUsuarioDestino(Long.parseLong(elem
                        .getChildText("idUsuarioDestino")));

                result.add(asignacionTramiteResp);

            }

            // result = element.getChildText("return");
        } catch (Exception e) {
            e.printStackTrace();
            log.catching(e);
        }
        log.info("Finalizando Metodo");
        return result;
    }

    public static String centralizarTramiteSicod(String idProceso,
            String idAsignacion, List<AsignacionBean> asignaciones,
            String idUsuario, String terminal) {
        String result = "";
        Element element;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SicodClient.centralizarTramiteSicod");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://www.services.uif/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<ser:centralizarTramiteSicod>");
            soapXml.append("<ser:idProceso>").append(idProceso)
                    .append("</ser:idProceso>");
            soapXml.append("<ser:idAsignacion>").append(idAsignacion)
                    .append("</ser:idAsignacion>");
            for (AsignacionBean asignacion : asignaciones) {
                soapXml.append("<ser:listAsignaciones>");
                soapXml.append("<destino>").append(asignacion.getDestino())
                        .append("</destino>");
                soapXml.append("<fechaAsignacion>")
                        .append(df.format(asignacion.getFechaAsignacion()))
                        .append("</fechaAsignacion>");
                soapXml.append("<fechaRecepcion>")
                        .append(df.format(asignacion.getFechaRecepcion()))
                        .append("</fechaRecepcion>");
                soapXml.append("<fojas>").append(asignacion.getFojas())
                        .append("</fojas>");
                soapXml.append("<idAsignacion>")
                        .append(asignacion.getIdAsignacion())
                        .append("</idAsignacion>");
                soapXml.append("<idUsuarioDestino>")
                        .append(asignacion.getIdUsuarioDestino())
                        .append("</idUsuarioDestino>");
                soapXml.append("<idUsuarioOrigen>")
                        .append(asignacion.getIdUsuarioOrigen())
                        .append("</idUsuarioOrigen>");
                soapXml.append("<origen>").append(asignacion.getOrigen())
                        .append("</origen>");
                soapXml.append("<proveido>").append(asignacion.getProveido())
                        .append("</proveido>");
                soapXml.append("</ser:listAsignaciones>");
            }
            soapXml.append("<ser:idUsuario>").append(idUsuario)
                    .append("</ser:idUsuario>");
            soapXml.append("<ser:terminal>").append(terminal)
                    .append("</ser:terminal>");
            soapXml.append("</ser:centralizarTramiteSicod>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder())
                    .append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(
                    wsConfig.getLocation(),// "http://172.17.0.58:4080/wssicod/sicodsrv",
                    "text/xml;charset=UTF-8", "", soapXml.toString());

            StringBuilder resp = st.tokenizerString((new StringBuilder())
                    .append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + resp);

            Namespace ns = Namespace.getNamespace("http://www.services.uif/");
            element = response.getRootElement();
            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("centralizarTramiteSicodResponse", ns);
            result = element.getChildText("return", ns);

        } catch (Exception ex) {
            ex.printStackTrace();
            log.catching(ex);
        }
        log.info("Finalizando Metodo");
        return result;
    }

    public static boolean esTramiteSicod(String idProceso) {
        boolean result = false;
        Element element;
        // StringTokenizer st = new StringTokenizer();
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SicodClient.esTramiteSicod");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://www.services.uif/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<ser:esTramiteSicod>");
            soapXml.append("<ser:idProceso>").append(idProceso).append("</ser:idProceso>");
            soapXml.append("</ser:esTramiteSicod>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder())
                    .append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(
                    "http://172.17.0.58:4080/wssicod/sicodsrv",
                    "text/xml;charset=UTF-8", "", soapXml.toString());
            log.debug("el url de la peticion" + response);
            StringBuilder resp = st.tokenizerString((new StringBuilder())
                    .append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + resp);

            Namespace ns = Namespace.getNamespace("http://www.services.uif/");
            element = response.getRootElement();
            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("esTramiteSicodResponse", ns);
            result = Boolean.parseBoolean(element.getChildText("return", ns));
        } catch (Exception ex) {
            // ex.printStackTrace();
            log.catching(ex);
        }
        log.info("Finalizando Metodo");
        return result;
    }

    public static String recepcionarTramiteSicod(String idProceso,
            String idAsignacion, String idUserRecepcion, String idUser,
            String terminal, String estado) {
        String respuesta = "";

        Element element;
        // StringTokenizer st = new StringTokenizer();
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SicodClient.recepcionarTramiteSicod");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://www.services.uif/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<ser:recepcionarTramiteSicod>");
            soapXml.append("<ser:idProceso>").append(idProceso)
                    .append("</ser:idProceso>");
            soapXml.append("<ser:idAsignacion>").append(idAsignacion)
                    .append("</ser:idAsignacion>");
            soapXml.append("<ser:idUserRecepcion>").append(idUserRecepcion)
                    .append("</ser:idUserRecepcion>");
            soapXml.append("<ser:idUser>").append(idUser)
                    .append("</ser:idUser>");
            soapXml.append("<ser:terminal>").append(terminal)
                    .append("</ser:terminal>");
            soapXml.append("<ser:estado>").append(estado)
                    .append("</ser:estado>");
            soapXml.append("</ser:recepcionarTramiteSicod>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder())
                    .append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(
                    wsConfig.getLocation(),// "http://172.17.0.58:4080/wssicod/sicodsrv",
                    "text/xml;charset=UTF-8", "", soapXml.toString());

            StringBuilder resp = st.tokenizerString((new StringBuilder())
                    .append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + resp);

            Namespace ns = Namespace.getNamespace("http://www.services.uif/");
            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("recepcionarTramiteSicodResponse", ns);
            respuesta = element.getChildText("return", ns);
        } catch (Exception ex) {
            // ex.printStackTrace();
            log.catching(ex);
        }
        log.info("Finalizando Metodo");
        return respuesta;
    }

    public static String revertirTramiteSicod(String idProceso,
            String idAsignacion, String idUsuarioOrigen, String idUser,
            String terminal) {
        String respuesta = "";

        Element element;
        // StringTokenizer st = new StringTokenizer();
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SicodClient.revertirTramiteSicod");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://www.services.uif/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<ser:revertirTramiteSicod>");
            soapXml.append("<ser:idProceso>").append(idProceso).append("</ser:idProceso>");
            soapXml.append("<ser:idAsignacion>").append(idAsignacion).append("</ser:idAsignacion>");
            soapXml.append("<ser:idUsuarioOrigen>").append(idUsuarioOrigen).append("</ser:idUsuarioOrigen>");
            soapXml.append("<ser:idUser>").append(idUser).append("</ser:idUser>");
            soapXml.append("<ser:terminal>").append(terminal).append("</ser:terminal>");
            soapXml.append("</ser:revertirTramiteSicod>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),// "http://172.17.0.58:4080/wssicod/sicodsrv",
                    "text/xml;charset=UTF-8", "", soapXml.toString());

            StringBuilder resp = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + resp);

            Namespace ns = Namespace.getNamespace("http://www.services.uif/");
            element = response.getRootElement();
            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("revertirTramiteSicodResponse", ns);
            respuesta = element.getChildText("return", ns);
        } catch (Exception ex) {
            // ex.printStackTrace();
            log.catching(ex);
        }
        log.info("Finalizando Metodo");
        return respuesta;
    }

    public static ProcesoBean verificartramiteSicod(String idProceso,
            String idUsuario) {
        ProcesoBean proceso = new ProcesoBean();

        Element element;
        // StringTokenizer st = new StringTokenizer();
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SicodClient.verificartramiteSicod");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://www.services.uif/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<ser:verificarTramitesicod>");
            soapXml.append("<ser:idProceso>").append(idProceso)
                    .append("</ser:idProceso>");
            soapXml.append("<ser:idUsuario>").append(idUsuario)
                    .append("</ser:idUsuario>");
            soapXml.append("</ser:verificarTramitesicod>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder())
                    .append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(
                    wsConfig.getLocation(),// "http://172.17.0.58:4080/wssicod/sicodsrv",
                    "text/xml;charset=UTF-8", "", soapXml.toString());

            StringBuilder resp = st.tokenizerString((new StringBuilder())
                    .append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + resp);

            Namespace ns = Namespace.getNamespace("http://www.services.uif/");
            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("verificarTramitesicodResponse", ns);
            element = element.getChild("return", ns);
            System.out.println("fechaCreacion: "
                    + element.getChildText("fechaCreacionProceso"));
            proceso.setFechaCreacionProceso(!"".equals(element
                    .getChildText("fechaCreacionProceso")) ? sdf.parse(element
                                    .getChildText("fechaCreacionProceso")) : null);
            proceso.setGestion(!"".equals(element.getChildText("gestion")) ? Long
                    .parseLong(element.getChildText("gestion")) : null);
            proceso.setIdProceso(!"".equals(element.getChildText("idProceso")) ? Long
                    .parseLong(element.getChildText("idProceso")) : null);
            proceso.setMensaje(!"".equals(element.getChildText("mensaje")) ? element
                    .getChildText("mensaje") : null);
            proceso.setReferencia(!"".equals(element.getChildText("referencia")) ? element
                    .getChildText("referencia") : null);
            proceso.setRespuesta(!"".equals(element.getChildText("respuesta")) ? Boolean
                    .valueOf(element.getChildText("respuesta")) : null);

        } catch (Exception ex) {
            // ex.printStackTrace();
            log.catching(ex);
        }
        log.info("Finalizando Metodo");
        return proceso;
    }

    public static byte[] documenetoPdfPorUid(String uid) {
        byte[] pdf = null;
        Element element;
        // StringTokenizer st = new StringTokenizer();
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SicodClient.documentoPdf");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://www.services.uif/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<ser:verDocumentoPDF>");
            soapXml.append("<ser:uid>").append(uid).append("</ser:uid>");
            soapXml.append("</ser:verDocumentoPDF>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder())
                    .append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(
                    wsConfig.getLocation(), "text/xml;charset=UTF-8", "",
                    soapXml.toString());

            StringBuilder resp = st.tokenizerString((new StringBuilder())
                    .append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + resp);

            Namespace ns = Namespace.getNamespace("http://www.services.uif/");
            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("verDocumentoPDFResponse", ns);
            // element = element.getChild("return", ns);
            BASE64Decoder decoder = new BASE64Decoder();
            pdf = decoder.decodeBuffer(element.getChildText("return", ns));

        } catch (Exception ex) {
            // ex.printStackTrace();
            log.catching(ex);

        }
        log.info("Finalizando Metodo");
        return pdf;
    }

    public static DocumentoBean buscaDocumentoSicodPorNumeroTramite(String numeroTramite, String tipoDocumento) {
        DocumentoBean documento = new DocumentoBean();
        Element element;
        // StringTokenizer st = new StringTokenizer();
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SicodClient.buscaDocumentoSicodPorNumeroTramite");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://www.services.uif/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<ser:buscaDocumentoSicodPorNumeroTramite>");
            soapXml.append("<ser:numeroTramite>").append(numeroTramite).append("</ser:numeroTramite>");
            soapXml.append("<ser:tipoDocumento>").append(tipoDocumento).append("</ser:tipoDocumento>");
            soapXml.append("</ser:buscaDocumentoSicodPorNumeroTramite>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");
            log.info("Consumiendo WS>");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder())
                    .append(requestLog));
            log.debug("Request : " + reqs);
            Document response = SoapHttpRequest.sendRequestPOST(
                    wsConfig.getLocation(),// "http://172.17.0.58:4080/wssicod/sicodsrv",
                    "text/xml;charset=UTF-8", "", soapXml.toString());
            StringBuilder resp = st.tokenizerString((new StringBuilder())
                    .append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + resp);
            Namespace ns = Namespace.getNamespace("http://www.services.uif/");
            element = response.getRootElement();
            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("buscaDocumentoSicodPorNumeroTramiteResponse", ns);
            element = element.getChild("return", ns);

            documento.setIdDocumento(!"".equals(element.getChildText("idDocumento")) ? element
                    .getChildText("idDocumento") : null);

            documento.setTipoDocumento(!"".equals(element.getChildText("tipoDocumento")) ? element
                    .getChildText("tipoDocumento") : null);

            documento.setClasificadorDocumento(!"".equals(element.getChildText("clasificadorDocumento")) ? element
                    .getChildText("clasificadorDocumento") : null);

            documento.setFechaDocumento(!"".equals(element
                    .getChildText("fechaDocumento")) ? sdf.parse(element
                                    .getChildText("fechaDocumento")) : null);

            documento.setFechaCreacion(!"".equals(element
                    .getChildText("fechaCreacion")) ? sdf.parse(element
                                    .getChildText("fechaCreacion")) : null);

            documento.setNumeroDocumento(!"".equals(element.getChildText("numeroDocumento")) ? element
                    .getChildText("numeroDocumento") : null);

            documento.setReferenciaDocumento(!"".equals(element.getChildText("referenciaDocumento")) ? element
                    .getChildText("referenciaDocumento") : null);

            documento.setEstado(!"".equals(element.getChildText("estado")) ? element
                    .getChildText("estado") : null);

            documento.setConclusiones(!"".equals(element.getChildText("conclusiones")) ? element
                    .getChildText("conclusiones") : null);

            documento.setIdArea(!"".equals(element.getChildText("idArea")) ? element
                    .getChildText("idArea") : null);

            documento.setIdProceso(!"".equals(element.getChildText("idProceso")) ? element
                    .getChildText("idProceso") : null);

            documento.setAdjuntoWord(!"".equals(element.getChildText("adjuntoWord")) ? Boolean
                    .valueOf(element.getChildText("adjuntoWord")) : null);

        } catch (Exception ex) {
            // ex.printStackTrace();
            documento.setConeccion(false);
            log.catching(ex);
        }
        log.info("Finalizando Metodo");
        return documento;
    }

    public static boolean verificaVariasAsignaciones(String numeroTramite) {
        boolean result = false;
        Element element;
        // StringTokenizer st = new StringTokenizer();
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SicodClient.verificaVariasAsignaciones");
        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://www.services.uif/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<ser:verificaVariasAsignaciones>");
            soapXml.append("<ser:numeroTramite>").append(numeroTramite).append("</ser:numeroTramite>");
            soapXml.append("</ser:verificaVariasAsignaciones>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder())
                    .append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(
                    "http://172.17.0.58:4080/wssicod/sicodsrv",
                    "text/xml;charset=UTF-8", "", soapXml.toString());
            log.debug("el url de la peticion" + response);
            StringBuilder resp = st.tokenizerString((new StringBuilder())
                    .append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + resp);

            Namespace ns = Namespace.getNamespace("http://www.services.uif/");
            element = response.getRootElement();
            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("verificaVariasAsignacionesResponse", ns);
            result = Boolean.parseBoolean(element.getChildText("return", ns));
        } catch (Exception ex) {
            // ex.printStackTrace();
            log.catching(ex);
        }
        log.info("Finalizando Metodo");
        return result;
    }

}
