package bo.gob.uif.webclients.aduana.util;

import bo.gob.uif.webclients.aduana.beans.PersonaConsultaBean;
import bo.gob.uif.webclients.aduana.beans.TestBean;
import bo.gob.uif.webclients.cfg.MainWebServicesConfig;
import bo.gob.uif.webclients.cfg.WebServiceConfig;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sun.misc.BASE64Decoder;

import javax.crypto.Cipher;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;

/**
 * Created by Vladimir.Callisaya on 16/06/2017.
 */
public class Util {

    public static WebServiceConfig wsConfig = MainWebServicesConfig.INSTANCE.getWebServiceConfig("ADUANAConsultaSerWeb");
    private static Logger log;

    public static PrivateKey generarPrivateKey(String ubicacion) throws Exception {
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("AduanaClient.consultaTestAnb");
        PrivateKey llavePrivada = null;

        try {
            File f = new File(ubicacion);
            FileInputStream fis = new FileInputStream(f);
            DataInputStream dis = new DataInputStream(fis);
            byte[] keyBytes = new byte[(int) f.length()];
            dis.readFully(keyBytes);
            dis.close();
            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            llavePrivada = kf.generatePrivate(spec);
        } catch (Exception ex) {
            log.error("No se pudo generar la llave privada: " + ex);
            throw ex;
        }
        return llavePrivada;
    }

    public static String cifrarMensajeTest(TestBean nombre, PrivateKey llavePrivada) throws Exception {
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("AduanaClient.consultaTestAnb");

        StringBuilder respuesta = new StringBuilder();
        String respuesta2;
        String resultado;

        try {
            /*resultado = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
             resultado = resultado + createDocumentXml(nombre, "UTF-8");*/
            resultado = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><testAnb><nombre>PRUEBA ANB</nombre></testAnb>";

            Cipher rsa;
            rsa = Cipher.getInstance("RSA");
            rsa.init(Cipher.ENCRYPT_MODE, llavePrivada);
            int bloque_cant = resultado.length() / 245;
            int bloque_fin = resultado.length() % 245;

            /*for(int i = 1; i <= bloque_cant; i++){
             int cont = 245;
             if(i == 1){
             byte[] msg = resultado.substring(0, cont).getBytes("UTF-8");
             byte[] doFinal = rsa.doFinal(msg);
             respuesta.append("<cadenaXML>").append(new String(org.bouncycastle.util.encoders.Base64.encode(doFinal), "UTF-8")).append("</cadenaXML>");
             }else{
             byte[] msg = resultado.substring(cont, (245 * i)).getBytes("UTF-8");
             byte[] doFinal = rsa.doFinal(msg);
             respuesta.append("<cadenaXML>").append(new String(org.bouncycastle.util.encoders.Base64.encode(doFinal), "UTF-8")).append("</cadenaXML>");
             cont = cont * i;
             }
             }

             if(bloque_fin != 0){
             byte[] msg = resultado.substring((resultado.length() - bloque_fin), resultado.length()).getBytes("UTF-8");
             byte[] doFinal = rsa.doFinal(msg);
             respuesta.append("<cadenaXML>").append(new String(org.bouncycastle.util.encoders.Base64.encode(doFinal), "UTF-8")).append("</cadenaXML>");
             }*/
            byte[] doFinal = rsa.doFinal(resultado.getBytes("UTF-8"));
            respuesta2 = new String(org.bouncycastle.util.encoders.Base64.encode(doFinal), "UTF-8");

        } catch (Exception ex) {
            log.catching(ex);
            throw ex;
        }
        return respuesta2;
    }

    public static ArrayList<String> cifrarMensajeCie(PersonaConsultaBean persona, PrivateKey llavePrivada) throws Exception {
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("AduanaClient.consultaCieAnb");
        String resultado = "";
        ArrayList<String> listaCifrada = new ArrayList<String>();

        try {
            resultado = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";
            resultado = resultado + createDocumentXml(persona, "UTF-8");

            log.debug(resultado);
            Cipher rsa;
            rsa = Cipher.getInstance("RSA");
            rsa.init(Cipher.ENCRYPT_MODE, llavePrivada);
            do {
                byte[] doFinal;
                if (resultado.length() > 240) {
                    doFinal = rsa.doFinal(resultado.substring(0, 240).getBytes("UTF8"));
                    log.debug("cifrar: " + resultado.substring(0, 240));
                    resultado = resultado.substring(240);
                } else {
                    doFinal = rsa.doFinal(resultado.getBytes("UTF8"));
                    log.debug("cifrar: " + resultado.substring(0, resultado.length()));
                    resultado = "";
                }
                listaCifrada.add(new String(org.bouncycastle.util.encoders.Base64.encode(doFinal), "UTF-8"));
            } while (resultado.length() > 0);

            log.debug("listaArray: " + listaCifrada);
        } catch (Exception ex) {
            log.catching(ex);
            throw ex;
        }
        return listaCifrada;
    }

    public static String createDocumentXml(Object obj, String encoding) throws Exception {
        JAXBContext context = JAXBContext.newInstance(obj.getClass());
        Marshaller m = context.createMarshaller();
        String xml = null;

        if (encoding != null) {
            m.setProperty("jaxb.encoding", encoding);
            m.setProperty(Marshaller.JAXB_FRAGMENT, true);
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        m.marshal(obj, baos);

        if (encoding != null) {
            xml = baos.toString(encoding);
        } else {
            xml = baos.toString();
        }
        return xml;
    }

    public static PublicKey generarPublicKey(String ruta) throws Exception {
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("AduanaClient.consultaTestAnb");

        PublicKey publicKey = null;
        try {
            File f = new File(ruta);
            FileInputStream fis = new FileInputStream(f);
            DataInputStream dis = new DataInputStream(fis);
            byte[] keyBytes = new byte[(int) f.length()];
            dis.readFully(keyBytes);
            dis.close();
            X509EncodedKeySpec spec1 = new X509EncodedKeySpec(keyBytes);
            KeyFactory kf1 = KeyFactory.getInstance("RSA");
            publicKey = kf1.generatePublic(spec1);
        } catch (Exception ex) {
            log.catching(ex);
            throw ex;
        }
        return publicKey;
    }

    public static String descifrarMensajeTest(String mensaje, PublicKey llavePublica) {
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("AduanaClient.consultaTestAnb");
        mensaje = "k1JtjHyV9Q6IogcMt3UrESHwjJZQxjMm4RX0Xbygybfbk2131vA8kJECqbxmQXyo6fYyFCYSeMZDET7XWzWCFZY/gStHkMLhHOv6i1LkC4HURV2WMTpPTrghV7erJf9hZ/siCLMhEftSWWovovwrx9RUX5zCChJPP3UwHduP3XT/jX1/th2SSgFBYE6BXJt1nM4uVY9b46z4eCtdbtCQaMkJ0G3Xz2B1cY+Fye3P8ueJw5UUAe0WJX6BVcakaKhsltDs89uU808UTGky/EotnemlEL19GQv6raEszdQbdGXIiF68bvS4MNjrql18HZM/THykM8r4fowG/yMIpOIJCA==";
        //mensaje = "f3euHnyf1Mztwj+jdpwYC3hbCJcBcHk8RiuHXxnTs/bsGuKhLp9PzcIVhLxp75TlWFSMWV0h6ppYreg5YD5bTTts/wTwzEKCuDYceLinXMT3JueLTMvgc5GKsj60TMzlUYqJ2eDdiZbQmTUj+McLgBhMpZUh8y+ZRXubLLZFm+zb3e37GeIDFR4FARB4/WmdRCvhDzIHHfspD7sZDSgWLnKM+2YUu72KpH6c+1sOUyo19S18gDZ26rKO+/wJ28cwsCh6ygdIVREPYQw7/w95+dGvD+k4XENDC7Eta2A73dd/bktxLgEqPP6yaF0eTrfad7CO7i/A7ux0O16EwbcI5g==";
        String resultado = "";
        try {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, llavePublica);
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] raw = decoder.decodeBuffer(mensaje);
            //byte[] raw = Base64.decode(mensaje);
            byte[] stringBytes = cipher.doFinal(raw);
            resultado = new String(stringBytes, "UTF-8");

        } catch (Exception ex) {
            log.catching(ex);
        }
        return resultado;
    }

    public static String descifrarMensajeCie(String mensaje, PublicKey llavePublica) {
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("AduanaClient.consultaCieAnb");
        String resultado = "";
        try {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, llavePublica);
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] raw = decoder.decodeBuffer(mensaje);
            byte[] stringBytes = cipher.doFinal(raw);
            resultado = new String(stringBytes, "UTF-8");

        } catch (Exception ex) {
            log.catching(ex);
        }
        return resultado;
    }
}
