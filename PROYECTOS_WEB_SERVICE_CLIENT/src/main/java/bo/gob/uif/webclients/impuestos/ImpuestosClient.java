package bo.gob.uif.webclients.impuestos;

import bo.gob.uif.webclients.common.SoapHttpRequest;
import bo.gob.uif.webclients.impuestos.beans.*;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import bo.gob.uif.webclients.cfg.MainWebServicesConfig;
import bo.gob.uif.webclients.cfg.WebServiceConfig;
import bo.gob.uif.webclients.common.StringTokenizer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.output.XMLOutputter;

/**
 * Created by Vladimir.Callisaya on 10/08/2016.
 *
 * Modificado: Susana Escobar Paz Fecha: 17/05/2018
 */
public class ImpuestosClient {

    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    public static WebServiceConfig wsConfig = MainWebServicesConfig.INSTANCE.getWebServiceConfig("SINConsultaSerWeb");
    private static Logger log;
    private static StringTokenizer st = new StringTokenizer();

    public static DatosContribuyenteActualBean consultarDatosContribuyenteActuales(long nit, String usuario) {
        DatosContribuyenteActualBean result = new DatosContribuyenteActualBean();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("ImpuestosClient.consultarDatosContribuyenteActuales");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:imp=\"http://www.impuestos.gob.bo/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<imp:ConsultarDatosContribuyenteActuales>");
            soapXml.append("<imp:mensajeSolicitud>");
            soapXml.append("<imp:Nit>").append(nit).append("</imp:Nit>");
            soapXml.append("<imp:Usuario>").append(usuario).append("</imp:Usuario>");
            soapXml.append("</imp:mensajeSolicitud>");
            soapXml.append("</imp:ConsultarDatosContribuyenteActuales>\n");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),
                    "text/xml;charset=UTF-8",
                    "http://www.impuestos.gob.bo/ConsultarDatosContribuyenteActuales",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + reps);

            Namespace ns = Namespace.getNamespace("http://www.impuestos.gob.bo/");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("ConsultarDatosContribuyenteActualesResponse", ns);
            element = element.getChild("ConsultarDatosContribuyenteActualesResult", ns);

            result.setNit(!"".equals(element.getChildText("Nit", ns)) ? Long.parseLong(element.getChildText("Nit", ns)) : 0);
            result.setNombreRazonSocial(!"".equals(element.getChildText("NombreRazonSocial", ns)) ? element.getChildText("NombreRazonSocial", ns) : null);
            result.setCedula(!"".equals(element.getChildText("Cedula", ns)) ? element.getChildText("Cedula", ns) : null);
            result.setDescripcionJurisdiccion(!"".equals(element.getChildText("DescripcionJurisdiccion", ns)) ? element.getChildText("DescripcionJurisdiccion", ns) : null);
            result.setDireccionFiscal(!"".equals(element.getChildText("DireccionFiscal", ns)) ? element.getChildText("DireccionFiscal", ns) : null);
            result.setFechaInscripcion(!"".equals(element.getChildText("FechaInscripcion", ns)) ? sdf.parse(element.getChildText("FechaInscripcion", ns)) : null);
            result.setFechaUltimoEstado(!"".equals(element.getChildText("FechaUltimoEstado", ns)) ? sdf.parse(element.getChildText("FechaUltimoEstado", ns)) : null);
            result.setDescripcionEstado(!"".equals(element.getChildText("DescripcionEstado", ns)) ? element.getChildText("DescripcionEstado", ns) : null);
            result.setDescripcionRegimen(!"".equals(element.getChildText("DescripcionRegimen", ns)) ? element.getChildText("DescripcionRegimen", ns) : null);

            element = element.getChild("ActividadesEconomicas", ns);

            List<Element> listElemAct = new ArrayList<Element>();
            if (element != null) {
                listElemAct = element.getChildren("MitActividadEconomica", ns);

                List<ActividadEconomicaBean> listaActividades = new ArrayList<ActividadEconomicaBean>();
                for (Element elem : listElemAct) {
                    ActividadEconomicaBean actividadEconomicaBean = new ActividadEconomicaBean();
                    actividadEconomicaBean.setDescripcionActividad(!"".equals(elem.getChildText("DescripcionActividad", ns)) ? elem.getChildText("DescripcionActividad", ns) : null);
                    actividadEconomicaBean.setDescripcionTipoActividad(!"".equals(elem.getChildText("DescripcionTipoActividad", ns)) ? elem.getChildText("DescripcionTipoActividad", ns) : null);
                    actividadEconomicaBean.setFechaDesde(!"".equals(elem.getChildText("FechaDesde", ns)) ? sdf.parse(elem.getChildText("FechaDesde", ns)) : null);
                    actividadEconomicaBean.setFechaHasta(!"".equals(elem.getChildText("FechaHasta", ns)) ? sdf.parse(elem.getChildText("FechaHasta", ns)) : null);
                    listaActividades.add(actividadEconomicaBean);
                }
                result.setActividadEconomicaBean(listaActividades);

                element = element.getParentElement();
                element = element.getChild("ObligacionesTributarias", ns);
                List<Element> listElemObTrib = element.getChildren("MitObligacionTributaria", ns);
                List<ObligacionTributariaBean> listaObligacionesTributarias = new ArrayList<ObligacionTributariaBean>();
                for (Element elem : listElemObTrib) {
                    ObligacionTributariaBean obligacionTributariaBean = new ObligacionTributariaBean();
                    obligacionTributariaBean.setDescripcionObligacion(!"".equals(elem.getChildText("DescripcionObligacion", ns)) ? elem.getChildText("DescripcionObligacion", ns) : null);
                    obligacionTributariaBean.setFechaDesde(!"".equals(elem.getChildText("FechaDesde", ns)) ? sdf.parse(elem.getChildText("FechaDesde", ns)) : null);
                    obligacionTributariaBean.setFechaHasta(!"".equals(elem.getChildText("FechaHasta", ns)) ? sdf.parse(elem.getChildText("FechaHasta", ns)) : null);

                    listaObligacionesTributarias.add(obligacionTributariaBean);
                }
                result.setObligacionTributariaBean(listaObligacionesTributarias);

                element = element.getParentElement();
                element = element.getChild("PersonasContribuyente", ns);
                List<Element> listElemPerContrib = element.getChildren("MitPersonasContribuyente", ns);
                List<PersonaContribuyenteBean> listaPersonasContribuyentes = new ArrayList<PersonaContribuyenteBean>();
                for (Element elem : listElemPerContrib) {
                    PersonaContribuyenteBean personaContribuyenteBean = new PersonaContribuyenteBean();
                    personaContribuyenteBean.setCedula(!"".equals(elem.getChildText("Cedula", ns)) ? elem.getChildText("Cedula", ns) : null);
                    personaContribuyenteBean.setNombreCompleto(!"".equals(elem.getChildText("NombreCompleto", ns)) ? elem.getChildText("NombreCompleto", ns) : null);

                    listaPersonasContribuyentes.add(personaContribuyenteBean);
                }
                result.setPersonaContribuyenteBean(listaPersonasContribuyentes);
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result.setConexion(false);
            log.catching(e);
        }

        log.info("Finalizando Metodo");
        return result;
    }

    //public static List<PersonaNatural> consultarDatosPersonaNatural(long ci, String paterno, String materno, String nombres, String usuario){
    public static List<PersonaNatural> consultarDatosPersonaNatural(long ci, String usuario) {

        List<PersonaNatural> result = new ArrayList<PersonaNatural>();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("ImpuestosClient.consultarDatosPersonaNatural");
        log.info("Iniciando Metodo");

        try {

            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:imp=\"http://www.impuestos.gob.bo/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<imp:ConsultarDatosPersonaNatural>");
            soapXml.append("<imp:mensajeSolicitud>");
            soapXml.append("<imp:Cedula>").append(ci).append("</imp:Cedula>");
            /*soapXml.append("<imp:ApellidoPaterno>").append(paterno).append("</imp:ApellidoPaterno>");
             soapXml.append("<imp:ApellidoMaterno>").append(materno).append("</imp:ApellidoMaterno>");
             soapXml.append("<imp:Nombres>").append(nombres).append("</imp:Nombres>");*/
            soapXml.append("<imp:ApellidoPaterno />");
            soapXml.append("<imp:ApellidoMaterno />");
            soapXml.append("<imp:Nombres />");
            soapXml.append("<imp:Usuario>").append(usuario).append("</imp:Usuario>");
            soapXml.append("</imp:mensajeSolicitud>");
            soapXml.append("</imp:ConsultarDatosPersonaNatural>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),
                    "text/xml;charset=UTF-8",
                    "http://www.impuestos.gob.bo/ConsultarDatosPersonaNatural",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + reps);

            Namespace ns = Namespace.getNamespace("http://www.impuestos.gob.bo/");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());

            element = element.getChild("ConsultarDatosPersonaNaturalResponse", ns);
            element = element.getChild("ConsultarDatosPersonaNaturalResult", ns);
            element = element.getChild("MitPersonaNatural", ns);

            List<Element> listElemPerNat = element.getChildren("MitPersonaNatural", ns);

            if (!listElemPerNat.isEmpty()) {
                for (Element elem : listElemPerNat) {
                    if (elem.getChildText("Nit", ns) != null && elem.getChildText("RazonSocial", ns) != null && elem.getChildText("DescripcionTipoPersona", ns) != null) {
                        PersonaNatural personaNatural = new PersonaNatural();
                        personaNatural.setNit(Long.parseLong(elem.getChildText("Nit", ns)));
                        personaNatural.setRazonSocial(elem.getChildText("RazonSocial", ns));
                        personaNatural.setDescripcionPersonaNatural(elem.getChildText("DescripcionTipoPersona", ns));
                        result.add(personaNatural);
                    }
                }
            }
            if (result.size() == 0) {
                PersonaNatural personaNatural = new PersonaNatural();
                personaNatural.setNit(0);
                personaNatural.setRazonSocial(null);
                personaNatural.setDescripcionPersonaNatural(null);
                result.add(personaNatural);
            }

        } catch (Exception e) {
            PersonaNatural personaNatural = new PersonaNatural();
            personaNatural.setNit(0);
            personaNatural.setRazonSocial(null);
            personaNatural.setDescripcionPersonaNatural(null);
            personaNatural.setConexion(false);
            result.add(personaNatural);
            log.catching(e);
        }

        log.info("Finalizando Metodo");
        return result;
    }

    public static List<UifEeffBean> consultarEeff(long nit, int gestion, String usuario) {
        List<UifEeffBean> result = new ArrayList<UifEeffBean>();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("ImpuestosClient.consultarEeff");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:imp=\"http://www.impuestos.gob.bo/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<imp:ConsultarEEFF>");
            soapXml.append("<imp:Nit>").append(nit).append("</imp:Nit>");
            soapXml.append("<imp:Gestion>").append(gestion).append("</imp:Gestion>");
            soapXml.append("<imp:Usuario>").append(usuario).append("</imp:Usuario>");
            soapXml.append("</imp:ConsultarEEFF>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),
                    "text/xml;charset=UTF-8",
                    "http://www.impuestos.gob.bo/ConsultarEEFF",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + reps);

            Namespace ns = Namespace.getNamespace("http://www.impuestos.gob.bo/");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("ConsultarEEFFResponse", ns);
            element = element.getChild("ConsultarEEFFResult", ns);
            element = element.getChild("DetalleEEFF", ns);

            if (element != null) {
                List<Element> listElementEeff = element.getChildren("UifEEFF", ns);

                for (Element elem : listElementEeff) {
                    UifEeffBean tmpUifEeff = new UifEeffBean();
                    tmpUifEeff.setNroOrden(!"".equals(elem.getChildText("NroOrden", ns)) ? Long.parseLong(elem.getChildText("NroOrden", ns)) : 0);
                    tmpUifEeff.setAnio(!"".equals(elem.getChildText("Anio", ns)) ? Integer.parseInt(elem.getChildText("Anio", ns)) : 0);
                    tmpUifEeff.setPeriodo(!"".equals(elem.getChildText("Periodo", ns)) ? Integer.parseInt(elem.getChildText("Periodo", ns)) : 0);
                    tmpUifEeff.setRegProfesional(!"".equals(elem.getChildText("RegProfesional", ns)) ? elem.getChildText("RegProfesional", ns) : null);
                    tmpUifEeff.setNitAuditorElabora(!"".equals(elem.getChildText("NitAuditorElabora", ns)) ? Long.parseLong(elem.getChildText("NitAuditorElabora", ns)) : 0);
                    tmpUifEeff.setNombreProfContable(!"".equals(elem.getChildText("NombreProfContable", ns)) ? elem.getChildText("NombreProfContable", ns) : null);
                    tmpUifEeff.setCiAuditorRevisa(!"".equals(elem.getChildText("CiAuditorRevisa", ns)) ? elem.getChildText("CiAuditorRevisa", ns) : null);
                    tmpUifEeff.setCiAuditorElabora(!"".equals(elem.getChildText("CiAuditorElabora", ns)) ? elem.getChildText("CiAuditorElabora", ns) : null);
                    tmpUifEeff.setNombreFirma1(!"".equals(elem.getChildText("NombreFirma1", ns)) ? elem.getChildText("NombreFirma1", ns) : null);
                    tmpUifEeff.setCargoFirma1(!"".equals(elem.getChildText("CargoFirma1", ns)) ? elem.getChildText("CargoFirma1", ns) : null);
                    tmpUifEeff.setCiFirma1(!"".equals(elem.getChildText("CiFirma1", ns)) ? elem.getChildText("CiFirma1", ns) : null);
                    tmpUifEeff.setNombreFirma2(!"".equals(elem.getChildText("NombreFirma2", ns)) ? elem.getChildText("NombreFirma2", ns) : null);
                    tmpUifEeff.setCargoFirma2(!"".equals(elem.getChildText("CargoFirma2", ns)) ? elem.getChildText("CargoFirma2", ns) : null);
                    tmpUifEeff.setCiFirma2(!"".equals(elem.getChildText("CiFirma2", ns)) ? elem.getChildText("CiFirma2", ns) : null);
                    tmpUifEeff.setNombreFirma3(!"".equals(elem.getChildText("NombreFirma3", ns)) ? elem.getChildText("NombreFirma3", ns) : null);
                    tmpUifEeff.setCargoFirma3(!"".equals(elem.getChildText("CargoFirma3", ns)) ? elem.getChildText("CargoFirma3", ns) : null);
                    tmpUifEeff.setCiFirma3(!"".equals(elem.getChildText("CiFirma3", ns)) ? elem.getChildText("CiFirma3", ns) : null);
                    tmpUifEeff.setNombreFirma4(!"".equals(elem.getChildText("NombreFirma4", ns)) ? elem.getChildText("NombreFirma4", ns) : null);
                    tmpUifEeff.setCargoFirma4(!"".equals(elem.getChildText("CargoFirma4", ns)) ? elem.getChildText("CargoFirma4", ns) : null);
                    tmpUifEeff.setCiFirma4(!"".equals(elem.getChildText("CiFirma4", ns)) ? elem.getChildText("CiFirma4", ns) : null);
                    tmpUifEeff.setOpinion(!"".equals(elem.getChildText("Opinion", ns)) ? element.getChildText("Opinion", ns) : null);

                    Element elementDetalleEeff = elem.getChild("DetalleEEFF", ns);
                    List<Element> listDetEeff = elementDetalleEeff.getChildren("UifDetalleEEFF", ns);
                    List<DetalleEeffBean> listElementDetalleEeff = new ArrayList<DetalleEeffBean>();

                    for (Element elemDet : listDetEeff) {
                        DetalleEeffBean detalleEeffBean = new DetalleEeffBean();
                        detalleEeffBean.setCodigo(elemDet.getChildText("Codigo", ns));
                        detalleEeffBean.setFormulario(elemDet.getChildText("Formulario", ns));
                        detalleEeffBean.setTotal(Float.parseFloat(elemDet.getChildText("Total", ns)));
                        listElementDetalleEeff.add(detalleEeffBean);
                    }
                    tmpUifEeff.setDetalleEeff(listElementDetalleEeff);
                    result.add(tmpUifEeff);
                }
            }
            //result.add(tmpUifEeff);
        } catch (Exception e) {
            //e.printStackTrace();
            log.catching(e);
            UifEeffBean tmpUifEeff = new UifEeffBean();
            tmpUifEeff.setConexion(false);
            result.add(tmpUifEeff);
        }
        log.info("Finalizando Metodo");
        return result;
    }

    public static List<UifExtracIvaBean> consultarExtractoIva(long nit, int desde, int hasta, String usuario) {
        List<UifExtracIvaBean> result = new ArrayList<UifExtracIvaBean>();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("ImpuestosClient.consultarExtractoIva");
        log.info("Iniciando Metodo");

        try {

            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:imp=\"http://www.impuestos.gob.bo/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<imp:ConsultarExtractoIVA>");
            soapXml.append("<imp:mensajeSolicitud>");
            soapXml.append("<imp:Nit>").append(nit).append("</imp:Nit>");
            soapXml.append("<imp:PeriodoDesde>").append(desde).append("</imp:PeriodoDesde>");
            soapXml.append("<imp:PeriodoHasta>").append(hasta).append("</imp:PeriodoHasta>");
            soapXml.append("<imp:Usuario>").append(usuario).append("</imp:Usuario>");
            soapXml.append("</imp:mensajeSolicitud>");
            soapXml.append("</imp:ConsultarExtractoIVA>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),
                    "text/xml;charset=UTF-8",
                    "http://www.impuestos.gob.bo/ConsultarExtractoIVA",
                    soapXml.toString());

            Namespace ns = Namespace.getNamespace("http://www.impuestos.gob.bo/");

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + reps);

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());

            element = element.getChild("ConsultarExtractoIVAResponse", ns);
            element = element.getChild("ConsultarExtractoIVAResult", ns);
            element = element.getChild("DetalleExtractoIva", ns);
            if (element != null) {
                List<Element> listDetIva = element.getChildren("UifExtractoIva", ns);

                List<UifExtracIvaBean> listIva = new ArrayList<UifExtracIvaBean>();

                if (!listDetIva.isEmpty()) {
                    for (Element elem : listDetIva) {
                        UifExtracIvaBean detExtracIva = new UifExtracIvaBean();
                        detExtracIva.setAnio(Integer.parseInt(elem.getChildText("Anio", ns)));
                        detExtracIva.setMes(Integer.parseInt(elem.getChildText("Mes", ns)));
                        detExtracIva.setFormulario(Integer.parseInt(elem.getChildText("Formulario", ns)));
                        detExtracIva.setDescripcionCasilla(elem.getChildText("DescripcionCasilla", ns));
                        detExtracIva.setValor(Long.parseLong(elem.getChildText("Valor", ns)));
                        listIva.add(detExtracIva);
                    }
                }
                result = listIva;
            }
        } catch (Exception e) {
            UifExtracIvaBean detExtracIva = new UifExtracIvaBean();
            detExtracIva.setConexion(false);
            result.add(detExtracIva);
            //e.printStackTrace();
            log.catching(e);
        }
        log.info("Finalizando Metodo");
        return result;
    }

    public static List<DetalleExtractoTributarioBean> consultarExtractoTributario(long nit, int desde, int hasta, String usuario) {
        List<DetalleExtractoTributarioBean> result = new ArrayList<DetalleExtractoTributarioBean>();
        List<DetalleExtractoTributarioBean> listDetExtracTrib = new ArrayList<DetalleExtractoTributarioBean>();
        List<Element> listExtracTrib = new ArrayList<Element>();
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("ImpuestosClient.consultarExtractoTributario");
        log.info("Iniciando Metodo");
        try {

            StringBuilder soapXml = new StringBuilder();

            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:imp=\"http://www.impuestos.gob.bo/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<imp:ConsultarExtractoTributario>");
            soapXml.append("<imp:mensajeSolicitud>");
            soapXml.append("<imp:Nit>").append(nit).append("</imp:Nit>");
            soapXml.append("<imp:PeriodoDesde>").append(desde).append("</imp:PeriodoDesde>");
            soapXml.append("<imp:PeriodoHasta>").append(hasta).append("</imp:PeriodoHasta>");
            soapXml.append("<imp:Usuario>").append(usuario).append("</imp:Usuario>");
            soapXml.append("</imp:mensajeSolicitud>");
            soapXml.append("</imp:ConsultarExtractoTributario>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),
                    "text/xml;charset=UTF-8",
                    "http://www.impuestos.gob.bo/ConsultarExtractoTributario",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + reps);

            Namespace ns = Namespace.getNamespace("http://www.impuestos.gob.bo/");

            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("ConsultarExtractoTributarioResponse", ns);
            element = element.getChild("ConsultarExtractoTributarioResult", ns);
            element = element.getChild("DetalleExtractoTributario", ns);
            try {
                listExtracTrib = element.getChildren("MitExtractoTributario", ns);
            } catch (Exception e) {
                // listExtracTrib.clear();
                listExtracTrib = new ArrayList<Element>();
            }

            if (!listExtracTrib.isEmpty()) {
                for (Element elem : listExtracTrib) {
                    DetalleExtractoTributarioBean detTrib = new DetalleExtractoTributarioBean();
                    detTrib.setAnio(Integer.parseInt(elem.getChildText("Anio", ns)));
                    detTrib.setMes(Integer.parseInt(elem.getChildText("Mes", ns)));
                    detTrib.setDescripcionFormulario(elem.getChildText("DescripcionFormulario", ns));
                    detTrib.setFechaPago(elem.getChildText("FechaPago", ns));
                    detTrib.setNumeroOrden(Long.parseLong(elem.getChildText("NumeroOrden", ns)));
                    detTrib.setImporteEnEfectivo(Float.parseFloat(elem.getChildText("ImporteEnEfectivo", ns)));
                    detTrib.setImporteEnValores(Float.parseFloat(elem.getChildText("ImporteEnValores", ns)));
                    detTrib.setImporteOtros(Float.parseFloat(elem.getChildText("ImporteOtros", ns)));
                    detTrib.setNumeroOrdenRectificada(Long.parseLong(elem.getChildText("NumeroOrdenRectificada", ns)));
                    detTrib.setDescripcionBanco(elem.getChildText("DescripcionBanco", ns));
                    listDetExtracTrib.add(detTrib);
                }
            }
            System.out.println("RESULTTTTT  -----------");
            result = listDetExtracTrib;
        } catch (Exception e) {
            //e.printStackTrace();
            log.catching(e);
            DetalleExtractoTributarioBean detTrib = new DetalleExtractoTributarioBean();
            detTrib.setConexion(false);
            result.add(detTrib);
            System.out.println("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
        }
        log.info("Finalizando Metodo");
        return result;
    }

}
