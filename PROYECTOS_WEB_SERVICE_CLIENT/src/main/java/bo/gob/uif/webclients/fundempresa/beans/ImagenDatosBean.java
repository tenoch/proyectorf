package bo.gob.uif.webclients.fundempresa.beans;

import java.io.Serializable;

/**
 * Created by Vladimir.Callisaya on 09/09/2016.
 */
public class ImagenDatosBean implements Serializable {

    private String ctrResult;
    private String idMatricula;
    private String numReg;
    private String idLibro;
    private String codImagen;
    private String rutaImagen;

    public String getCtrResult() {
        return ctrResult;
    }

    public void setCtrResult(String ctrResult) {
        this.ctrResult = ctrResult;
    }

    public String getIdMatricula() {
        return idMatricula;
    }

    public void setIdMatricula(String idMatricula) {
        this.idMatricula = idMatricula;
    }

    public String getNumReg() {
        return numReg;
    }

    public void setNumReg(String numReg) {
        this.numReg = numReg;
    }

    public String getIdLibro() {
        return idLibro;
    }

    public void setIdLibro(String idLibro) {
        this.idLibro = idLibro;
    }

    public String getCodImagen() {
        return codImagen;
    }

    public void setCodImagen(String codImagen) {
        this.codImagen = codImagen;
    }

    public String getRutaImagen() {
        return rutaImagen;
    }

    public void setRutaImagen(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }

    @Override
    public String toString() {
        return "ImagenDatosBean{"
                + "ctrResult='" + ctrResult + '\''
                + ", idMatricula='" + idMatricula + '\''
                + ", numReg='" + numReg + '\''
                + ", idLibro='" + idLibro + '\''
                + ", codImagen='" + codImagen + '\''
                + ", rutaImagen='" + rutaImagen + '\''
                + '}';
    }
}
