package bo.gob.uif.webclients.impuestos.beans;

import java.io.Serializable;

/**
 * Created by Vladimir.Callisaya on 11/08/2016.
 */
public class PersonaContribuyenteBean implements Serializable {

    private String cedula;
    private String nombreCompleto;

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }
}
