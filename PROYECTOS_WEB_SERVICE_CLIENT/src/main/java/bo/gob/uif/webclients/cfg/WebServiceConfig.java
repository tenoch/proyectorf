/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.gob.uif.webclients.cfg;

import bo.gob.uif.webclients.exceptions.WebServiceInternalException;

import java.util.Map;

/**
 *
 * @author Alan.Portugal
 */
public class WebServiceConfig {

    private String location;
    private String location2;
    private String idContract;
    private String keyContract;
    private String keyStorePriv;
    private String pathKeyStorePriv;
    private String keyStorePublic;
    private String pathKeyStorePublic;
    private String usuarioConvenio;
    private String contrasenaConvenio;
    private String codigoInstitucion;
    private String usuario;
    private String contrasena;
    private String location3;

    private Map<String, String> propierties;

    public WebServiceConfig(Map<String, String> propierties) {
        if (propierties == null) {
            throw new NullPointerException("El parametro propiedades es nulo");
        }
        this.propierties = propierties;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation2() {
        return location2;
    }

    public void setLocation2(String location2) {
        this.location2 = location2;
    }

    public String getIdContract() {
        return idContract;
    }

    public void setIdContract(String idContract) {
        this.idContract = idContract;
    }

    public String getKeyContract() {
        return keyContract;
    }

    public void setKeyContract(String keyContract) {
        this.keyContract = keyContract;
    }

    public String getKeyStorePriv() {
        return keyStorePriv;
    }

    public void setKeyStorePriv(String keyStorePriv) {
        this.keyStorePriv = keyStorePriv;
    }

    public String getPathKeyStorePriv() {
        return pathKeyStorePriv;
    }

    public void setPathKeyStorePriv(String pathKeyStorePriv) {
        this.pathKeyStorePriv = pathKeyStorePriv;
    }

    public String getKeyStorePublic() {
        return keyStorePublic;
    }

    public void setKeyStorePublic(String keyStorePublic) {
        this.keyStorePublic = keyStorePublic;
    }

    public String getPathKeyStorePublic() {
        return pathKeyStorePublic;
    }

    public void setPathKeyStorePublic(String pathKeyStorePublic) {
        this.pathKeyStorePublic = pathKeyStorePublic;
    }

    public String getContrasenaConvenio() {
        return contrasenaConvenio;
    }

    public void setContrasenaConvenio(String contrasenaConvenio) {
        this.contrasenaConvenio = contrasenaConvenio;
    }

    public String getUsuarioConvenio() {
        return usuarioConvenio;
    }

    public void setUsuarioConvenio(String usuarioConvenio) {
        this.usuarioConvenio = usuarioConvenio;
    }

    public String getCodigoInstitucion() {
        return codigoInstitucion;
    }

    public void setCodigoInstitucion(String codigoInstitucion) {
        this.codigoInstitucion = codigoInstitucion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getLocation3() {
        return location3;
    }

    public void setLocation3(String location3) {
        this.location3 = location3;
    }

    public String getParameterExtra(String propertieName) throws WebServiceInternalException {
        if (propertieName == null) {
            throw new RuntimeException("El parametro para obtener el valor de una propiedad es nulo");
        }
        String value = propierties.get(propertieName);
        if (value == null) {
            throw new WebServiceInternalException("No se pudo encontrar el parametro [" + propertieName + "] en el archivo de propiedades");
        }
        return value;
    }
}
