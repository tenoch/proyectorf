package bo.gob.uif.webclients.aduana.beans;

import java.util.List;

/**
 * Created by Vladimir.Callisaya on 28/09/2017.
 */
public class RespuestaAduanaExportacionBean {

    private boolean respuestaDatos;
    private String faultCode;
    private String faultString;
    private List<ExportacionBean> listaExportaciones;

    public boolean isRespuestaDatos() {
        return respuestaDatos;
    }

    public void setRespuestaDatos(boolean respuestaDatos) {
        this.respuestaDatos = respuestaDatos;
    }

    public String getFaultCode() {
        return faultCode;
    }

    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }

    public String getFaultString() {
        return faultString;
    }

    public void setFaultString(String faultString) {
        this.faultString = faultString;
    }

    public List<ExportacionBean> getListaExportaciones() {
        return listaExportaciones;
    }

    public void setListaExportaciones(List<ExportacionBean> listaExportaciones) {
        this.listaExportaciones = listaExportaciones;
    }
}
