package bo.gob.uif.webclients.sicod.beans;

import java.io.Serializable;

/**
 * Created by Vladimir.Callisaya on 12/08/2016.
 */
public class TramiteAsignacionBean implements Serializable {

    private int fojas;
    private String idUsuarioDestino;
    private String nombredestino;
    private String numCopia;
    private String proveido;
    private String tipoAsignacion;
    private String tipoAsignacionDesc;

    public int getFojas() {
        return fojas;
    }

    public void setFojas(int fojas) {
        this.fojas = fojas;
    }

    public String getIdUsuarioDestino() {
        return idUsuarioDestino;
    }

    public void setIdUsuarioDestino(String idUsuarioDestino) {
        this.idUsuarioDestino = idUsuarioDestino;
    }

    public String getNombredestino() {
        return nombredestino;
    }

    public void setNombredestino(String nombredestino) {
        this.nombredestino = nombredestino;
    }

    public String getNumCopia() {
        return numCopia;
    }

    public void setNumCopia(String numCopia) {
        this.numCopia = numCopia;
    }

    public String getProveido() {
        return proveido;
    }

    public void setProveido(String proveido) {
        this.proveido = proveido;
    }

    public String getTipoAsignacion() {
        return tipoAsignacion;
    }

    public void setTipoAsignacion(String tipoAsignacion) {
        this.tipoAsignacion = tipoAsignacion;
    }

    public String getTipoAsignacionDesc() {
        return tipoAsignacionDesc;
    }

    public void setTipoAsignacionDesc(String tipoAsignacionDesc) {
        this.tipoAsignacionDesc = tipoAsignacionDesc;
    }
}
