package bo.gob.uif.webclients.exceptions;

/**
 * Created by Vladimir.Callisaya on 11/08/2016.
 */
public class WebServiceConnectionException extends Exception {

    public WebServiceConnectionException(String message) {
        super(message);
    }

    public WebServiceConnectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
