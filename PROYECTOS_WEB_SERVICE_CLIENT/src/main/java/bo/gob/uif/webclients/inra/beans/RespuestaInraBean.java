/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.gob.uif.webclients.inra.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Susana.Escobar
 *
 * Creado por: Susana Escobar Paz:
 *
 * Descripcion: mapeo de la entidad para almacenado de respuesta de la consulta
 * al servicio rest del Inra, se usa en la implementacion de metodos de la clase
 * InraClient
 *
 * Fecha: 30/05/2018
 */
public class RespuestaInraBean implements Serializable {

    private String numeroTitulo;
    private Date fechaTitulo;
    private String nombrePredio;
    private Double superficie;
    private String distribucion;
    private String claseTitulo;
    private String calificacion;
    private String clasificacion;
    private String departamento;
    private String provincia;
    private String municipio;
    private String canton;
    private String codigoCatastral;
    private String presidente;
    private String director;
    private String resolucionTitulacion;
    private Date fechaResolucionTitulacion;
    private List<BeneficiarioBean> beneficiariosList;
    private Boolean conexion = true;
    private Boolean datoEncontrado;
    private String mensaje;

    public String getNumeroTitulo() {
        return numeroTitulo;
    }

    public void setNumeroTitulo(String numeroTitulo) {
        this.numeroTitulo = numeroTitulo;
    }

    public Date getFechaTitulo() {
        return fechaTitulo;
    }

    public void setFechaTitulo(Date fechaTitulo) {
        this.fechaTitulo = fechaTitulo;
    }

    public String getNombrePredio() {
        return nombrePredio;
    }

    public void setNombrePredio(String nombrePredio) {
        this.nombrePredio = nombrePredio;
    }

    public Double getSuperficie() {
        return superficie;
    }

    public void setSuperficie(Double superficie) {
        this.superficie = superficie;
    }

    public String getDistribucion() {
        return distribucion;
    }

    public void setDistribucion(String distribucion) {
        this.distribucion = distribucion;
    }

    public String getClaseTitulo() {
        return claseTitulo;
    }

    public void setClaseTitulo(String claseTitulo) {
        this.claseTitulo = claseTitulo;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getCodigoCatastral() {
        return codigoCatastral;
    }

    public void setCodigoCatastral(String codigoCatastral) {
        this.codigoCatastral = codigoCatastral;
    }

    public String getPresidente() {
        return presidente;
    }

    public void setPresidente(String presidente) {
        this.presidente = presidente;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getResolucionTitulacion() {
        return resolucionTitulacion;
    }

    public void setResolucionTitulacion(String resolucionTitulacion) {
        this.resolucionTitulacion = resolucionTitulacion;
    }

    public Date getFechaResolucionTitulacion() {
        return fechaResolucionTitulacion;
    }

    public void setFechaResolucionTitulacion(Date fechaResolucionTitulacion) {
        this.fechaResolucionTitulacion = fechaResolucionTitulacion;
    }

    public List<BeneficiarioBean> getBeneficiariosList() {
        return beneficiariosList;
    }

    public void setBeneficiariosList(List<BeneficiarioBean> beneficiariosList) {
        this.beneficiariosList = beneficiariosList;
    }

    public Boolean getConexion() {
        return conexion;
    }

    public void setConexion(Boolean conexion) {
        this.conexion = conexion;
    }

    public Boolean getDatoEncontrado() {
        return datoEncontrado;
    }

    public void setDatoEncontrado(Boolean datoEncontrado) {
        this.datoEncontrado = datoEncontrado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "RespuestaInraBean{" + "numeroTitulo=" + numeroTitulo + ", fechaTitulo=" + fechaTitulo + ", nombrePredio=" + nombrePredio + ", superficie=" + superficie + ", distribucion=" + distribucion + ", claseTitulo=" + claseTitulo + ", calificacion=" + calificacion + ", clasificacion=" + clasificacion + ", departamento=" + departamento + ", provincia=" + provincia + ", municipio=" + municipio + ", canton=" + canton + ", codigoCatastral=" + codigoCatastral + ", presidente=" + presidente + ", director=" + director + ", resolucionTitulacion=" + resolucionTitulacion + ", fechaResolucionTitulacion=" + fechaResolucionTitulacion + ", beneficiariosList=" + beneficiariosList + ", conexion=" + conexion + ", datoEncontrado=" + datoEncontrado + ", mensaje=" + mensaje + '}';
    }

}
