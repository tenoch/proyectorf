package bo.gob.uif.webclients.fundempresa.beans;

import java.io.Serializable;

/**
 * Created by Vladimir.Callisaya on 02/09/2016.
 */
public class MatriculaDatosSucBean implements Serializable {

    private int idSuc;
    private String sucursal;
    private String departamento;
    private String municipio;
    private String provincia;
    private String direccion;
    private String zona;
    private String telefono;
    private String idClase;
    private String numId;
    private String representante;

    public int getIdSuc() {
        return idSuc;
    }

    public void setIdSuc(int idSuc) {
        this.idSuc = idSuc;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getIdClase() {
        return idClase;
    }

    public void setIdClase(String idClase) {
        this.idClase = idClase;
    }

    public String getNumId() {
        return numId;
    }

    public void setNumId(String numId) {
        this.numId = numId;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    @Override
    public String toString() {
        return "MatriculaDatosSucBean{"
                + "idSuc=" + idSuc
                + ", sucursal='" + sucursal + '\''
                + ", departamento='" + departamento + '\''
                + ", municipio='" + municipio + '\''
                + ", provincia='" + provincia + '\''
                + ", direccion='" + direccion + '\''
                + ", zona='" + zona + '\''
                + ", telefono='" + telefono + '\''
                + ", idClase='" + idClase + '\''
                + ", numId='" + numId + '\''
                + ", representante='" + representante + '\''
                + '}';
    }
}
