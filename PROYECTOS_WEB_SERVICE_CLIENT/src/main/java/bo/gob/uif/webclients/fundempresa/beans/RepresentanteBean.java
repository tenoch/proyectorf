package bo.gob.uif.webclients.fundempresa.beans;

import java.io.Serializable;

/**
 * Created by Vladimir.Callisaya on 09/09/2016.
 */
public class RepresentanteBean implements Serializable {

    private String ctrResult;
    private String idMatricula;
    private String nombreVinculo;
    private String numId;
    private String idClase;
    private String fecRegistro;
    private String numDoc;
    private String fecDocumento;
    private String noticiaDocumento;
    private String tipoVinculo;
    private String idLibro;
    private String numReg;

    public String getCtrResult() {
        return ctrResult;
    }

    public void setCtrResult(String ctrResult) {
        this.ctrResult = ctrResult;
    }

    public String getIdMatricula() {
        return idMatricula;
    }

    public void setIdMatricula(String idMatricula) {
        this.idMatricula = idMatricula;
    }

    public String getNombreVinculo() {
        return nombreVinculo;
    }

    public void setNombreVinculo(String nombreVinculo) {
        this.nombreVinculo = nombreVinculo;
    }

    public String getNumId() {
        return numId;
    }

    public void setNumId(String numId) {
        this.numId = numId;
    }

    public String getIdClase() {
        return idClase;
    }

    public void setIdClase(String idClase) {
        this.idClase = idClase;
    }

    public String getFecRegistro() {
        return fecRegistro;
    }

    public void setFecRegistro(String fecRegistro) {
        this.fecRegistro = fecRegistro;
    }

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public String getFecDocumento() {
        return fecDocumento;
    }

    public void setFecDocumento(String fecDocumento) {
        this.fecDocumento = fecDocumento;
    }

    public String getNoticiaDocumento() {
        return noticiaDocumento;
    }

    public void setNoticiaDocumento(String noticiaDocumento) {
        this.noticiaDocumento = noticiaDocumento;
    }

    public String getTipoVinculo() {
        return tipoVinculo;
    }

    public void setTipoVinculo(String tipoVinculo) {
        this.tipoVinculo = tipoVinculo;
    }

    public String getIdLibro() {
        return idLibro;
    }

    public void setIdLibro(String idLibro) {
        this.idLibro = idLibro;
    }

    public String getNumReg() {
        return numReg;
    }

    public void setNumReg(String numReg) {
        this.numReg = numReg;
    }

    @Override
    public String toString() {
        return "RepresentanteBean{"
                + "ctrResult='" + ctrResult + '\''
                + ", idMatricula='" + idMatricula + '\''
                + ", nombreVinculo='" + nombreVinculo + '\''
                + ", numId='" + numId + '\''
                + ", idClase='" + idClase + '\''
                + ", fecRegistro='" + fecRegistro + '\''
                + ", numDoc='" + numDoc + '\''
                + ", fecDocumento='" + fecDocumento + '\''
                + ", noticiaDocumento='" + noticiaDocumento + '\''
                + ", tipoVinculo='" + tipoVinculo + '\''
                + ", idLibro='" + idLibro + '\''
                + ", numReg='" + numReg + '\''
                + '}';
    }
}
