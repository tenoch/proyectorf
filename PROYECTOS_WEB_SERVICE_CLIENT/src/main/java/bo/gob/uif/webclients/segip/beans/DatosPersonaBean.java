package bo.gob.uif.webclients.segip.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Vladimir.Callisaya on 12/05/2017.
 */
public class DatosPersonaBean implements Serializable {

    private String complementoVisible;
    private String numeroDocumento;
    private String complemento;
    private String nombres;
    private String primerApellido;
    private String segundoApellido;
    private Date fechaNacimiento;

    public String getComplementoVisible() {
        return complementoVisible;
    }

    public void setComplementoVisible(String complementoVisible) {
        this.complementoVisible = complementoVisible;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
}
