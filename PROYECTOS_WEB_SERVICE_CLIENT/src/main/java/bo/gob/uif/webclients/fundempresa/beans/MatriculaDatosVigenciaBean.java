package bo.gob.uif.webclients.fundempresa.beans;

import java.io.Serializable;

/**
 * Created by Vladimir.Callisaya on 09/09/2016.
 */
public class MatriculaDatosVigenciaBean implements Serializable {

    private String ctrResult;
    private String idMatricula;
    private String razonSocial;
    private String fechaInscripcion;
    private String fechaUltRenovacion;
    private String UltGestionRenovada;
    private String vigenciaMatricula;
    private String fecVigenciaMatricula;
    private String nit;
    private String ctrEstado;

    public String getCtrResult() {
        return ctrResult;
    }

    public void setCtrResult(String ctrResult) {
        this.ctrResult = ctrResult;
    }

    public String getIdMatricula() {
        return idMatricula;
    }

    public void setIdMatricula(String idMatricula) {
        this.idMatricula = idMatricula;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getFechaInscripcion() {
        return fechaInscripcion;
    }

    public void setFechaInscripcion(String fechaInscripcion) {
        this.fechaInscripcion = fechaInscripcion;
    }

    public String getFechaUltRenovacion() {
        return fechaUltRenovacion;
    }

    public void setFechaUltRenovacion(String fechaUltRenovacion) {
        this.fechaUltRenovacion = fechaUltRenovacion;
    }

    public String getUltGestionRenovada() {
        return UltGestionRenovada;
    }

    public void setUltGestionRenovada(String ultGestionRenovada) {
        UltGestionRenovada = ultGestionRenovada;
    }

    public String getVigenciaMatricula() {
        return vigenciaMatricula;
    }

    public void setVigenciaMatricula(String vigenciaMatricula) {
        this.vigenciaMatricula = vigenciaMatricula;
    }

    public String getFecVigenciaMatricula() {
        return fecVigenciaMatricula;
    }

    public void setFecVigenciaMatricula(String fecVigenciaMatricula) {
        this.fecVigenciaMatricula = fecVigenciaMatricula;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getCtrEstado() {
        return ctrEstado;
    }

    public void setCtrEstado(String ctrEstado) {
        this.ctrEstado = ctrEstado;
    }

    @Override
    public String toString() {
        return "MatriculaDatosVigenciaBean{"
                + "ctrResult='" + ctrResult + '\''
                + ", idMatricula='" + idMatricula + '\''
                + ", razonSocial='" + razonSocial + '\''
                + ", fechaInscripcion='" + fechaInscripcion + '\''
                + ", fechaUltRenovacion='" + fechaUltRenovacion + '\''
                + ", UltGestionRenovada='" + UltGestionRenovada + '\''
                + ", vigenciaMatricula='" + vigenciaMatricula + '\''
                + ", fecVigenciaMatricula='" + fecVigenciaMatricula + '\''
                + ", nit='" + nit + '\''
                + ", ctrEstado='" + ctrEstado + '\''
                + '}';
    }
}
