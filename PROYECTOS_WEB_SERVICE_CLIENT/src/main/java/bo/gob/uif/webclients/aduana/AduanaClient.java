package bo.gob.uif.webclients.aduana;

import bo.gob.uif.webclients.aduana.beans.*;
import bo.gob.uif.webclients.aduana.util.Util;
import bo.gob.uif.webclients.cfg.MainWebServicesConfig;
import bo.gob.uif.webclients.cfg.WebServiceConfig;
import bo.gob.uif.webclients.common.SoapHttpRequest;
import bo.gob.uif.webclients.common.StringTokenizer;
//import bo.gob.uif.webclients.exceptions.WebServiceInternalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vladimir.Callisaya on 16/06/2017.
 *
 * Modificado: Susana Escobar Paz Fecha: 17/05/2018
 */
public class AduanaClient {

    public static WebServiceConfig wsConfig = MainWebServicesConfig.INSTANCE.getWebServiceConfig("ADUANAConsultaSerWeb");
    public static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private static Logger log;
    private static StringTokenizer st = new StringTokenizer();

    public static String testAnb(TestBean test) throws Exception {
        Element element;
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("AduanaClient.consultaTestAnb");
        String resultado = "";

        /*log.debug("Comenzando el metodo TestBean de la Aduana");
         PrivateKey keyPrivate = Util.generarPrivateKey(wsConfig.getPathKeyStorePriv() + wsConfig.getKeyStorePriv());

         log.debug("Cifrando el mensaje");
         String mensajeCifrado = Util.cifrarMensajeTest(test, keyPrivate);
         System.out.println("msg: cifrado: " + mensajeCifrado);
         String mensajeCifrado2 = "f/1FVmUqQvanO9wwZF2L3tJCoE4GMDh57utW8y1Mwr/bTTwv6YG+s5Vyr60c8T9nVHMzZfrWjKLFFWC8EdTCaJGLo+O4HFwMTyN+btFyqsZkt6w4UtAASPYFF6xLbWXv/a1qabGXI5ocZywMQj+Yk4CbJzF+IXJ2W16B00r5dKehj5bXcy3iHWQjhqG3oa5+TmujVFce3SPrItGADFr/Hh28TteHYqngw7I1iPEROY+7M0MQxILbTkxxi/jqzuEyINKBDlcLINEMLARxOKOrAqFB1btTNYTjhjJVxIb210SUB6H2Qny9mFv0GUFPki9j1hRhUOqOfKURDHg9dtXpmA==";


         log.debug("Armando la consulta");
         StringBuilder soapXml = new StringBuilder();
         soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:con=\"http://consulta/\">");
         soapXml.append("<soapenv:Header/>");
         soapXml.append("<soapenv:Body>");
         soapXml.append("<con:dummy>");
         soapXml.append("<identificador>").append(wsConfig.getParameterExtra("identificador")).append("</identificador>");
         soapXml.append("<cadenaXML>").append(mensajeCifrado2).append("</cadenaXML>");
         soapXml.append("</con:dummy>");
         soapXml.append("</soapenv:Body>");
         soapXml.append("</soapenv:Envelope>");

         String requestLog = soapXml.toString();
         StringBuilder req = st.tokenizerString((new StringBuilder()).append(requestLog));
         log.debug("Request: " + req);

         log.debug("Comenzamos el consumo");

         Document response = SoapHttpRequest.sendRequestPOST("http://deslogic01.aduana.gob.bo:7001/wsTestAnb/consulta",
         "text/xml;charset=UTF-8",
         "",
         soapXml.toString());

         StringBuilder res = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
         log.debug("Response : "+res);

         Namespace ns = Namespace.getNamespace("http://consulta/");

         element = response.getRootElement();

         element = element.getChild("Body", element.getNamespace());
         element = element.getChild("dummyResponse", ns);*/
        PublicKey keyPublic = Util.generarPublicKey(wsConfig.getPathKeyStorePublic() + wsConfig.getKeyStorePublic());
        System.out.println("llave" + wsConfig.getKeyStorePublic());
        System.out.println("path" + wsConfig.getPathKeyStorePublic());

        String ans = "";

        System.out.println("resp: " + Util.descifrarMensajeTest(ans, keyPublic));
        /*List<Element> xmlSigned = element.getChildren("return");
         for(int i = 0; i < xmlSigned.size(); i++){
         String dato = xmlSigned.get(i).getValue();
         resultado = resultado + Util.descifrarMensajeTest(dato, keyPublic);
         }

         if(resultado.equals("")){
         throw new Exception("Mensaje de respuesta vacio");
         }*/

        return resultado;
    }

    public static RespuestaAduanaImportacionBean cImportacionesAnb(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, String numeroDocumento, String fechaInicio, String fechaFin) throws Exception {
        PersonaConsultaBean datosPersona = new PersonaConsultaBean();
        datosPersona.setPrimerNombre(primerNombre);
        datosPersona.setSegundoNombre(segundoNombre);
        datosPersona.setPrimerApellido(primerApellido);
        datosPersona.setSegundoApellido(segundoApellido);
        datosPersona.setNumeroDocumento(numeroDocumento);
        datosPersona.setFechaInicio(fechaInicio);
        datosPersona.setFechaFin(fechaFin);

        RespuestaAduanaImportacionBean respuestaImportaciones = new RespuestaAduanaImportacionBean();
        try {
            Element element;
            MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
            log = LogManager.getLogger("AduanaClient.consultaCieAnb");

            log.debug("Comenzando el metodo CImportaciones de la Aduana");
            log.debug("path llave: " + wsConfig.getPathKeyStorePriv());
            log.debug("llave: " + wsConfig.getKeyStorePriv());
            PrivateKey keyPrivate = Util.generarPrivateKey(wsConfig.getPathKeyStorePriv() + wsConfig.getKeyStorePriv());

            log.debug("Cifrando el mensaje");
            ArrayList<String> listaCifrada = Util.cifrarMensajeCie(datosPersona, keyPrivate);

            log.debug("Armando la consulta");
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsc=\"http://wscie/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<wsc:cImportaciones>");
            for (String txtCifrado : listaCifrada) {
                soapXml.append("<arg0>").append(txtCifrado).append("</arg0>");
            }
            soapXml.append("<arg1>").append(wsConfig.getParameterExtra("identificador")).append("</arg1>");
            soapXml.append("</wsc:cImportaciones>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            String requestLog = soapXml.toString();
            StringBuilder req = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request: " + req);

            log.debug("Comenzamos el consumo");
            log.debug("consummiendo de: " + wsConfig.getLocation());
            Document response = new Document();

            response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),
                    "text/xml;charset=UTF-8",
                    "",
                    soapXml.toString());

            StringBuilder res = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("Response : " + res);

            element = response.getRootElement();
            element = element.getChild("Body", element.getNamespace());

            if (res.indexOf("<S:Fault xmlns:ns4=\"http://www.w3.org/2003/05/soap-envelope\">") != -1) {
                log.debug("Se obtuvo un Fault");
                element = element.getChild("Fault", element.getNamespace());
                respuestaImportaciones.setRespuestaDatos(false);
                respuestaImportaciones.setFaultCode(element.getChildText("faultcode"));
                respuestaImportaciones.setFaultString(element.getChildText("faultstring"));

            } else {
                log.debug("Se obtenieron datos");
                List<ImportacionBean> listaImportaciones = new ArrayList<ImportacionBean>();

                log.debug("Descifrando mesnajes");
                PublicKey publicKey = Util.generarPublicKey(wsConfig.getPathKeyStorePublic() + wsConfig.getKeyStorePublic());

                Namespace ns = Namespace.getNamespace("http://wscie/");

                element = element.getChild("cImportacionesResponse", ns);
                List<Element> listElementImportaciones = element.getChildren("return");

                StringBuilder respuestaXmlArmada = new StringBuilder();
                log.debug("Numero elementos: " + listElementImportaciones.size());
                for (Element elem : listElementImportaciones) {
                    log.debug("return: " + elem.getText());
                    respuestaXmlArmada.append(Util.descifrarMensajeCie(elem.getText(), publicKey));
                }
                log.debug("respuesta descifrada: " + respuestaXmlArmada.toString());

                SAXBuilder sax = new SAXBuilder();
                Document docXml = sax.build(new java.io.ByteArrayInputStream(respuestaXmlArmada.toString().getBytes("UTF-8")));

                element = docXml.getRootElement();
                List<Element> listElementItems = element.getChildren("item");
                log.debug("Nro items: " + listElementItems.size());
                ImportacionBean imp;

                for (Element el : listElementItems) {
                    imp = new ImportacionBean();
                    imp.setNombreOperador(!"".equals(el.getChildText("nombreOperador")) ? el.getChildText("nombreOperador") : null);
                    imp.setDocumentoOperador(!"".equals(el.getChildText("documentoOperador")) ? el.getChildText("documentoOperador") : null);
                    imp.setDeclaracion(!"".equals(el.getChildText("declaracion")) ? el.getChildText("declaracion") : null);
                    imp.setNumeroItem(!"".equals(el.getChildText("numeroItem")) ? el.getChildText("numeroItem") : null);
                    imp.setFechaRegistro(!" ".equals(el.getChildText("fechaRegistro")) ? sdf.parse(el.getChildText("fechaRegistro")) : null);
                    imp.setSubpartida(!"".equals(el.getChildText("subpartida")) ? el.getChildText("subpartida") : null);
                    imp.setDesMercancia(!"".equals(el.getChildText("desMercancia")) ? el.getChildText("desMercancia") : null);
                    imp.setValorFob(!"".equals(el.getChildText("valorFob")) ? el.getChildText("valorFob") : null);
                    imp.setValorCif(!"".equals(el.getChildText("valorCif")) ? el.getChildText("valorCif") : null);
                    imp.setImpuestos(!"".equals(el.getChildText("impuestos")) ? el.getChildText("impuestos") : null);
                    imp.setPatron(!"".equals(el.getChildText("patron")) ? el.getChildText("patron") : null);
                    imp.setRegimen(!"".equals(el.getChildText("regimen")) ? el.getChildText("regimen") : null);
                    imp.setFechaPago(!" ".equals(el.getChildText("fechaPago")) ? sdf.parse(el.getChildText("fechaPago")) : null);
                    listaImportaciones.add(imp);
                }

                respuestaImportaciones.setRespuestaDatos(true);
                respuestaImportaciones.setFaultCode("N/N");
                respuestaImportaciones.setFaultString("N/N");
                respuestaImportaciones.setListaImportaciones(listaImportaciones);
            }

        } catch (Exception ex) {
            log.debug("hubo un problema con el consumo");
            log.catching(ex);
            respuestaImportaciones.setFaultCode("Mensaje");
            respuestaImportaciones.setFaultString("No hay conexión con el servicio");
        }

        return respuestaImportaciones;
    }

    public static RespuestaAduanaExportacionBean cExportacionesAnb(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, String numeroDocumento, String fechaInicio, String fechaFin) throws Exception {
        PersonaConsultaBean datosPersona = new PersonaConsultaBean();
        datosPersona.setPrimerNombre(primerNombre);
        datosPersona.setSegundoNombre(segundoNombre);
        datosPersona.setPrimerApellido(primerApellido);
        datosPersona.setSegundoApellido(segundoApellido);
        datosPersona.setNumeroDocumento(numeroDocumento);
        datosPersona.setFechaInicio(fechaInicio);
        datosPersona.setFechaFin(fechaFin);

        //List<ExportacionBean> listaExportaciones = new ArrayList<ExportacionBean>();
        RespuestaAduanaExportacionBean respuestaExportaciones = new RespuestaAduanaExportacionBean();
        try {
            Element element;
            MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
            log = LogManager.getLogger("AduanaClient.consultaCieAnb");

            log.debug("Comenzando el metodo CExportaciones de la Aduana");
            log.debug("path llave: " + wsConfig.getPathKeyStorePriv());
            log.debug("llave: " + wsConfig.getKeyStorePriv());
            PrivateKey keyPrivate = Util.generarPrivateKey(wsConfig.getPathKeyStorePriv() + wsConfig.getKeyStorePriv());

            log.debug("Cifrando el mensaje");
            ArrayList<String> listaCifrada = Util.cifrarMensajeCie(datosPersona, keyPrivate);

            log.debug("Armando la consulta");
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsc=\"http://wscie/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<wsc:cExportaciones>");
            for (String txtCifrado : listaCifrada) {
                soapXml.append("<arg0>").append(txtCifrado).append("</arg0>");
            }
            soapXml.append("<arg1>").append(wsConfig.getParameterExtra("identificador")).append("</arg1>");
            soapXml.append("</wsc:cExportaciones>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            String requestLog = soapXml.toString();
            StringBuilder req = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request: " + req);

            log.debug("Comenzamos el consumo");
            log.debug("consummiendo de: " + wsConfig.getLocation());
            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),
                    "text/xml;charset=UTF-8",
                    "",
                    soapXml.toString());

            StringBuilder res = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("Response : " + res);

            element = response.getRootElement();
            element = element.getChild("Body", element.getNamespace());

            if (res.indexOf("<S:Fault xmlns:ns4=\"http://www.w3.org/2003/05/soap-envelope\">") != -1) {
                log.debug("Se obtuvo un Fault");
                element = element.getChild("Fault", element.getNamespace());
                respuestaExportaciones.setRespuestaDatos(false);
                respuestaExportaciones.setFaultCode(element.getChildText("faultcode"));
                respuestaExportaciones.setFaultString(element.getChildText("faultstring"));
            } else {
                log.debug("Se obtenieron datos");
                List<ExportacionBean> listaExportaciones = new ArrayList<ExportacionBean>();
                log.debug("Descifrando mensajes");
                PublicKey publicKey = Util.generarPublicKey(wsConfig.getPathKeyStorePublic() + wsConfig.getKeyStorePublic());

                Namespace ns = Namespace.getNamespace("http://wscie/");

                element = element.getChild("cExportacionesResponse", ns);
                List<Element> listElementExportaciones = element.getChildren("return");

                StringBuilder respuestaXmlArmada = new StringBuilder();
                log.debug("Numero elementos: " + listElementExportaciones.size());
                for (Element elem : listElementExportaciones) {
                    log.debug("return: " + elem.getText());
                    respuestaXmlArmada.append(Util.descifrarMensajeCie(elem.getText(), publicKey));
                }
                log.debug("respuesta descifrada: " + respuestaXmlArmada.toString());

                SAXBuilder sax = new SAXBuilder();
                Document docXml = sax.build(new java.io.ByteArrayInputStream(respuestaXmlArmada.toString().getBytes("UTF-8")));

                element = docXml.getRootElement();
                List<Element> listElementItems = element.getChildren("item");
                log.debug("Nro items: " + listElementItems.size());
                ExportacionBean exp;

                for (Element el : listElementItems) {
                    exp = new ExportacionBean();
                    exp.setNombreOperador(!"".equals(el.getChildText("nombreOperador")) ? el.getChildText("nombreOperador") : null);
                    exp.setDocumentoOperador(!"".equals(el.getChildText("documentoOperador")) ? el.getChildText("documentoOperador") : null);
                    exp.setDeclaracion(!"".equals(el.getChildText("declaracion")) ? el.getChildText("declaracion") : null);
                    exp.setNumeroItem(!"".equals(el.getChildText("numeroItem")) ? el.getChildText("numeroItem") : null);
                    exp.setFechaRegistro(!" ".equals(el.getChildText("fechaRegistro")) ? sdf.parse(el.getChildText("fechaRegistro")) : null);
                    exp.setSubpartida(!"".equals(el.getChildText("subpartida")) ? el.getChildText("subpartida") : null);
                    exp.setDesMercancia(!"".equals(el.getChildText("desMercancia")) ? el.getChildText("desMercancia") : null);
                    exp.setValorFob(!"".equals(el.getChildText("valorFob")) ? el.getChildText("valorFob") : null);
                    exp.setPatron(!"".equals(el.getChildText("patron")) ? el.getChildText("patron") : null);
                    exp.setRegimen(!"".equals(el.getChildText("regimen")) ? el.getChildText("regimen") : null);
                    listaExportaciones.add(exp);
                }

                respuestaExportaciones.setRespuestaDatos(true);
                respuestaExportaciones.setFaultCode("N/N");
                respuestaExportaciones.setFaultString("N/N");
                respuestaExportaciones.setListaExportaciones(listaExportaciones);
            }
        } catch (Exception ex) {
            respuestaExportaciones.setFaultCode("Mensaje");
            respuestaExportaciones.setFaultString("No hay conexión con el servicio");
        }

        return respuestaExportaciones;
    }
}
