package bo.gob.uif.webclients.impuestos.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Vladimir.Callisaya on 11/08/2016.
 */
public class ActividadEconomicaBean implements Serializable {

    private String descripcionActividad;
    private String descripcionTipoActividad;
    private Date fechaDesde;
    private Date fechaHasta;

    public String getDescripcionActividad() {
        return descripcionActividad;
    }

    public void setDescripcionActividad(String descripcionActividad) {
        this.descripcionActividad = descripcionActividad;
    }

    public String getDescripcionTipoActividad() {
        return descripcionTipoActividad;
    }

    public void setDescripcionTipoActividad(String descripcionTipoActividad) {
        this.descripcionTipoActividad = descripcionTipoActividad;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }
}
