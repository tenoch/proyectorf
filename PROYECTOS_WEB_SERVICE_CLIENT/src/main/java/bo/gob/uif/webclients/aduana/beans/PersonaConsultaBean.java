package bo.gob.uif.webclients.aduana.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Created by Vladimir.Callisaya on 09/08/2017.
 */
@XmlRootElement(name = "parametros")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonaConsultaBean implements Serializable {

    @XmlElement(name = "primerNombre")
    private String primerNombre;
    @XmlElement(name = "segundoNombre")
    private String segundoNombre;
    @XmlElement(name = "primerApellido")
    private String primerApellido;
    @XmlElement(name = "segundoApellido")
    private String segundoApellido;
    @XmlElement(name = "numeroDocumento")
    private String numeroDocumento;
    @XmlElement(name = "fechaInicio")
    private String fechaInicio;
    @XmlElement(name = "fechaFin")
    private String fechaFin;

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }
}
