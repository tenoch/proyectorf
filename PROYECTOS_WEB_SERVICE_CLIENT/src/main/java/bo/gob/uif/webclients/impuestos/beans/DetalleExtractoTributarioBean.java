/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.gob.uif.webclients.impuestos.beans;

/**
 * @author Alan.Portugal
 *
 * Modificado: Susana Escobar Paz Fecha: 17/05/2018
 */
public class DetalleExtractoTributarioBean {

    private int anio;
    private int mes;
    private String descripcionFormulario;
    private String fechaPago;
    private long numeroOrden;
    private float importeEnEfectivo;
    private float importeEnValores;
    private float importeOtros;
    private long numeroOrdenRectificada;
    private String descripcionBanco;
    private Boolean conexion = true;

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public String getDescripcionFormulario() {
        return descripcionFormulario;
    }

    public void setDescripcionFormulario(String descripcionFormulario) {
        this.descripcionFormulario = descripcionFormulario;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public long getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(long numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public float getImporteEnEfectivo() {
        return importeEnEfectivo;
    }

    public void setImporteEnEfectivo(float importeEnEfectivo) {
        this.importeEnEfectivo = importeEnEfectivo;
    }

    public float getImporteEnValores() {
        return importeEnValores;
    }

    public void setImporteEnValores(float importeEnValores) {
        this.importeEnValores = importeEnValores;
    }

    public float getImporteOtros() {
        return importeOtros;
    }

    public void setImporteOtros(float importeOtros) {
        this.importeOtros = importeOtros;
    }

    public long getNumeroOrdenRectificada() {
        return numeroOrdenRectificada;
    }

    public void setNumeroOrdenRectificada(long numeroOrdenRectificada) {
        this.numeroOrdenRectificada = numeroOrdenRectificada;
    }

    public String getDescripcionBanco() {
        return descripcionBanco;
    }

    public void setDescripcionBanco(String descripcionBanco) {
        this.descripcionBanco = descripcionBanco;
    }

    public Boolean getConexion() {
        return conexion;
    }

    public void setConexion(Boolean conexion) {
        this.conexion = conexion;
    }

}
