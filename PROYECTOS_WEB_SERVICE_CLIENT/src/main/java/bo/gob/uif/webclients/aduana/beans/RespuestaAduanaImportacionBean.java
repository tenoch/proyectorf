package bo.gob.uif.webclients.aduana.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vladimir.Callisaya on 28/09/2017.
 */
public class RespuestaAduanaImportacionBean implements Serializable {

    private boolean respuestaDatos;
    private String faultCode;
    private String faultString;
    private List<ImportacionBean> listaImportaciones;

    public boolean isRespuestaDatos() {
        return respuestaDatos;
    }

    public void setRespuestaDatos(boolean respuestaDatos) {
        this.respuestaDatos = respuestaDatos;
    }

    public String getFaultCode() {
        return faultCode;
    }

    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }

    public String getFaultString() {
        return faultString;
    }

    public void setFaultString(String faultString) {
        this.faultString = faultString;
    }

    public List<ImportacionBean> getListaImportaciones() {
        return listaImportaciones;
    }

    public void setListaImportaciones(List<ImportacionBean> listaImportaciones) {
        this.listaImportaciones = listaImportaciones;
    }
}
