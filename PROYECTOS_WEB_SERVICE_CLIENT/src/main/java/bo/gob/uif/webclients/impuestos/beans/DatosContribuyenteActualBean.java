package bo.gob.uif.webclients.impuestos.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Vladimir.Callisaya on 11/08/2016.
 *
 * Modificado: Susana Escobar Paz Fecha: 17/05/2018
 */
public class DatosContribuyenteActualBean implements Serializable {

    private String cedula;
    private long nit;
    private String descripcionJurisdiccion;
    private String direccionFiscal;
    private List<ActividadEconomicaBean> actividadEconomicaBean;
    private Date fechaInscripcion;
    private Date fechaUltimoEstado;
    private String descripcionEstado;
    private String nombreRazonSocial;
    private List<ObligacionTributariaBean> obligacionTributariaBean;
    private String descripcionRegimen;
    private List<PersonaContribuyenteBean> personaContribuyenteBean;
    private Boolean conexion = true;

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public long getNit() {
        return nit;
    }

    public void setNit(long nit) {
        this.nit = nit;
    }

    public String getDescripcionJurisdiccion() {
        return descripcionJurisdiccion;
    }

    public void setDescripcionJurisdiccion(String descripcionJurisdiccion) {
        this.descripcionJurisdiccion = descripcionJurisdiccion;
    }

    public String getDireccionFiscal() {
        return direccionFiscal;
    }

    public void setDireccionFiscal(String direccionFiscal) {
        this.direccionFiscal = direccionFiscal;
    }

    public List<ActividadEconomicaBean> getActividadEconomicaBean() {
        return actividadEconomicaBean;
    }

    public void setActividadEconomicaBean(List<ActividadEconomicaBean> actividadEconomicaBean) {
        this.actividadEconomicaBean = actividadEconomicaBean;
    }

    public Date getFechaInscripcion() {
        return fechaInscripcion;
    }

    public void setFechaInscripcion(Date fechaInscripcion) {
        this.fechaInscripcion = fechaInscripcion;
    }

    public Date getFechaUltimoEstado() {
        return fechaUltimoEstado;
    }

    public void setFechaUltimoEstado(Date fechaUltimoEstado) {
        this.fechaUltimoEstado = fechaUltimoEstado;
    }

    public String getDescripcionEstado() {
        return descripcionEstado;
    }

    public void setDescripcionEstado(String descripcionEstado) {
        this.descripcionEstado = descripcionEstado;
    }

    public String getNombreRazonSocial() {
        return nombreRazonSocial;
    }

    public void setNombreRazonSocial(String nombreRazonSocial) {
        this.nombreRazonSocial = nombreRazonSocial;
    }

    public List<ObligacionTributariaBean> getObligacionTributariaBean() {
        return obligacionTributariaBean;
    }

    public void setObligacionTributariaBean(List<ObligacionTributariaBean> obligacionTributariaBean) {
        this.obligacionTributariaBean = obligacionTributariaBean;
    }

    public String getDescripcionRegimen() {
        return descripcionRegimen;
    }

    public void setDescripcionRegimen(String descripcionRegimen) {
        this.descripcionRegimen = descripcionRegimen;
    }

    public List<PersonaContribuyenteBean> getPersonaContribuyenteBean() {
        return personaContribuyenteBean;
    }

    public void setPersonaContribuyenteBean(List<PersonaContribuyenteBean> personaContribuyenteBean) {
        this.personaContribuyenteBean = personaContribuyenteBean;
    }

    public Boolean getConexion() {
        return conexion;
    }

    public void setConexion(Boolean conexion) {
        this.conexion = conexion;
    }

}
