package bo.gob.uif.webclients.aduana.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Vladimir.Callisaya on 28/08/2017.
 */
public class ExportacionBean implements Serializable {

    private String nombreOperador;
    private String documentoOperador;
    private String declaracion;
    private String numeroItem;
    private Date fechaRegistro;
    private String subpartida;
    private String desMercancia;
    private String valorFob;
    private String patron;
    private String regimen;

    public String getNombreOperador() {
        return nombreOperador;
    }

    public void setNombreOperador(String nombreOperador) {
        this.nombreOperador = nombreOperador;
    }

    public String getDocumentoOperador() {
        return documentoOperador;
    }

    public void setDocumentoOperador(String documentoOperador) {
        this.documentoOperador = documentoOperador;
    }

    public String getDeclaracion() {
        return declaracion;
    }

    public void setDeclaracion(String declaracion) {
        this.declaracion = declaracion;
    }

    public String getNumeroItem() {
        return numeroItem;
    }

    public void setNumeroItem(String numeroItem) {
        this.numeroItem = numeroItem;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getSubpartida() {
        return subpartida;
    }

    public void setSubpartida(String subpartida) {
        this.subpartida = subpartida;
    }

    public String getDesMercancia() {
        return desMercancia;
    }

    public void setDesMercancia(String desMercancia) {
        this.desMercancia = desMercancia;
    }

    public String getValorFob() {
        return valorFob;
    }

    public void setValorFob(String valorFob) {
        this.valorFob = valorFob;
    }

    public String getPatron() {
        return patron;
    }

    public void setPatron(String patron) {
        this.patron = patron;
    }

    public String getRegimen() {
        return regimen;
    }

    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }
}
