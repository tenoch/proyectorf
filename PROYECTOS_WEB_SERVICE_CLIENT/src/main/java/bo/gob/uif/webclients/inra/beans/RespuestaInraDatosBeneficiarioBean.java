/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.gob.uif.webclients.inra.beans;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Susana.Escobar
 */
public class RespuestaInraDatosBeneficiarioBean implements Serializable {

    private List<RespuestaInraBean> listaRespuestaDatosBeneficiario;
    private Boolean conexion = true;
    private Boolean datoEncontrado;
    private String mensaje;

    public List<RespuestaInraBean> getListaRespuestaDatosBeneficiario() {
        return listaRespuestaDatosBeneficiario;
    }

    public void setListaRespuestaDatosBeneficiario(List<RespuestaInraBean> listaRespuestaDatosBeneficiario) {
        this.listaRespuestaDatosBeneficiario = listaRespuestaDatosBeneficiario;
    }

    public Boolean getConexion() {
        return conexion;
    }

    public void setConexion(Boolean conexion) {
        this.conexion = conexion;
    }

    public Boolean getDatoEncontrado() {
        return datoEncontrado;
    }

    public void setDatoEncontrado(Boolean datoEncontrado) {
        this.datoEncontrado = datoEncontrado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

}
