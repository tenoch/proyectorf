package bo.gob.uif.webclients.fundempresa.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vladimir.Callisaya on 02/09/2016.
 */
public class InfoMatriculaBean implements Serializable {

    private String ctrResult;
    private String idMatricula;
    private String razonSocial;
    private String tipoSocietario;
    private String fechaInscripcion;
    private String fechaUltRenovacion;
    private String ultGestionRenovada;
    private String nit;
    private String departamento;
    private String provincia;
    private String municipio;
    private String calleAv;
    private String entreCalles;
    private String nro;
    private String uv;
    private String mza;
    private String edificio;
    private String piso;
    private String nroOficina;
    private String zona;
    private String correoElectronico;
    private String actividad;
    private String claseCIIU;
    private String ctrEstado;
    private List<MatriculaDatosSucBean> matriculaDatosSucList;

    public String getCtrResult() {
        return ctrResult;
    }

    public void setCtrResult(String ctrResult) {
        this.ctrResult = ctrResult;
    }

    public String getIdMatricula() {
        return idMatricula;
    }

    public void setIdMatricula(String idMatricula) {
        this.idMatricula = idMatricula;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getTipoSocietario() {
        return tipoSocietario;
    }

    public void setTipoSocietario(String tipoSocietario) {
        this.tipoSocietario = tipoSocietario;
    }

    public String getFechaInscripcion() {
        return fechaInscripcion;
    }

    public void setFechaInscripcion(String fechaInscripcion) {
        this.fechaInscripcion = fechaInscripcion;
    }

    public String getFechaUltRenovacion() {
        return fechaUltRenovacion;
    }

    public void setFechaUltRenovacion(String fechaUltRenovacion) {
        this.fechaUltRenovacion = fechaUltRenovacion;
    }

    public String getUltGestionRenovada() {
        return ultGestionRenovada;
    }

    public void setUltGestionRenovada(String ultGestionRenovada) {
        this.ultGestionRenovada = ultGestionRenovada;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getCalleAv() {
        return calleAv;
    }

    public void setCalleAv(String calleAv) {
        this.calleAv = calleAv;
    }

    public String getEntreCalles() {
        return entreCalles;
    }

    public void setEntreCalles(String entreCalles) {
        this.entreCalles = entreCalles;
    }

    public String getNro() {
        return nro;
    }

    public void setNro(String nro) {
        this.nro = nro;
    }

    public String getUv() {
        return uv;
    }

    public void setUv(String uv) {
        this.uv = uv;
    }

    public String getMza() {
        return mza;
    }

    public void setMza(String mza) {
        this.mza = mza;
    }

    public String getEdificio() {
        return edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getNroOficina() {
        return nroOficina;
    }

    public void setNroOficina(String nroOficina) {
        this.nroOficina = nroOficina;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getClaseCIIU() {
        return claseCIIU;
    }

    public void setClaseCIIU(String claseCIIU) {
        this.claseCIIU = claseCIIU;
    }

    public String getCtrEstado() {
        return ctrEstado;
    }

    public void setCtrEstado(String ctrEstado) {
        this.ctrEstado = ctrEstado;
    }

    public List<MatriculaDatosSucBean> getMatriculaDatosSucList() {
        return matriculaDatosSucList;
    }

    public void setMatriculaDatosSucList(List<MatriculaDatosSucBean> matriculaDatosSucList) {
        this.matriculaDatosSucList = matriculaDatosSucList;
    }

    @Override
    public String toString() {
        return "InfoMatriculaBean{"
                + "ctrResult='" + ctrResult + '\''
                + ", idMatricula='" + idMatricula + '\''
                + ", razonSocial='" + razonSocial + '\''
                + ", tipoSocietario='" + tipoSocietario + '\''
                + ", fechaInscripcion='" + fechaInscripcion + '\''
                + ", fechaUltRenovacion='" + fechaUltRenovacion + '\''
                + ", ultGestionRenovada='" + ultGestionRenovada + '\''
                + ", nit='" + nit + '\''
                + ", departamento='" + departamento + '\''
                + ", provincia='" + provincia + '\''
                + ", municipio='" + municipio + '\''
                + ", calleAv='" + calleAv + '\''
                + ", entreCalles='" + entreCalles + '\''
                + ", nro='" + nro + '\''
                + ", uv='" + uv + '\''
                + ", mza='" + mza + '\''
                + ", edificio='" + edificio + '\''
                + ", piso='" + piso + '\''
                + ", nroOficina='" + nroOficina + '\''
                + ", zona='" + zona + '\''
                + ", correoElectronico='" + correoElectronico + '\''
                + ", ctrEstado='" + ctrEstado + '\''
                + ", matriculaDatosSucList=" + matriculaDatosSucList
                + '}';
    }
}
