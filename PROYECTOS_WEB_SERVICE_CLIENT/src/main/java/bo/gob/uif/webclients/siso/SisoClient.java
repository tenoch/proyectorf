package bo.gob.uif.webclients.siso;

import bo.gob.uif.webclients.cfg.MainWebServicesConfig;
import bo.gob.uif.webclients.cfg.WebServiceConfig;
import bo.gob.uif.webclients.common.SoapHttpRequest;
import bo.gob.uif.webclients.common.StringTokenizer;
import static bo.gob.uif.webclients.impuestos.ImpuestosClient.wsConfig;
import bo.gob.uif.webclients.siso.beans.ReporteRosBean;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.XMLOutputter;

import java.text.SimpleDateFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Vladimir.Callisaya on 15/09/2016.
 */
public class SisoClient {

    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    public static WebServiceConfig wsConfig = MainWebServicesConfig.INSTANCE.getWebServiceConfig("SISOConsultaSerWeb");
    private static Logger log;
    private static StringTokenizer st = new StringTokenizer();

    public static ReporteRosBean obtenerRos(String idTramite) {
        ReporteRosBean result = new ReporteRosBean();
        Element element;
        //StringTokenizer st = new StringTokenizer();
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        log = LogManager.getLogger("SisoClient.obtenerRos");
        log.info("Iniciando Metodo");

        try {
            StringBuilder soapXml = new StringBuilder();
            soapXml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws.vista.sinf.sis.uif.bo/\">");
            soapXml.append("<soapenv:Header/>");
            soapXml.append("<soapenv:Body>");
            soapXml.append("<ws:obtenerRos>");
            soapXml.append("<idTramite>").append(idTramite).append("</idTramite>");
            soapXml.append("</ws:obtenerRos>");
            soapXml.append("</soapenv:Body>");
            soapXml.append("</soapenv:Envelope>");

            log.info("Consumiendo WS");
            String requestLog = soapXml.toString();
            StringBuilder reqs = st.tokenizerString((new StringBuilder()).append(requestLog));
            log.debug("Request : " + reqs);

            Document response = SoapHttpRequest.sendRequestPOST(wsConfig.getLocation(),
                    "text/xml;charset=UTF-8",
                    "",
                    soapXml.toString());

            StringBuilder reps = st.tokenizerString((new StringBuilder()).append(new XMLOutputter().outputString(response).trim()));
            log.debug("response : " + reps);

            Namespace ns = Namespace.getNamespace("ns2", "http://ws.vista.sinf.sis.uif.bo/");
            element = response.getRootElement();

            element = element.getChild("Body", element.getNamespace());
            element = element.getChild("obtenerRosResponse", ns);
            element = element.getChild("return");

            if (element.getChildText("existe").equals("SI")) {
                result.setExiste(!"".equals(element.getChildText("existe")) ? element.getChildText("existe") : null);
                result.setFechaEnvio(!"".equals(element.getChildText("fechaEnvio")) ? sdf.parse(element.getChildText("fechaEnvio")) : null);
                result.setIdRosFormulario(!"".equals(element.getChildText("idRosFormulario")) ? Long.parseLong(element.getChildText("idRosFormulario")) : 0);
                result.setIdUsuarioSicod(!"".equals(element.getChildText("idUsuarioSicod")) ? Long.parseLong(element.getChildText("idUsuarioSicod")) : 0);
                result.setNroTramiteSicod(!"".equals(element.getChildText("nroTramiteSicod")) ? element.getChildText("nroTramiteSicod") : null);
                result.setNumero(!"".equals(element.getChildText("numero")) ? element.getChildText("numero") : null);
                result.setSujetoObligado(!"".equals(element.getChildText("sujetoObligado")) ? element.getChildText("sujetoObligado") : null);
            } else {
                result.setExiste(!"".equals(element.getChildText("existe")) ? element.getChildText("existe") : null);
            }

        } catch (Exception ex) {
            //ex.printStackTrace();
            log.catching(ex);
        }
        log.info("Finalizando Metodo");
        return result;
    }
}
