package bo.gob.uif.webclients.segip.beans;

import java.io.Serializable;

/**
 * Created by Vladimir.Callisaya on 12/05/2017.
 */
public class DatosRespuestaBean implements Serializable {

    private boolean esValido;
    private String mensaje;
    private String tipoMensaje;
    private int codigoRespuesta;
    private String codigoUnico;
    private String descripcionRespuesta;
    private DatosPersonaBean persona;
    private byte[] certificacionPdf;

    public boolean isEsValido() {
        return esValido;
    }

    public void setEsValido(boolean esValido) {
        this.esValido = esValido;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getTipoMensaje() {
        return tipoMensaje;
    }

    public void setTipoMensaje(String tipoMensaje) {
        this.tipoMensaje = tipoMensaje;
    }

    public int getCodigoRespuesta() {
        return codigoRespuesta;
    }

    public void setCodigoRespuesta(int codigoRespuesta) {
        this.codigoRespuesta = codigoRespuesta;
    }

    public String getCodigoUnico() {
        return codigoUnico;
    }

    public void setCodigoUnico(String codigoUnico) {
        this.codigoUnico = codigoUnico;
    }

    public String getDescripcionRespuesta() {
        return descripcionRespuesta;
    }

    public void setDescripcionRespuesta(String descripcionRespuesta) {
        this.descripcionRespuesta = descripcionRespuesta;
    }

    public DatosPersonaBean getPersona() {
        return persona;
    }

    public void setPersona(DatosPersonaBean persona) {
        this.persona = persona;
    }

    public byte[] getCertificacionPdf() {
        return certificacionPdf;
    }

    public void setCertificacionPdf(byte[] certificacionPdf) {
        this.certificacionPdf = certificacionPdf;
    }
}
