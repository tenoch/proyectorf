package bo.gob.uif.tests.siso;

import bo.gob.uif.webclients.siso.SisoClient;
import bo.gob.uif.webclients.siso.beans.ReporteRosBean;

/**
 * Created by Vladimir.Callisaya on 15/09/2016.
 */
public class TestSiso {

    public static void main(String[] args) {
        //metodo para verificar si un tramite es ros
        String idTramite = "39481";

        try {
            ReporteRosBean ros = SisoClient.obtenerRos(idTramite);
            System.out.println("existe: " + ros.getExiste());
            System.out.println("fechaEnvio: " + ros.getFechaEnvio());
            System.out.println("idRosFormulario: " + ros.getIdRosFormulario());
            System.out.println("nroTramiteSicod: " + ros.getNroTramiteSicod());
            System.out.println("numero: " + ros.getNumero());
            System.out.println("sujetoObligado: " + ros.getSujetoObligado());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
