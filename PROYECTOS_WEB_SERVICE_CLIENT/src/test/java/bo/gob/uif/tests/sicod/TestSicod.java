package bo.gob.uif.tests.sicod;

import bo.gob.uif.webclients.sicod.SicodClient;
import bo.gob.uif.webclients.sicod.beans.AsignacionBean;
import bo.gob.uif.webclients.sicod.beans.AsignacionTramiteRespBean;
import bo.gob.uif.webclients.sicod.beans.DocumentoBean;
import bo.gob.uif.webclients.sicod.beans.ProcesoBean;
import bo.gob.uif.webclients.sicod.beans.TramiteAsignacionBean;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vladimir.Callisaya on 12/08/2016.
 */
public class TestSicod {

    /**
     * @param args
     */
    /**
     * @param args
     */
    /**
     * @param args
     */
    public static void main(String[] args) {

        /* Datos para enviar al cliente del SICOD - Asignacion de tramite */
        /*
         * String respuesta = "";
         * 
         * String idProceso = "35262"; String idAsignacionOrigen = "137168";
         * String idUsuarioOrigen = "150";
         * 
         * List<TramiteAsignacionBean> listaDestinos = new
         * ArrayList<TramiteAsignacionBean>(); TramiteAsignacionBean destino1 =
         * new TramiteAsignacionBean(); destino1.setFojas(8);
         * destino1.setIdUsuarioDestino("134");
         * destino1.setNombredestino("Boris Cuevas"); destino1.setNumCopia("0");
         * destino1.setProveido("Para su atencion octava");
         * destino1.setTipoAsignacion("TA2");
         * destino1.setTipoAsignacionDesc("ASIGNACION");
         * 
         * listaDestinos.add(destino1);
         * 
         * TramiteAsignacionBean destino2 = new TramiteAsignacionBean();
         * destino2.setFojas(8); destino2.setIdUsuarioDestino("131");
         * destino2.setNombredestino("Vladimir Callisaya");
         * destino2.setNumCopia("0");
         * destino2.setProveido("Para su atencion novena");
         * destino2.setTipoAsignacion("TA2");
         * destino2.setTipoAsignacionDesc("ASIGNACION");
         * 
         * listaDestinos.add(destino2);
         * 
         * String idUser = "150"; String terminal = "127.0.0.1";
         * 
         * try { //respuesta = SicodClient.asignarTramiteSicod(idProceso,
         * idAsignacionOrigen, idUsuarioOrigen, listaDestinos, idUser,
         * terminal);
         * 
         * System.out.println("La respuesta del cliente es: "); for
         * (AsignacionTramiteRespBean l :
         * SicodClient.asignarTramiteSicod(idProceso, idAsignacionOrigen,
         * idUsuarioOrigen, listaDestinos, idUser, terminal)) {
         * System.out.println("Asignacion: " + l.getIdAsignacion());
         * System.out.println("Usuario: " + l.getIdUsuarioDestino()); }
         * 
         * //System.out.println("La respuesta del cliente es: ");
         * //System.out.println("******** " + respuesta + " ********"); } catch
         * (Exception e) { e.printStackTrace(); }
         */
		// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/* Datos para enviar al cliente del SICOD - centralizarTramiteSicod */
        /*
         * String respuesta = ""; String idProceso = "33689"; String
         * idAsignacion = "137037";
         * 
         * SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
         * 
         * List<AsignacionBean> listaAsignaciones = new
         * ArrayList<AsignacionBean>(); AsignacionBean asignacion = new
         * AsignacionBean(); asignacion.setDestino("131"); try {
         * asignacion.setFechaAsignacion(sdf.parse("2017-01-18"));
         * asignacion.setFechaRecepcion(sdf.parse("2017-01-18")); } catch
         * (ParseException ex) { ex.printStackTrace(); }
         * asignacion.setFojas("6"); asignacion.setIdAsignacion("137038");
         * asignacion.setIdUsuarioDestino("131");
         * asignacion.setIdUsuarioOrigen("134"); asignacion.setOrigen("134");
         * asignacion.setProveido("FGHFGH"); listaAsignaciones.add(asignacion);
         * 
         * String idUsuario = "131"; String terminal = "127.0.0.1";
         * 
         * try { respuesta = SicodClient.centralizarTramiteSicod(idProceso,
         * idAsignacion, listaAsignaciones, idUsuario, terminal);
         * System.out.println("La respuesta del cliente es: ");
         * System.out.println("******** " + respuesta + " ********"); } catch
         * (Exception ex) { ex.printStackTrace(); }
         */
		// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/*
         * Datos para enviar al cliente del SICOD - verificacion de tramite
         * sicod
         */

        /*
         * String idProceso = "5";Boolean respuesta; try{ respuesta =
         * SicodClient.esTramiteSicod(idProceso);
         * System.out.println("El tramite sicod: " + idProceso +
         * " devolvio el valor: " + respuesta); }catch (Exception ex){
         * ex.printStackTrace(); }
         */
		// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/* Datos para enviar al cliente del SICOD - reversión de tramite sicod */
        /*
         * String idProceso = "6025"; String idAsignacion = "14169"; String
         * idUsuarioOrigen = "107"; // debe ser el usuario asignado String
         * idUser = "107"; String terminal = "127.0.0.1";
         * 
         * try{ String respuesta = SicodClient.revertirTramiteSicod(idProceso,
         * idAsignacion, idUsuarioOrigen, idUser, terminal);
         * System.out.println("Respuesta para la reversion: " + respuesta);
         * }catch (Exception ex){ ex.printStackTrace(); }
         */
		// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/*
         * Datos para enviar al cliente del SICOD - verificacion del tramite
         * sicod
         */
        /*
         * String idProceso = "17674"; String idUsuario = "77"; ProcesoBean proc
         * = new ProcesoBean();
         * 
         * try{ proc = SicodClient.verificartramiteSicod(idProceso, idUsuario);
         * 
         * System.out.println("respuesta metodo: " + proc.isRespuesta());
         * System.out.println("fechaCreacion metodo: " +
         * proc.getFechaCreacionProceso());
         * System.out.println("gestion metodo: " + proc.getGestion());
         * System.out.println("idProceso metodo: " + proc.getIdProceso());
         * System.out.println("mensaje metodo: " + proc.getMensaje());
         * System.out.println("referencia metodo: " + proc.getReferencia());
         * 
         * }catch (Exception ex){ ex.printStackTrace(); }
         */
		// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/* datos para devolver pdf por uid */
        // String uid = "464c8fa3-d8c5-48bf-bcfe-f64ee1f68d5c"; //word adjunto
        // String uid = "9e15c5b1-2473-4df1-867b-4f159e80ff68"; //editor
        // String uid = "9e15c5b1-2473-4df1-867b-4f159e80ff68";
        // String uid = "40f6130a-b8c3-461d-a21b-2698bd39fc59";
        //
        // byte[] pdfDocumento = SicodClient.documenetoPdfPorUid(uid);
        //
        //
        // if(pdfDocumento != null){
        // try{
        // FileOutputStream fileOutput = new
        // FileOutputStream("d:\\documentoSicodGenerado.pdf");
        // BufferedOutputStream buffered = new BufferedOutputStream(fileOutput);
        // //byte[] arr = respuestaSegip.getCertificacionPdf();
        // buffered.write(pdfDocumento, 0, pdfDocumento.length);
        //
        // buffered.close();
        // fileOutput.close();
        // }catch (Exception ex){
        // ex.printStackTrace();
        // }
        // }else{
        // System.out.println("el pdf esta vacio");
        // }
        // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/* Datos para enviar al cliente del SICOD - recepcion de tramite sicod */
        /*
         * String idProceso = "6026"; String idAsignacion = "14174"; String
         * idUserRecepcion = "108"; // debe ser el usuario asignado String
         * idUser = "107"; String terminal = "127.0.0.1"; String estado = "R";
         * 
         * try{ String respuesta =
         * SicodClient.recepcionarTramiteSicod(idProceso, idAsignacion,
         * idUserRecepcion, idUser, terminal, estado);
         * System.out.println("Respuesta para la recepcion: " + respuesta);
         * }catch (Exception ex){ ex.printStackTrace(); }
         */
//		List<String> numeroTramite = new ArrayList<String>();
//		numeroTramite.add("43687");
//		numeroTramite.add("49123");
//		numeroTramite.add("40100");
//		numeroTramite.add("36149");
//		//			numeroTramite.add("41526");
//		numeroTramite.add("43646");
//		numeroTramite.add("46022");
//        numeroTramite.add("48713");    //INFORME/UIF/DAFL/JAF2/423/2017 
//		numeroTramite.add("42237");    //INFORME/UIF/DAFL/JAF2/412/2017
//		//numeroTramite.add("43805");	   //INFORME/UIF/DAFL/JAF1/446/2017
//		numeroTramite.add("41947");	   //INFORME/UIF/DAFL/JAF2/367/2017
//		numeroTramite.add("45780");
//		numeroTramite.add("45702");
//		numeroTramite.add("37590");
//		numeroTramite.add("39428");
//		numeroTramite.add("44048");
//		//              numeroTramite.add("40463");
//		numeroTramite.add("43243");
//		numeroTramite.add("42152");
//		numeroTramite.add("46242");
//		String tipoDocumento = "INF5";
//		DocumentoBean documento;
//		try {
//			for (int i = 0; i < numeroTramite.size(); i++) {
//				documento = SicodClient.buscaDocumentoSicodPorNumeroTramite(numeroTramite.get(i),
//						tipoDocumento);
//				
//				System.out.println("NumeroTrmaite-------:"+numeroTramite.get(i));
//				System.out.println("La respuesta del cliente es: ");
//				System.out.println("******** " + documento.getNumeroDocumento()
//						+ " ********");
//			}
//
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
        String numeroProceso = "59740";
        Boolean verifica_mas_asignaciones;
        verifica_mas_asignaciones = SicodClient.verificaVariasAsignaciones(numeroProceso);
        System.out.println("verificar mas de una asignacion" + verifica_mas_asignaciones);

    }
}
/*pruebas de servico web para buscar 
		
		
		
		
		
		

 }
 }
 // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 /*
 * Datos para enviar al cliente del SICOD -
 * buscaDocumentoSicodPorNumeroTramite
 */
		// String numeroTramite = "40100";
// String numeroTramite="43687"; //INFORME/UIF/DAFL/JAF1/354/2017
// String numeroTramite="49123"; //INFORME/UIF/DAFL/JAF1/423/2017
// String numeroTramite="40100"; //INFORME/UIF/DAFL/JAF2/319/2017
// String numeroTramite="36149";
// String numeroTramite="41526";
// String numeroTramite="43646";
// String numeroTramite="46022";
// String numeroTramite="48713";
// String numeroTramite="42237";
// String numeroTramite="43805";
// String numeroTramite="41947";
// String numeroTramite="45780";
// String numeroTramite="45702";
// String numeroTramite="37590";
// String numeroTramite="39428";
// String numeroTramite="44048";
// String numeroTramite="40463";
// String numeroTramite="43243";
// String numeroTramite="42152";
// String numeroTramite="46242";

