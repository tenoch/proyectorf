package bo.gob.uif.tests.fundempresa;

import bo.gob.uif.webclients.fundempresa.FundempresaClient;
import bo.gob.uif.webclients.fundempresa.beans.*;

import java.util.List;

/**
 * Created by Vladimir.Callisaya on 02/09/2016.
 */
public class TestFundempresa {

    public static void main(String[] args) {
        // prueba metodo srvActividades
        String idMatricula = "150483";

        try {
            List<MatriculaActividadBean> lista;
            lista = FundempresaClient.srvActividades(idMatricula);

            for (MatriculaActividadBean l : lista) {
                System.out.println("CtrResult: " + l.getCtrResult());
                System.out.println("IdMatricula: " + l.getIdMatricula());
                System.out.println("TxtActividad: " + l.getTxtActividad());
                System.out.println("Seccion: " + l.getSeccion());
                System.out.println("Division: " + l.getDivision());
                System.out.println("Clase: " + l.getClase());
                System.out.println("VersionClasificador: " + l.getVersionClasificador());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // prueba metodo SrvInfomatricula
        //String idMatricula = "150483";
        /*String idMatricula = "00013228";
         try{
         List<InfoMatriculaBean> lista;
         lista = FundempresaClient.srvInfoMatricula(idMatricula);

         for(InfoMatriculaBean l : lista){
         System.out.println("CtrResult: " + l.getCtrResult());
         System.out.println("IdMatricula: " + l.getIdMatricula());
         System.out.println("RazonSocial: " + l.getRazonSocial());
         System.out.println("TipoSocietario: " + l.getTipoSocietario());
         System.out.println("FechaInscripcion: " + l.getFechaInscripcion());
         System.out.println("FechaUltRenovacion: " + l.getFechaUltRenovacion());
         System.out.println("UltGestionRenovada: " + l.getUltGestionRenovada());
         System.out.println("Nit: " + l.getNit());
         System.out.println("Departamento: " + l.getDepartamento());
         System.out.println("Provincia: " + l.getProvincia());
         System.out.println("Municipio: " + l.getMunicipio());
         System.out.println("CalleAv: " + l.getCalleAv());
         System.out.println("EntreCalles: " + l.getEntreCalles());
         System.out.println("Nro: " + l.getNro());
         System.out.println("Uv: " + l.getUv());
         System.out.println("Mza: " + l.getMza());
         System.out.println("Edificio: " + l.getEdificio());
         System.out.println("Piso: " + l.getPiso());
         System.out.println("NroOficina: " + l.getNroOficina());
         System.out.println("Zona: " + l.getZona());
         System.out.println("CorreoElectronico: " + l.getCorreoElectronico());
         System.out.println("Activdad: "+ l.getActividad());
         System.out.println("ClaseCIIU: "+ l.getClaseCIIU());
         System.out.println("CtrEstado: " + l.getCtrEstado());
         System.out.println("MatriculaDatosSucList1: ");
         if(l.getMatriculaDatosSucList()!=null){
         for(MatriculaDatosSucBean m : l.getMatriculaDatosSucList()){
         System.out.println("IdSuc: " + m.getIdSuc());
         System.out.println("Sucursal: " + m.getSucursal());
         System.out.println("Departamento: " + m.getDepartamento());
         System.out.println("Municipio: " + m.getMunicipio());
         System.out.println("Provincia: " + m.getProvincia());
         System.out.println("Direccion: " + m.getDireccion());
         System.out.println("Zona: " + m.getZona());
         System.out.println("Telefono: " + m.getTelefono());
         System.out.println("IdClase: " + m.getIdClase());
         System.out.println("NumId: " + m.getNumId());
         System.out.println("Representante: " + m.getRepresentante());
         }
         }else{
         System.out.println("No se devolvio MatriculaDatosSucList");
         }
         }
         }catch (Exception ex){
         ex.printStackTrace();
         }*/
        // prueba del método SrvMatriculaConsultaNit
        /*String nit = "749036018";

         try{
         List<MatriculasResultBean> lista;
         lista = FundempresaClient.srvMatriculaConsultaNit(nit);

         for(MatriculasResultBean l : lista){
         System.out.println("CtrResult: " + l.getCtrResult());
         System.out.println("IdMatricula: " + l.getIdMatricula());
         System.out.println("Nit: " + l.getNit());
         System.out.println("RazonSocial: " + l.getRazonSocial());
         System.out.println("CtrEstado: " + l.getCtrEstado());
         }
         }catch (Exception ex){
         ex.printStackTrace();
         }*/
        //prueba del metodo SrvMatriculaConsultaRazon
        /*String consulta = "dios";
         try{
         List<BusquedaRazonSocialBean> lista;
         lista = FundempresaClient.srvMatriculaConsultaRazon(consulta);

         for(BusquedaRazonSocialBean l : lista){
         System.out.println("CtrResult: " + l.getCtrResult());
         System.out.println("IdMatricula: " + l.getIdMatricula());
         System.out.println("RazonSocial: " + l.getRazonSocial());
         System.out.println("Nit: " + l.getNit());
         System.out.println("***************************************************");
         }
         }catch (Exception ex){
         ex.printStackTrace();
         }*/
        //prueba para el metodo SrvMatriculaListSuc
        /*String idMatricula = "150483";
         try{
         List<MatriculaResultSucBean> lista;
         lista = FundempresaClient.srvMatriculaListSuc(idMatricula);

         for(MatriculaResultSucBean l : lista){
         System.out.println("CtrResult: " + l.getCtrResult());
         System.out.println("IdMatricula: " + l.getIdMatricula());
         System.out.println("RazonSocial: " + l.getRazonSocial());
         System.out.println("FechaInscripcion: " + l.getFechaInscripcion());
         System.out.println("FechaUltRenovacion: " + l.getFechaUltRenovacion());
         System.out.println("UltGestionRenovada: " + l.getUltGestionRenovada());
         System.out.println("Nit: " + l.getNit());
         System.out.println("CtrEstado: " + l.getCtrEstado());
         if(l.getListaMatriculaDatosSucBean()!=null){
         for(MatriculaDatosSucBean m : l.getListaMatriculaDatosSucBean()){
         System.out.println("IdSuc: " + m.getIdSuc());
         System.out.println("Sucursal: " + m.getSucursal());
         System.out.println("Departamento: " + m.getDepartamento());
         System.out.println("Municipio: " + m.getMunicipio());
         System.out.println("Provincia: " + m.getProvincia());
         System.out.println("Direccion: " + m.getDireccion());
         System.out.println("Zona: " + m.getZona());
         System.out.println("Telefono: " + m.getTelefono());
         System.out.println("IdClase: " + m.getIdClase());
         System.out.println("NumId: " + m.getNumId());
         System.out.println("Representante: " + m.getRepresentante());

         }
         }else{
         System.out.println("No se devolvio MatriculaDatosSucList");
         }
         }
         }catch (Exception ex){
         ex.printStackTrace();
         }*/
        //prueba del metodo SrvMatriculaVigencia
        /*String idMatricula = "150483";
         try{
         List<MatriculaDatosVigenciaBean> lista;
         lista = FundempresaClient.srvMatriculaVigencia(idMatricula);

         for(MatriculaDatosVigenciaBean l : lista){
         System.out.println("CtrResult: " + l.getCtrResult());
         System.out.println("IdMatricula: " + l.getIdMatricula());
         System.out.println("RazonSocial: " + l.getRazonSocial());
         System.out.println("FechaInscripcion: " + l.getFechaInscripcion());
         System.out.println("FechaUltRenovacion: " + l.getFechaUltRenovacion());
         System.out.println("UltGestionRenovada: " + l.getUltGestionRenovada());
         System.out.println("VigenciaMatricula: " + l.getVigenciaMatricula());
         System.out.println("FecVigenciaMatricula: " + l.getFecVigenciaMatricula());
         System.out.println("Nit: " + l.getNit());
         System.out.println("CtrEstado: " + l.getCtrEstado());
         }
         }catch (Exception ex){
         ex.printStackTrace();
         }*/
        //prueba del metodo SrvRepresentante
        /*String idMatricula = "150483";
         try{
         List<RepresentanteBean> lista;
         lista = FundempresaClient.srvRepresentante(idMatricula);

         for(RepresentanteBean l : lista){
         System.out.println("CtrResult: " + l.getCtrResult());
         System.out.println("IdMatricula: " + l.getIdMatricula());
         System.out.println("NombreVinculo: " + l.getNombreVinculo());
         System.out.println("NumId: " + l.getNumId());
         System.out.println("IdClase: " + l.getIdClase());
         System.out.println("FecRegistro: " + l.getFecRegistro());
         System.out.println("NumDoc: " + l.getNumDoc());
         System.out.println("FecDocumento: " + l.getFecDocumento());
         System.out.println("NoticiaDocumento: " + l.getNoticiaDocumento());
         System.out.println("TipoVinculo: " + l.getTipoVinculo());
         System.out.println("IdLibro: " + l.getIdLibro());
         System.out.println("NumReg: " + l.getNumReg());
         }
         }catch (Exception ex){
         ex.printStackTrace();
         }*/
        //prueba del metodo srvMatricula
        /*String idMatricula = "150483";
         try{
         List<InfoMatriculaBean> lista;
         lista = FundempresaClient.srvMatricula(idMatricula);

         for(InfoMatriculaBean l : lista){
         System.out.println("CtrResult: " + l.getCtrResult());
         System.out.println("IdMatricula: " + l.getIdMatricula());
         System.out.println("RazonSocial: " + l.getRazonSocial());
         System.out.println("FechaInscripcion: " + l.getFechaInscripcion());
         System.out.println("FechaUltRenovacion: " + l.getFechaUltRenovacion());
         System.out.println("UltGestionRenovada: " + l.getUltGestionRenovada());
         System.out.println("Nit: " + l.getNit());
         System.out.println("CtrEstado: " + l.getCtrEstado());
         }
         }catch (Exception ex){
         ex.printStackTrace();
         }*/
        //prueba del metodo srvImagen

        /*String idMatricula = "150483";
         String idLibro = "80";
         String numReg = "00147426";
         try{
         List<ImagenDatosBean> lista;
         lista = FundempresaClient.srvImagen(idMatricula, idLibro, numReg);  

         for(ImagenDatosBean l : lista){
         System.out.println("CtrResult: " + l.getCtrResult());
         System.out.println("IdMatricula: " + l.getIdMatricula());
         System.out.println("NumReg: " + l.getNumReg());
         System.out.println("IdLibro: " + l.getIdLibro());
         System.out.println("CodImagen: " + l.getCodImagen());
         System.out.println("RutaImagen: " + l.getRutaImagen());
         }
         }catch (Exception ex){
         ex.printStackTrace();
         }*/
    }
}
