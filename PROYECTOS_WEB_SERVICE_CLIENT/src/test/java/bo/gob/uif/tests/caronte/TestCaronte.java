//package bo.gob.uif.tests.caronte;
//
//import bo.gob.uif.webclients.caronte.CaronteClient;
//import bo.gob.uif.webclients.caronte.beans.SujetoObligadoBusquedaBean;
//import bo.gob.uif.webclients.caronte.beans.SujetoObligadoDatosBean;
//import bo.gob.uif.webclients.caronte.beans.FuncionarioResponsableDatosBean;
//import bo.gob.uif.webclients.caronte.beans.AnalistaCumplimientoDatosBean;
//import bo.gob.uif.webclients.caronte.beans.UsuarioServiciosBean;
//
//import java.util.List;
//
//public class TestCaronte {
//	 public static void main(String[] args) {
//		 
//		 // SERVICIO 1 : PRUEBA DEL PRIMER SERVICIO (BUSQUEDA SUJETO OBLIGADO)
//		 /*
//		 String strSujetoObligado = "aso";
//		 int intTipoBusqueda = 2;
//		 String usuario = "UIF";
//		 String contrasenia = "1234567";
//		 try{
//	            List<SujetoObligadoBusquedaBean> lista;
//	            lista = CaronteClient.getSujetosObligados_busqueda(strSujetoObligado, intTipoBusqueda, usuario, contrasenia);
//	            System.out.println("-----------------------------------------------------------------------------");
//	            for(SujetoObligadoBusquedaBean l : lista)
//	            {
//	                System.out.println("idEntidad: " + l.getIdEntidad());
//	                System.out.println("SUJETO_OBLIGADO: " + l.getSujetoObligado());
//	                System.out.println("TIPO_EMPRESA: " + l.getTipoEmpresa());
//	                System.out.println("TIPO_SUJETO_OBLIGADO: " + l.getTipoSujetoObligado());
//	                System.out.println("OFCEN_CIUDAD_LOCALIDAD: " + l.getOfcen_ciudad_localidad());
//	                System.out.println("DESCRIPCION_ESTADO: " + l.getEstado());
//	                System.out.println("err_codigo: " + l.getErr_codigo());
//	                System.out.println("err_mensaje: " + l.getErr_mensaje());
//	                System.out.println("-----------------------------------------------------------------------------");
//	                System.out.println();
//	            }
//	            
//	        }catch (Exception ex){
//	            ex.printStackTrace();
//	        }
//	     
//	     
//		// SERVICIO 2 : PRUEBA DEL SEGUNDO SERVICIO (SUJETO OBLIGADO ESPECIFICO SUJETO OBLIGADO)
//		 int idEntidad = 1005;  
//		 int nivCodigo = 12; 
//		 int dominfCodigo = 1;
//		 String usuario = "UIF";
//		 String contrasenia = "123456";
//
//		 try{
//			 	List<SujetoObligadoDatosBean> lista;
//	            lista = CaronteClient.getSujetosObligados_datos_dinamicos(idEntidad, nivCodigo, dominfCodigo, usuario, contrasenia); // CaronteClient.getSujetosObligados_busqueda(strSujetoObligado, intTipoBusqueda);
//	            System.out.println("-----------------------------------------------------------------------------");
//	            for(SujetoObligadoDatosBean l : lista)
//	            {
//	                System.out.println("CodigoEntidad: " + l.getCodigoEntidad());
//	                System.out.println("DescripcionEntidad: " + l.getDescripcionEntidad());
//	                System.out.println("TipoEntidad: " + l.getTipoEntidad());
//	                System.out.println("OtroEntidad: " + l.getOtroEntidad());
//	                System.out.println("NitEntidad: " + l.getNitEntidad());
//	                System.out.println("LugarEntidad: " + l.getLugarEntidad());
//	                System.out.println("ZonaEntidad: " + l.getZonaEntidad());
//	                System.out.println("DireccionEntidad: " + l.getDireccionEntidad());
//	                System.out.println("TelefonoEntidad: " + l.getTelefonoEntidad());
//	                System.out.println("CorreoEntidad: " + l.getCorreoEntidad());
//	                System.out.println("TipoSujetoObligado: " + l.getTipoSujetoObligado());
//	                System.out.println("ErrCodigo: " + l.getErrCodigo());
//	                System.out.println("ErrMensaje: " + l.getErrMensaje());
//	                System.out.println("-----------------------------------------------------------------------------");
//	                System.out.println();
//	            }
//	            
//	        }catch (Exception ex){
//	            ex.printStackTrace();
//	        }
//	    
//		 
//		// SERVICIO 3 : PRUEBA DEL TERCER SERVICIO (FUNCIONARIO RESPONSABLE)
//				 int idEntidad = 1005;  
//				 int nivCodigo = 12; 
//				 int dominfCodigo = 2;
//				 String usuario = "UIF";
//				 String contrasenia = "1234567";
//
//				 try{
//					 	List<FuncionarioResponsableDatosBean> lista;
//			            lista = CaronteClient.getFuncionarioResponsable_datos_dinamicos(idEntidad, nivCodigo, dominfCodigo, usuario, contrasenia); // CaronteClient.getSujetosObligados_busqueda(strSujetoObligado, intTipoBusqueda);
//			            System.out.println("-----------------------------------------------------------------------------");
//			            for(FuncionarioResponsableDatosBean l : lista)
//			            {
//			                System.out.println("CodigoFuncionario: " + l.getCodigoFuncionario());
//			                System.out.println("NombreFuncionario: " + l.getNombreFuncionario());
//			                System.out.println("ApPaternoFuncionario: " + l.getApPaternoFuncionario());
//			                System.out.println("ApMaternoFuncionario: " + l.getApMaternoFuncionario());
//			                System.out.println("ApCasadaFuncionario: " + l.getApCasadaFuncionario());
//			                System.out.println("DocumentoFuncionario: " + l.getDocumentoFuncionario());
//			                System.out.println("FechaNacimientoFuncionario: " + l.getFechaNacimientoFuncionario());
//			                System.out.println("TelefonoFuncionario: " + l.getTelefonoFuncionario());
//			                System.out.println("InternoFuncionario: " + l.getInternoFuncionario());
//			                System.out.println("ErrCodigo: " + l.getErrCodigo());
//			                System.out.println("ErrMensaje: " + l.getErrMensaje());
//			                System.out.println("-----------------------------------------------------------------------------");
//			                System.out.println();
//			            }
//			            
//			        }catch (Exception ex){
//			            ex.printStackTrace();
//			        }
//		*/
//		/*	
//		// SERVICIO 4 : PRUEBA DEL CUARTO SERVICIO (ANALISTA DE CUMPLIMIENTO)
//		 int idEntidad = 1005;  
//		 int nivCodigo = 12; 
//		 int dominfCodigo = 2;
//		 String usuario = "UIF";
//		 String contrasenia = "123456";
//
//		 try{
//			 	List<AnalistaCumplimientoDatosBean> lista;
//	            lista = CaronteClient.getAnalistaCumplimiento_datos_dinamicos(idEntidad, nivCodigo, dominfCodigo, usuario, contrasenia); // CaronteClient.getSujetosObligados_busqueda(strSujetoObligado, intTipoBusqueda);
//	            System.out.println("-----------------------------------------------------------------------------");
//	            for(AnalistaCumplimientoDatosBean l : lista)
//	            {
//	                System.out.println("CodigoFuncionario: " + l.getCodigoFuncionario());
//	                System.out.println("NombreFuncionario: " + l.getNombreFuncionario());
//	                System.out.println("ApPaternoFuncionario: " + l.getApPaternoFuncionario());
//	                System.out.println("ApMaternoFuncionario: " + l.getApMaternoFuncionario());
//	                System.out.println("ApCasadaFuncionario: " + l.getApCasadaFuncionario());
//	                System.out.println("DocumentoFuncionario: " + l.getDocumentoFuncionario());
//	                System.out.println("FechaNacimientoFuncionario: " + l.getFechaNacimientoFuncionario());
//	                System.out.println("TelefonoFuncionario: " + l.getTelefonoFuncionario());
//	                System.out.println("InternoFuncionario: " + l.getInternoFuncionario());
//	                System.out.println("ErrCodigo: " + l.getErrCodigo());
//	                System.out.println("ErrMensaje: " + l.getErrMensaje());
//	                System.out.println("-----------------------------------------------------------------------------");
//	                System.out.println();
//	            }
//	            
//	        }catch (Exception ex){
//	            ex.printStackTrace();
//	        }
//	    */
//		 // SERVICIO 5 : PRUEBA AL SERVICIO DE AUTENTICACION DE USUARIO
//		 String usuario = "UIF";
//		 String contrasenia = "123456";
//		 try{
//			 	List<UsuarioServiciosBean> lista;
//	            lista = CaronteClient.getUsuario_servicio(usuario, contrasenia); // CaronteClient.getSujetosObligados_busqueda(strSujetoObligado, intTipoBusqueda);
//	            System.out.println("-----------------------------------------------------------------------------");
//	            for(UsuarioServiciosBean l : lista)
//	            {
//	            	System.out.println("Alias: " + l.getAlias());
//	            	System.out.println("Fecha Registro: " + l.getFechaRegistro());
//	            	System.out.println("Estado: " + l.getEstado());
//	            	System.out.println("Estado Descripcion: " + l.getEstadoDescripcion());
//	            	System.out.println("ErrCodigo: " + l.getErrCodigo());
//	                System.out.println("ErrMensaje: " + l.getErrMensaje());
//	                System.out.println("-----------------------------------------------------------------------------");
//	                System.out.println();
//	            }
//	            
//	        }catch (Exception ex){
//	            ex.printStackTrace();
//	        }
//	    
//	}
//}
