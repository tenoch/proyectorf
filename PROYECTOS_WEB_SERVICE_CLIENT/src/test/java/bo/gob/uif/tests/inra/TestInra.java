/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.gob.uif.tests.inra;

import bo.gob.uif.webclients.cfg.MainWebServicesConfig;
import bo.gob.uif.webclients.cfg.WebServiceConfig;
import bo.gob.uif.webclients.inra.beans.RespuestaInraBean;
import static bo.gob.uif.webclients.inra.InraClient.JSON_MAPPER;
import com.fasterxml.jackson.core.type.TypeReference;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.io.IOUtils;
import bo.gob.uif.webclients.inra.InraClient;
import bo.gob.uif.webclients.inra.beans.RespuestaInraDatosBeneficiarioBean;

/**
 *
 * @author Susana.Escobar
 *
 * Creado por: Susana Escobar Paz:
 *
 * Descripcion: clase para pruebas de la implementacion de metodos de la clase
 * InraClient
 *
 * Fecha: 30/05/2018
 */
public class TestInra {

    // public static WebServiceConfig wsConfig = MainWebServicesConfig.INSTANCE.getWebServiceConfig("PRUEBA");
    public static void main(String[] args) {
        RespuestaInraBean objeto = new RespuestaInraBean();

        String titulo = "SPPNAL072699";

        objeto = InraClient.busquedaNumeroTitulo(titulo);

        System.out.println("OBEJTOOOOO   " + objeto.getNumeroTitulo());
        System.out.println("OBEJTOOOOO   " + objeto.toString());
        System.out.println("OBEJTOOOOO   " + objeto.getMensaje());

        String numero = "3966973";
        String dato = "SALINAS";

        RespuestaInraDatosBeneficiarioBean respues = InraClient.busquedaDatosBenefciario(numero, dato);

        System.out.println("TAMANIO            " + respues.getListaRespuestaDatosBeneficiario().size());
        System.out.println("OBEJTOOOOO   " + respues.getListaRespuestaDatosBeneficiario().get(0).toString());
        System.out.println("RESPUESTA           " + respues.getDatoEncontrado());
        System.out.println("MENSAJE             " + respues.getMensaje());
    }

}
