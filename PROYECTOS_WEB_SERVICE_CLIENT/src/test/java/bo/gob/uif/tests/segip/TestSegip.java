package bo.gob.uif.tests.segip;

import bo.gob.uif.webclients.cfg.MainWebServicesConfig;
import bo.gob.uif.webclients.cfg.WebServiceConfig;
import bo.gob.uif.webclients.segip.SegipClient;
import bo.gob.uif.webclients.segip.beans.DatosRespuestaBean;
import bo.gob.uif.webclients.segip.beans.UsuarioFinalAltaBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;

/**
 * Created by Vladimir.Callisaya on 12/05/2017.
 */
public class TestSegip {

    public static void main(String[] args) {
        DatosRespuestaBean respuestaSegip;
        UsuarioFinalAltaBean usuarioFinal;
        WebServiceConfig wsConfig = MainWebServicesConfig.INSTANCE.getWebServiceConfig("SINConsultaSerWeb");
        MainWebServicesConfig.INSTANCE.getConfigLogClWs("log4j2ClWs");
        Logger log = LogManager.getLogger("TestBean.SegipClient");
        try {

            /*
             // para creacion de usuario

             String pNumeroCedula = "13979276";
             String pComplemento = "";
             String pNombre = "MATTHIAS";
             String pPrimerApellido = "PADILLA";
             String pSegundoApellido = "PEI";
             String pFechaNacimiento = "";
             String pNombreInstitucion = "UIF";
             String pNombreCargo = "ANALISTA SISTEMAS";
             String pDepartamento = "LA PAZ";
             String pProvincia = "MURILLO";
             String pSeccionMunicipal = "";
             String pLocalidad = "NUESTRA SEÑORA DE LA PAZ";
             String pOficina = "";
             String pCorreoElectronico = "";
             String pTelefono = "2188988";
             String pTelefonoOficina = "112";




             usuarioFinal = SegipClient.creacionUsuarioFinal(pNumeroCedula, pComplemento, pNombre, pPrimerApellido, pSegundoApellido, pFechaNacimiento, pNombreInstitucion, pNombreCargo, pDepartamento, pProvincia, pSeccionMunicipal, pLocalidad, pOficina, pCorreoElectronico, pTelefono, pTelefonoOficina);

             log.debug("Consumiendo UsuarioFinalAlta");
             log.debug("Clave de acceso: " + usuarioFinal.getClaveAcceso());
             log.debug("Es valido: " + usuarioFinal.isEsValido());
             log.debug("Mensaje: " + usuarioFinal.getMensaje());
             log.debug("Tipo Mensaje: " + usuarioFinal.getTipoMensaje());*/
            // para modificacion de datos de usuario
            String claveAcceso = "M44013979276";
            String pNombreInstitucion = "U.I.F.";
            String pNombreCargo = "ANALISTA SISTEMAS";
            String pDepartamento = "LA PAZ";
            String pProvincia = "MURILLO";
            String pSeccionMunicipal = "";
            String pLocalidad = "NUESTRA SEÑORA DE LA PAZ";
            String pOficina = "";
            String pCorreoElectronico = "";
            String pTelefono = "2188988";
            String pTelefonoOficina = "112";

            /*
             //para habilitacion e inhabilitación de usuario

             String claveAcceso = "M44013979276";
             boolean estado = false;

             usuarioFinal = SegipClient.habilitaInhabilitaUsuarioFinal(claveAcceso, estado);

             log.debug("Consumiendo usuarioFinalHabilitacionInhabilitacion");
             log.debug("Clave de acceso: " + usuarioFinal.getClaveAcceso());
             log.debug("Es valido: " + usuarioFinal.isEsValido());
             log.debug("Mensaje: " + usuarioFinal.getMensaje());
             log.debug("Tipo Mensaje: " + usuarioFinal.getTipoMensaje());
             */
            //Para pruebas de consumo metodos json y certificacion
            /*String pClaveAccesoUsuarioFinal = "R3795543216";
             String pNumeroAutorizacion = "1";            
             String pNumeroDocumento = "13979276";
             String pComplemento = "";
             String pNombre = "";
             String pPrimerApellido = "";
             String pSegundoApellido = "";
             String pFechaNacimiento = "";*/

            /*respuestaSegip = SegipClient.consultaDatoPersonaEnJson(pClaveAccesoUsuarioFinal, pNumeroAutorizacion, pNumeroDocumento, pComplemento, pNombre, pPrimerApellido, pSegundoApellido, pFechaNacimiento);
             log.debug("Consumiendo DatoPerosnaJson");
             log.debug("Es valido: " + respuestaSegip.isEsValido());
             log.debug("Mensaje: " + respuestaSegip.getMensaje());
             log.debug("Tipo Mensaje: " + respuestaSegip.getTipoMensaje());
             log.debug("Codigo Respuesta: " + respuestaSegip.getCodigoRespuesta());
             log.debug("Codigo Unico: " + respuestaSegip.getCodigoUnico());
             log.debug("Descripcion respuesta: " + respuestaSegip.getDescripcionRespuesta());
             if(respuestaSegip.getCodigoRespuesta() == 2){
             log.debug("******** Datos Persona ********");
             log.debug("Complemento visible: " + respuestaSegip.getPersona().getComplementoVisible());
             log.debug("Complemento: " + respuestaSegip.getPersona().getComplemento());
             log.debug("Nombres: " + respuestaSegip.getPersona().getNombres());
             log.debug("Primer Apellido: " + respuestaSegip.getPersona().getPrimerApellido());
             log.debug("Segundo Apellido: " + respuestaSegip.getPersona().getSegundoApellido());
             log.debug("Fecha Nacimiento: " + respuestaSegip.getPersona().getFechaNacimiento());
             }*/

            /*respuestaSegip = SegipClient.consultaDatoPersonaCertificacion(pClaveAccesoUsuarioFinal, pNumeroAutorizacion, pNumeroDocumento, pComplemento, pNombre, pPrimerApellido, pSegundoApellido, pFechaNacimiento);
             log.debug("Consumiendo DatoPersonaCertificacion");
             log.debug("Es valido: " + respuestaSegip.isEsValido());
             log.debug("Mensaje: " + respuestaSegip.getMensaje());
             log.debug("Tipo Mensaje: " + respuestaSegip.getTipoMensaje());
             log.debug("Codigo Respuesta: " + respuestaSegip.getCodigoRespuesta());
             log.debug("Codigo Unico: " + respuestaSegip.getCodigoUnico());
             log.debug("Descripcion respuesta: " + respuestaSegip.getDescripcionRespuesta());
             if(respuestaSegip.getCodigoRespuesta() == 2){
             log.debug("******** Certificacion ********");

             FileOutputStream fileOutput = new FileOutputStream("d:\\certificacion.pdf");
             BufferedOutputStream buffered = new BufferedOutputStream(fileOutput);
             byte[] arr = respuestaSegip.getCertificacionPdf();
             buffered.write(arr, 0, arr.length);

             buffered.close();
             fileOutput.close();
             }*/
        } catch (Exception ex) {
            log.catching(ex);
        }
    }
}
