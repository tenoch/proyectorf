/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.gob.uif.webclients.cfg;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;

/**
 *
 * @author Alan.Portugal
 */
public enum MainWebServicesConfig {

    INSTANCE;
    private static final boolean IS_WINDOWS;
    private static final String PROPERTIES_PATH;
    // valores para digital sign
    public static final String KEY_STORE_PRIV;
    public static final String PATH_KEY_STORE_PRIV;
    public static final String KEY_STORE_PUBLIC;
    public static final String PATH_KEY_STORE_PUBLIC;

    private Map<String, WebServiceConfig> webServiceConfig;

    static {
        InputStream input = null;
        IS_WINDOWS = INSTANCE.isSoWin();
        if (IS_WINDOWS) {
            PROPERTIES_PATH = "C:\\app\\serviciosweb\\propiedades\\";
        } else {
            PROPERTIES_PATH = "/opt/webclients/properties/";
        }

        Properties mainConfig = null;
        try {
            input = new FileInputStream(PROPERTIES_PATH + "MainWebServicesConfig.properties");
            mainConfig = new Properties();
            mainConfig.load(input);
        } catch (Exception ex) {

        }

        if (mainConfig != null) {
            KEY_STORE_PRIV = mainConfig.getProperty("sign.key_store_priv");
            PATH_KEY_STORE_PRIV = mainConfig.getProperty("sign.path_key_store_priv");
            KEY_STORE_PUBLIC = mainConfig.getProperty("sign.key_store_public");
            PATH_KEY_STORE_PUBLIC = mainConfig.getProperty("sign.path_key_store_public");
        } else {
            KEY_STORE_PRIV = null;
            PATH_KEY_STORE_PRIV = null;
            KEY_STORE_PUBLIC = null;
            PATH_KEY_STORE_PUBLIC = null;
        }
    }

    private boolean isSoWin() {
        boolean result = System.getProperty("os.name").toLowerCase().indexOf("win") >= 0;
        return result;
    }

    public WebServiceConfig getWebServiceConfig(String webServiceName) {
        WebServiceConfig result = null;
        InputStream input = null;
        if (webServiceConfig == null) {
            webServiceConfig = new LinkedHashMap<String, WebServiceConfig>();
        }
        if (!webServiceConfig.containsKey(webServiceName)) {
            Properties wsConfig = null;
            try {
                input = new FileInputStream(PROPERTIES_PATH + webServiceName + ".properties");
                wsConfig = new Properties();
                wsConfig.load(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (wsConfig != null) {
                result = loadConfig(wsConfig);
                webServiceConfig.put(webServiceName, result);
            }
        } else {
            result = webServiceConfig.get(webServiceName);
        }

        return result;
    }

    private WebServiceConfig loadConfig(Properties values) {
        WebServiceConfig result = new WebServiceConfig(propertiesToMap(values));
        if (values.getProperty("clws.location") != null && values.getProperty("clws.location").trim().length() > 0){
            result.setLocation(values.getProperty("clws.location"));
        } else {
            result.setLocation("");
        }
        if (values.getProperty("clws.location2") != null && values.getProperty("clws.location2").trim().length() > 0){
            result.setLocation2(values.getProperty("clws.location2"));
        } else {
            result.setLocation2("");
        }
        if (values.getProperty("sign.contrato_id") != null && values.getProperty("sign.contrato_id").trim().length() > 0){
            result.setIdContract(values.getProperty("sign.contrato_id"));
        } else {
            result.setIdContract("");
        }
        if (values.getProperty("sign.key_contrato") != null && values.getProperty("sign.key_contrato").trim().length() > 0){
            result.setKeyContract(values.getProperty("sign.key_contrato"));
        } else {
            result.setKeyContract("");
        }
        if (values.getProperty("sign.key_store_priv") != null && values.getProperty("sign.key_store_priv").trim().length() > 0) {
            result.setKeyStorePriv(values.getProperty("sign.key_store_priv"));
        } else {
            result.setKeyStorePriv(KEY_STORE_PRIV);
        }
        if (values.getProperty("sign.path_key_store_priv") != null && values.getProperty("sign.path_key_store_priv").trim().length() > 0) {
            result.setPathKeyStorePriv(values.getProperty("sign.path_key_store_priv"));
        } else {
            result.setPathKeyStorePriv(PATH_KEY_STORE_PRIV);
        }
        if (values.getProperty("sign.key_store_public") != null && values.getProperty("sign.key_store_public").trim().length() > 0) {
            result.setKeyStorePublic(values.getProperty("sign.key_store_public"));
        } else {
            result.setKeyStorePublic(KEY_STORE_PUBLIC);
        }
        if (values.getProperty("sign.path_key_store_public") != null && values.getProperty("sign.path_key_store_public").trim().length() > 0) {
            result.setPathKeyStorePublic(values.getProperty("sign.path_key_store_public"));
        } else {
            result.setPathKeyStorePublic(PATH_KEY_STORE_PUBLIC);
        }
        if (values.getProperty("sign.usuarioConvenio") != null && values.getProperty("sign.usuarioConvenio").trim().length() > 0) {
            result.setUsuarioConvenio(values.getProperty("sign.usuarioConvenio"));
        } else {
            result.setUsuarioConvenio("");
        }
        if (values.getProperty("sign.contrasenaConvenio") != null && values.getProperty("sign.contrasenaConvenio").trim().length() > 0) {
            result.setContrasenaConvenio(values.getProperty("sign.contrasenaConvenio"));
        } else {
            result.setContrasenaConvenio("");
        }
        if (values.getProperty("sign.codigoInstitucion") != null && values.getProperty("sign.codigoInstitucion").trim().length() > 0) {
            result.setCodigoInstitucion(values.getProperty("sign.codigoInstitucion"));
        } else {
            result.setContrasenaConvenio("");
        }

        if (values.getProperty("sign.usuario") != null && values.getProperty("sign.usuario").trim().length() > 0) {
            result.setUsuario(values.getProperty("sign.usuario"));
        } else {
            result.setUsuario("");
        }
        if (values.getProperty("sign.contrasena") != null && values.getProperty("sign.contrasena").trim().length() > 0) {
            result.setContrasena(values.getProperty("sign.contrasena"));
        } else {
            result.setContrasena("");
        }
        if (values.getProperty("clws.location3") != null && values.getProperty("clws.location3").trim().length() > 0) {
            result.setLocation3(values.getProperty("clws.location3"));
        } else {
            result.setLocation3("");
        } 
        return result;
    }

    private Map<String, String> propertiesToMap(Properties properties) {
        HashMap<String, String> map = new HashMap<String, String>();
        for (String key : properties.stringPropertyNames()) {
            String value = properties.getProperty(key);
            map.put(key, value);
        }
        return map;
    }

    public static void getConfigLogClWs(String webServiceClientName) {
        try {
            String log4jConfigFile = PROPERTIES_PATH + webServiceClientName + ".xml";
            ConfigurationSource source = new ConfigurationSource(new FileInputStream(log4jConfigFile));
            Configurator.initialize(null, source);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
